package com.welldyne.workflow.entities.jbpm;

public enum Operator {
	IS_NULL,
	NOT_NULL,
	EQUALS_TO,
	NOT_EQUALS_TO,
	LIKE_TO,
	GREATER_THAN,
	GREATER_OR_EQUALS_TO,
	LOWER_THAN,
	LOWER_OR_EQUALS_TO,
	BETWEEN,
	IN,
	NOT_IN
}
