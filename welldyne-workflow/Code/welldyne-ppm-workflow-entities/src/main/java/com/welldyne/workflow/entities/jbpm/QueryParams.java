package com.welldyne.workflow.entities.jbpm;

import java.util.ArrayList;
import java.util.List;

public class QueryParams<T> {

	private String column;

	private Operator operator;

	private List<T> values = new ArrayList<>();

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public List<T> getValues() {
		return values;
	}

	public void setValues(List<T> values) {
		this.values = values;
	}

}
