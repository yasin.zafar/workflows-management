package com.welldyne.workflow.entities.jbpm;

import java.util.ArrayList;
import java.util.List;

public class QueryRequest {

	private String orderBy;

	private String orderAsc;

	private Page page;

	List<QueryParams> filter = new ArrayList<>();

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderAsc() {
		return orderAsc;
	}

	public void setOrderAsc(String orderAsc) {
		this.orderAsc = orderAsc;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<QueryParams> getFilter() {
		return filter;
	}

	public void setFilter(List<QueryParams> filter) {
		this.filter = filter;
	}

}
