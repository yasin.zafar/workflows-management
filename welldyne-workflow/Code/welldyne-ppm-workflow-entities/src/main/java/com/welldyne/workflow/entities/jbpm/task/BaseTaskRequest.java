package com.welldyne.workflow.entities.jbpm.task;

public class BaseTaskRequest {

	private String containerId;

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
}
