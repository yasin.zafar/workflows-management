package com.welldyne.workflow.entities.jbpm.task;

public class SubmitTaskRequest extends BaseTaskRequest {

	private String taskId;
	private Object taskInput;
	private String targetUser;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Object getTaskInput() {
		return taskInput;
	}

	public void setTaskInput(Object taskInput) {
		this.taskInput = taskInput;
	}

	public String getTargetUser() {
		return targetUser;
	}

	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

}
