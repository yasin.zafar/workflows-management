package com.welldyne.workflow.entities.jbpm.process;

public class StartProcessRequest extends BaseProcessRequest {

	private Object processInput;

	public Object getProcessInput() {
		return processInput;
	}

	public void setProcessInput(Object processInput) {
		this.processInput = processInput;
	}

}
