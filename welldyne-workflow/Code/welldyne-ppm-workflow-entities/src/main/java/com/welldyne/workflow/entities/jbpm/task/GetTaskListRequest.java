package com.welldyne.workflow.entities.jbpm.task;

import com.welldyne.workflow.entities.jbpm.QueryRequest;

public class GetTaskListRequest extends BaseTaskRequest {

	private String queryName;
	private QueryRequest queryRequest;

	public String getQueryName() {
		return queryName;
	}

	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}

	public QueryRequest getQueryRequest() {
		return queryRequest;
	}

	public void setQueryRequest(QueryRequest queryRequest) {
		this.queryRequest = queryRequest;
	}

}
