package com.welldyne.ex.domainentities.order;

import java.time.LocalDate;

import com.welldyne.ex.domainentities.common.EnterpriseResourceType;

public class OrderLineItem {

	private String rxNumber;
	private String drugName;
	private String firstname;
	private String lastname;
	private int daysSuppy;
	private Integer dispenseQty;
	private Integer orderQty;
	private OrderItemStatus status;
	private String trackingNumber;
	private LocalDate estimatedDeliveryDate;
	private LocalDate shippingDate;
	private Double copay;

	private String lineItemIdentifier;
	private String ndc;
	private Integer numberOfPacks;
	private String orderSatatusMessage;
	private String rxDescription;

	private EnterpriseResourceType splitReason;

	public Double getCopay() {
		return copay;
	}

	public void setCopay(Double copay) {
		this.copay = copay;
	}

	public Integer getDispenseQty() {
		return dispenseQty;
	}

	public void setDispenseQty(Integer dispenseQty) {
		this.dispenseQty = dispenseQty;
	}

	public Integer getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(Integer orderQty) {
		this.orderQty = orderQty;
	}

	public String getLineItemIdentifier() {
		return lineItemIdentifier;
	}

	public void setLineItemIdentifier(String lineItemIdentifier) {
		this.lineItemIdentifier = lineItemIdentifier;
	}

	public String getNdc() {
		return ndc;
	}

	public void setNdc(String ndc) {
		this.ndc = ndc;
	}

	public Integer getNumberOfPacks() {
		return numberOfPacks;
	}

	public void setNumberOfPacks(Integer numberOfPacks) {
		this.numberOfPacks = numberOfPacks;
	}

	public String getOrderSatatusMessage() {
		return orderSatatusMessage;
	}

	public void setOrderSatatusMessage(String orderSatatusMessage) {
		this.orderSatatusMessage = orderSatatusMessage;
	}

	public String getRxDescription() {
		return rxDescription;
	}

	public void setRxDescription(String rxDescription) {
		this.rxDescription = rxDescription;
	}

	public String getRxNumber() {
		return rxNumber;
	}

	public void setRxNumber(String rxNumber) {
		this.rxNumber = rxNumber;
	}

	public EnterpriseResourceType getSplitReason() {
		return splitReason;
	}

	public void setSplitReason(EnterpriseResourceType splitReason) {
		this.splitReason = splitReason;
	}

	public OrderItemStatus getStatus() {
		return status;
	}

	public void setStatus(OrderItemStatus status) {
		this.status = status;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getDaysSuppy() {
		return daysSuppy;
	}

	public void setDaysSuppy(int daysSuppy) {
		this.daysSuppy = daysSuppy;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public LocalDate getEstimatedDeliveryDate() {
		return estimatedDeliveryDate;
	}

	public void setEstimatedDeliveryDate(LocalDate estimatedDeliveryDate) {
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}

	public LocalDate getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(LocalDate shippingDate) {
		this.shippingDate = shippingDate;
	}

}
