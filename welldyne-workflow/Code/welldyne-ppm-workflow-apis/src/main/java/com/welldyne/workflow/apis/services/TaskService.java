package com.welldyne.workflow.apis.services;

import com.welldyne.workflow.entities.jbpm.task.GetTaskListRequest;
import com.welldyne.workflow.entities.jbpm.task.SubmitTaskRequest;

public interface TaskService {

	public Object queryTask(GetTaskListRequest getTaskListRequest);

	public Object getTaskDetails(String containerId, String taskId);

	public void submitTask(SubmitTaskRequest submitTaskRequest);
}
