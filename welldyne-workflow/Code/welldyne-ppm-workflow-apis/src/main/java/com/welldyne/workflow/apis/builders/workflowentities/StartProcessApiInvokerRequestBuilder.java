package com.welldyne.workflow.apis.builders.workflowentities;

import com.welldyne.workflow.entities.jbpm.process.StartProcessRequest;
import com.welldyne.workflowapiinvokers.invokers.requests.StartProcessApiInvokerRequest;

public class StartProcessApiInvokerRequestBuilder {

	public static StartProcessApiInvokerRequest with(StartProcessRequest startProcessRequest) {
		var startProcessApiInvokerRequest = new StartProcessApiInvokerRequest();

		startProcessApiInvokerRequest.setContainerId(startProcessRequest.getContainerId());
		startProcessApiInvokerRequest.setCorrelationId(startProcessRequest.getCorrelationId());
		startProcessApiInvokerRequest.setProcessDefId(startProcessRequest.getProcessDefId());
		startProcessApiInvokerRequest.setProcessVariables(startProcessRequest.getProcessInput());

		return startProcessApiInvokerRequest;

	}

}
