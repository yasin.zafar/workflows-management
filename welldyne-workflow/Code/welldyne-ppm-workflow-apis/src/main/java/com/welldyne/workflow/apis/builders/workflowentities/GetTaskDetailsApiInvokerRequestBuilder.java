package com.welldyne.workflow.apis.builders.workflowentities;

import com.welldyne.workflowapiinvokers.invokers.requests.GetTaskDetailsApiInvokerRequest;

public class GetTaskDetailsApiInvokerRequestBuilder {

	public static GetTaskDetailsApiInvokerRequest with(String containerId, String taskId, boolean isInputDataRequired) {

		var invokerRequest = new GetTaskDetailsApiInvokerRequest();
		invokerRequest.setContainerId(containerId);
		invokerRequest.setTaskId(taskId);
		invokerRequest.setInputDataRequired(isInputDataRequired);

		return invokerRequest;
	}

}
