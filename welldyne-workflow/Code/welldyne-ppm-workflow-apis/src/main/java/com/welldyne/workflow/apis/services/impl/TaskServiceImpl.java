package com.welldyne.workflow.apis.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.welldyne.workflow.apis.builders.workflowentities.GetTaskDetailsApiInvokerRequestBuilder;
import com.welldyne.workflow.apis.builders.workflowentities.QueryTaskApiInvokerRequestBuilder;
import com.welldyne.workflow.apis.builders.workflowentities.SubmitTaskApiInvokerRequestBuilder;
import com.welldyne.workflow.apis.services.TaskService;
import com.welldyne.workflow.entities.jbpm.Page;
import com.welldyne.workflow.entities.jbpm.task.GetTaskListRequest;
import com.welldyne.workflow.entities.jbpm.task.SubmitTaskRequest;
import com.welldyne.workflowapiinvokers.invokers.task.GetTaskDetailsApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.task.QueryTaskApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.task.SubmitTaskApiInvoker;

@Service
public class TaskServiceImpl extends BaseBusinessService implements TaskService {

	@Autowired
	QueryTaskApiInvoker queryTaskApiInvoker;

	@Autowired
	GetTaskDetailsApiInvoker getTaskDetailsApiInvoker;

	@Autowired
	SubmitTaskApiInvoker submitTaskApiInvoker;

	@Override
	public Object queryTask(GetTaskListRequest getTaskListRequest) {

		var mapper = "RawList";
		if (getTaskListRequest.getQueryRequest()
				.getPage() == null) {
			Page page = new Page();
			page.setPageNumber(0);
			page.setPageSize(Integer.MAX_VALUE);
			getTaskListRequest.getQueryRequest()
					.setPage(page);
		}
		var queryTaskApiInvokerRequest = QueryTaskApiInvokerRequestBuilder.with(getTaskListRequest, mapper);
		var tasksList = queryTaskApiInvoker.invoke(queryTaskApiInvokerRequest);

		return tasksList;
	}

	@Override
	public Object getTaskDetails(String containerId, String taskId) {
		var isInputDataRequired = true;
		var invokerRequest = GetTaskDetailsApiInvokerRequestBuilder.with(containerId, taskId, isInputDataRequired);

		return getTaskDetailsApiInvoker.invoke(invokerRequest);
	}

	@Override
	public void submitTask(SubmitTaskRequest submitTaskRequest) {
		var autoProgress = true;
		var invokerRequest = SubmitTaskApiInvokerRequestBuilder.with(submitTaskRequest, autoProgress);

		submitTaskApiInvoker.invoke(invokerRequest);
	}

}
