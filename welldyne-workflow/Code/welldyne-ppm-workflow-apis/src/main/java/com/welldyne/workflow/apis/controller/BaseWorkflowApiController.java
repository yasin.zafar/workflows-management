package com.welldyne.workflow.apis.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.welldyne.common.frameworks.layers.RestApiController;
import com.welldyne.common.frameworks.logging.Loggable;

@RestApiController
@Loggable
public class BaseWorkflowApiController {

	protected final Logger logger;

	public BaseWorkflowApiController() {
		logger = LogManager.getLogger(this.getClass());
	}

}
