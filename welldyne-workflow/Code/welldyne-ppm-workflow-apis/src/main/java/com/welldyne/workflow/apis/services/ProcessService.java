package com.welldyne.workflow.apis.services;

import com.welldyne.workflow.entities.jbpm.process.StartProcessRequest;

public interface ProcessService {

	public String startProcess(StartProcessRequest startProcessRequest);
}
