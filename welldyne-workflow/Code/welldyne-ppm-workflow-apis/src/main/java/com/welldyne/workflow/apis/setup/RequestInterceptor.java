package com.welldyne.workflow.apis.setup;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * 
 * @author nilofarmew
 *
 */
@Component
public class RequestInterceptor implements HandlerInterceptor {

	protected final Logger logger = LogManager.getLogger(this.getClass());

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		updateLogTransactionId(request);
		return true;
	}

	/**
	 * Adds session id or thread it in log thread context.
	 * 
	 * @param request
	 */
	private void updateLogTransactionId(HttpServletRequest request) {
		String transactionId = request.getHeader("transactionId");
		if (!StringUtils.isEmpty(transactionId)) {
			ThreadContext.put("transactionId", transactionId);
		} else {
			ThreadContext.put("transactionId", String.valueOf(Thread.currentThread().getId()));
		}
	}

}
