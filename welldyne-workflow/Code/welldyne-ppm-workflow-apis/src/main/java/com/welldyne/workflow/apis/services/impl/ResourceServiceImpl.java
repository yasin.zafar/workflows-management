package com.welldyne.workflow.apis.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.welldyne.workflow.apis.services.ResourceService;
import com.welldyne.workflowapiinvokers.invokers.process.GetContainersApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.requests.BaseWorkflowApiInvokerRequest;

@Service
public class ResourceServiceImpl extends BaseBusinessService implements ResourceService {

	@Autowired
	GetContainersApiInvoker getContainersApiInvoker;

	@Override
	public Object getAllContainers() {

		var containers = getContainersApiInvoker.invoke(new BaseWorkflowApiInvokerRequest());
		System.out.println(containers);

		return containers;
	}

}
