package com.welldyne.workflow.apis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.welldyne.workflow.apis.services.ResourceService;

@RestController
@RequestMapping("/")
public class ResourceController extends BaseWorkflowApiController {

	@Autowired
	ResourceService resourceService;

	@GetMapping(value = "container/all")
	@ResponseBody
	public Object getAllContainers() {

		return resourceService.getAllContainers();
	}

	@GetMapping(value = "hello")
	@ResponseBody
	public Object testController() {

		return "Working fine bro";
	}
}
