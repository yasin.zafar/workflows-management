package com.welldyne.workflow.apis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.welldyne.workflow.apis.services.ProcessService;
import com.welldyne.workflow.entities.jbpm.process.StartProcessRequest;

@RestController
@RequestMapping("process")
public class ProcessController extends BaseWorkflowApiController {

	@Autowired
	ProcessService processService;

	@PostMapping(value = "/start-process-instance")
	@ResponseBody
	public String startProcessInstance(@RequestBody StartProcessRequest startProcessRequest) {

		var processId = processService.startProcess(startProcessRequest);
		return processId;
	}
}
