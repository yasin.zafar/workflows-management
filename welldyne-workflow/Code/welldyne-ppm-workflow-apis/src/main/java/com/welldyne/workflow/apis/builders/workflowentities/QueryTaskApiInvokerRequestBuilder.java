package com.welldyne.workflow.apis.builders.workflowentities;

import java.util.ArrayList;

import com.welldyne.workflow.entities.jbpm.task.GetTaskListRequest;
import com.welldyne.workflowapiinvokers.invokers.requests.QueryParams;
import com.welldyne.workflowapiinvokers.invokers.requests.QueryTaskApiInvokerRequest;

public class QueryTaskApiInvokerRequestBuilder {

	public static QueryTaskApiInvokerRequest with(GetTaskListRequest getTaskListRequest, String mapper) {
		var queryTaskApiInvokerRequest = new QueryTaskApiInvokerRequest();
		queryTaskApiInvokerRequest.setQueryName(getTaskListRequest.getQueryName());
		queryTaskApiInvokerRequest.setContainerId(getTaskListRequest.getContainerId());
		queryTaskApiInvokerRequest.setMapper(mapper);

		var queryRequest = getTaskListRequest.getQueryRequest();
		var queryFilter = new com.welldyne.workflowapiinvokers.invokers.requests.QueryRequest();
		queryFilter.setOrderAsc(queryRequest.getOrderAsc());
		queryFilter.setOrderBy(queryRequest.getOrderBy());
		var queryParams = new ArrayList<QueryParams>();
		queryRequest.getFilter()
				.forEach(param -> {
					var queryParam = new QueryParams();
					queryParam.setColumn(param.getColumn());
					queryParam.setOperator(param.getOperator()
							.toString());
					queryParam.setValues(param.getValues());

					queryParams.add(queryParam);
				});

		queryFilter.setQueryParams(queryParams);
		queryTaskApiInvokerRequest.setQueryFilter(queryFilter);

		queryTaskApiInvokerRequest.setPageNumber(queryRequest.getPage()
				.getPageNumber());
		queryTaskApiInvokerRequest.setPageSize(queryRequest.getPage()
				.getPageSize());

		return queryTaskApiInvokerRequest;
	}

}
