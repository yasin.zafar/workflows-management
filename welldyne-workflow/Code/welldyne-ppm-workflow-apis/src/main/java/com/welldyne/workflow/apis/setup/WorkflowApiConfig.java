package com.welldyne.workflow.apis.setup;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
@ComponentScan("com.welldyne")
@PropertySources({ @PropertySource("classpath:workflow-apis-configuration.properties"), @PropertySource("classpath:workflow-apis-configuration-${env}.properties") })
public class WorkflowApiConfig extends WebMvcConfigurationSupport {

}
