package com.welldyne.workflow.apis.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.welldyne.workflow.apis.builders.workflowentities.StartProcessApiInvokerRequestBuilder;
import com.welldyne.workflow.apis.services.ProcessService;
import com.welldyne.workflow.entities.jbpm.process.StartProcessRequest;
import com.welldyne.workflowapiinvokers.invokers.process.StartProcessApiInvoker;

@Service
public class ProcessServiceImpl extends BaseBusinessService implements ProcessService {

	@Autowired
	StartProcessApiInvoker startProcessApiInvoker;

	@Override
	public String startProcess(StartProcessRequest startProcessRequest) {

		var startProcessApiInvokerRequest = StartProcessApiInvokerRequestBuilder.with(startProcessRequest);
		var processId = startProcessApiInvoker.invoke(startProcessApiInvokerRequest);

		return processId;
	}

}
