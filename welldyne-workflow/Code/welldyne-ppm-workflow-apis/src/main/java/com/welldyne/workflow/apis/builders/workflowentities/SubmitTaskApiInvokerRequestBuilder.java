package com.welldyne.workflow.apis.builders.workflowentities;

import com.welldyne.workflow.entities.jbpm.task.SubmitTaskRequest;
import com.welldyne.workflowapiinvokers.invokers.requests.SubmitTaskApiInvokerRequest;

public class SubmitTaskApiInvokerRequestBuilder {

	public static SubmitTaskApiInvokerRequest with(SubmitTaskRequest submitTaskRequest, boolean autoProgress) {

		var invokerRequest = new SubmitTaskApiInvokerRequest();

		invokerRequest.setTaskInput(submitTaskRequest.getTaskInput());
		invokerRequest.setContainerId(submitTaskRequest.getContainerId());
		invokerRequest.setTaskId(submitTaskRequest.getTaskId());
		invokerRequest.setTargetUser(submitTaskRequest.getTargetUser());
		invokerRequest.setAutoProgress(autoProgress);

		return invokerRequest;
	}

}
