package com.welldyne.workflow.apis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.welldyne.workflow.apis.services.TaskService;
import com.welldyne.workflow.entities.jbpm.task.GetTaskListRequest;
import com.welldyne.workflow.entities.jbpm.task.SubmitTaskRequest;

@RestController
@RequestMapping("task")
public class TaskController extends BaseWorkflowApiController {

	@Autowired
	TaskService taskService;

	@PostMapping(value = "list")
	@ResponseBody
	public Object queryTasks(@RequestBody GetTaskListRequest getTaskListRequest) {
		return taskService.queryTask(getTaskListRequest);
	}

	@GetMapping(value = "/{containerId}/{taskId}")
	@ResponseBody
	public Object getTaskDetails(@PathVariable String containerId, @PathVariable String taskId) {
		return taskService.getTaskDetails(containerId, taskId);
	}

	@PutMapping(value = "submit")
	@ResponseBody
	public void submitTask(@RequestBody SubmitTaskRequest submitTaskRequest) {
		taskService.submitTask(submitTaskRequest);
	}
}
