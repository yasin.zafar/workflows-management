package com.welldyne.workflow.apis.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.welldyne.common.frameworks.layers.BusinessService;
import com.welldyne.common.frameworks.logging.Loggable;

@Loggable
@BusinessService
public abstract class BaseBusinessService {

	protected final Logger logger;

	public BaseBusinessService() {
		logger = LogManager.getLogger(this.getClass());
	}
}
