package com.welldyne.workflowapiinvokers.invokers.base;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.welldyne.common.frameworks.exceptionhandling.DataAccessException;
import com.welldyne.common.frameworks.exceptionhandling.Error;
import com.welldyne.common.frameworks.restapiinvoker.RestApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.requests.BaseWorkflowApiInvokerRequest;

/**
 *
 * 
 *
 * @param <TInvokerRequest>
 * @param <TApiResponse>
 */
public abstract class BaseWorkflowApiInvoker<TInvokerRequest extends BaseWorkflowApiInvokerRequest, TApiResponse> extends RestApiInvoker<TInvokerRequest, TApiResponse> {

	@Override
	public TApiResponse invoke(TInvokerRequest invokerRequest) {
		try {
			TApiResponse response = super.invoke(invokerRequest);
			return response;
		} catch (HttpServerErrorException e) {
			List<Error> errors = new ArrayList<Error>();
			var errorBody = e.getResponseBodyAsString();
			try {
				errors = objectMapper.readValue(errorBody, new TypeReference<List<Error>>() {
				});
			} catch (Exception e1) {
				throw new DataAccessException(e);
			}
			throw new DataAccessException(e).withErrors(errors);
		}

	}

	@Override
	protected HttpHeaders getHeaders(TInvokerRequest request) {
		String transactionId = ThreadContext.get("transactionId");
		HttpHeaders headers = new HttpHeaders();
		headers.add("transactionId", transactionId);

		String plainCreds = "wbadmin:wbadmin";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder()
				.encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		headers.add("Authorization", "Basic " + base64Creds);
		headers.add("content-type", "application/json");
		headers.add("Accept", "application/json");
		return headers;
	}

}
