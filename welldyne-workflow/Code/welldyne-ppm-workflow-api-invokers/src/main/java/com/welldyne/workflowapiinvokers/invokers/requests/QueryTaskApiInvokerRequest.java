package com.welldyne.workflowapiinvokers.invokers.requests;

public class QueryTaskApiInvokerRequest extends BaseWorkflowApiInvokerRequest {

	private String containerId;
	private String queryName;
	private String mapper;
	private int pageNumber;
	private int pageSize;

	private QueryRequest queryFilter;

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getQueryName() {
		return queryName;
	}

	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}

	public String getMapper() {
		return mapper;
	}

	public void setMapper(String mapper) {
		this.mapper = mapper;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public QueryRequest getQueryFilter() {
		return queryFilter;
	}

	public void setQueryFilter(QueryRequest queryFilter) {
		this.queryFilter = queryFilter;
	}

}
