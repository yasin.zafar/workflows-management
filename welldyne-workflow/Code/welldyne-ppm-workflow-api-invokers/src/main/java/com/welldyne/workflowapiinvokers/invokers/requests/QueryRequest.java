package com.welldyne.workflowapiinvokers.invokers.requests;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QueryRequest {

	@JsonProperty("order-by")
	private String orderBy;

	@JsonProperty("order-asc")
	private String orderAsc;

	@JsonProperty("query-params")
	List<QueryParams> queryParams = new ArrayList<>();

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderAsc() {
		return orderAsc;
	}

	public void setOrderAsc(String orderAsc) {
		this.orderAsc = orderAsc;
	}

	public List<QueryParams> getQueryParams() {
		return queryParams;
	}

	public void setQueryParams(List<QueryParams> queryParams) {
		this.queryParams = queryParams;
	}

}
