package com.welldyne.workflowapiinvokers.invokers.requests;

public class StartProcessApiInvokerRequest extends BaseWorkflowApiInvokerRequest {

	private String containerId;
	private String processDefId;
	private String correlationId;

	private Object processVariables;

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getProcessDefId() {
		return processDefId;
	}

	public void setProcessDefId(String processDefId) {
		this.processDefId = processDefId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public Object getProcessVariables() {
		return processVariables;
	}

	public void setProcessVariables(Object processVariables) {
		this.processVariables = processVariables;
	}

}
