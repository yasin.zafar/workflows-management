package com.welldyne.workflowapiinvokers.invokers.process;

import org.springframework.stereotype.Repository;

import com.welldyne.common.frameworks.restapiinvoker.RestApi;
import com.welldyne.workflowapiinvokers.invokers.base.BaseWorkflowApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.requests.BaseWorkflowApiInvokerRequest;

@Repository
public class GetContainersApiInvoker extends BaseWorkflowApiInvoker<BaseWorkflowApiInvokerRequest, Object> {

	@Override
	protected RestApi getApi() {
		return RestApi.GET_CONTAINERS;
	}

}
