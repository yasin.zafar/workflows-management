package com.welldyne.workflowapiinvokers.invokers.requests;

public class GetTaskDetailsApiInvokerRequest extends BaseWorkflowApiInvokerRequest {

	private String containerId;
	private String taskId;
	private boolean inputDataRequired;

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public boolean isInputDataRequired() {
		return inputDataRequired;
	}

	public void setInputDataRequired(boolean inputDataRequired) {
		this.inputDataRequired = inputDataRequired;
	}

}
