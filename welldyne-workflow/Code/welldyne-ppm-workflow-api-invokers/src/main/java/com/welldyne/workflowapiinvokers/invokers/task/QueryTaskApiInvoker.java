package com.welldyne.workflowapiinvokers.invokers.task;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.welldyne.common.frameworks.restapiinvoker.RestApi;
import com.welldyne.workflowapiinvokers.invokers.base.BaseWorkflowApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.requests.QueryTaskApiInvokerRequest;

@Repository
public class QueryTaskApiInvoker extends BaseWorkflowApiInvoker<QueryTaskApiInvokerRequest, String> {

	@Override
	protected RestApi getApi() {
		return RestApi.GET_WORK_ITEMS;
	}

	@Override
	protected Map<String, Object> getQueryParameters(QueryTaskApiInvokerRequest request) {
		Map<String, Object> param = new HashMap<>();
		param.put("containerId", request.getContainerId());
		param.put("queryName", request.getQueryName());
		param.put("page", request.getPageNumber());
		param.put("pagesize", request.getPageSize());
		param.put("mapper", request.getMapper());

		return param;
	}

	@Override
	protected Object getRequestBody(QueryTaskApiInvokerRequest request) {
		return request.getQueryFilter();
	}

}
