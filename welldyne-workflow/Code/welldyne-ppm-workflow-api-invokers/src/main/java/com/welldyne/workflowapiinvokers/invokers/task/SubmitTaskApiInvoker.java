package com.welldyne.workflowapiinvokers.invokers.task;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.welldyne.common.frameworks.restapiinvoker.RestApi;
import com.welldyne.workflowapiinvokers.invokers.base.BaseWorkflowApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.requests.SubmitTaskApiInvokerRequest;

@Repository
public class SubmitTaskApiInvoker extends BaseWorkflowApiInvoker<SubmitTaskApiInvokerRequest, String> {

	@Override
	protected RestApi getApi() {
		return RestApi.SUBMIT_TASK;
	}

	@Override
	protected Map<String, Object> getQueryParameters(SubmitTaskApiInvokerRequest request) {
		Map<String, Object> param = new HashMap<>();
		param.put("containerId", request.getContainerId());
		param.put("taskId", request.getTaskId());
		param.put("user", request.getTargetUser());
		param.put("auto-progress", request.isAutoProgress());

		return param;
	}

	@Override
	protected Object getRequestBody(SubmitTaskApiInvokerRequest request) {
		return request.getTaskInput();
	}
}
