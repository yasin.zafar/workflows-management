package com.welldyne.workflowapiinvokers.invokers.task;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.welldyne.common.frameworks.restapiinvoker.RestApi;
import com.welldyne.workflowapiinvokers.invokers.base.BaseWorkflowApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.requests.GetTaskDetailsApiInvokerRequest;

@Repository
public class GetTaskDetailsApiInvoker extends BaseWorkflowApiInvoker<GetTaskDetailsApiInvokerRequest, String> {

	@Override
	protected RestApi getApi() {
		return RestApi.GET_WORK_ITEM_DETAIL;
	}

	@Override
	protected Map<String, Object> getQueryParameters(GetTaskDetailsApiInvokerRequest request) {
		Map<String, Object> param = new HashMap<>();
		param.put("containerId", request.getContainerId());
		param.put("taskId", request.getTaskId());
		param.put("withInputData", request.isInputDataRequired());

		return param;
	}

}
