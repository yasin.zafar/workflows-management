package com.welldyne.workflowapiinvokers.invokers.requests;

public class GetContainersApiInvokerRequest extends BaseWorkflowApiInvokerRequest {

	private String containerId;
	private String processDefId;
	private String correlationId;

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getProcessDefId() {
		return processDefId;
	}

	public void setProcessDefId(String processDefId) {
		this.processDefId = processDefId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

}
