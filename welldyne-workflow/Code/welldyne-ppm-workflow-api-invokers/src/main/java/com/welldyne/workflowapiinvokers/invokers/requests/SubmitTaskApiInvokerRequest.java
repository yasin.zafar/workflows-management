package com.welldyne.workflowapiinvokers.invokers.requests;

public class SubmitTaskApiInvokerRequest extends BaseWorkflowApiInvokerRequest {

	private Object taskInput;
	private String containerId;
	private String taskId;
	private String targetUser;
	private boolean autoProgress;

	public Object getTaskInput() {
		return taskInput;
	}

	public void setTaskInput(Object taskInput) {
		this.taskInput = taskInput;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTargetUser() {
		return targetUser;
	}

	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

	public boolean isAutoProgress() {
		return autoProgress;
	}

	public void setAutoProgress(boolean autoProgress) {
		this.autoProgress = autoProgress;
	}

}
