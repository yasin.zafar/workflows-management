package com.welldyne.workflowapiinvokers.invokers.requests;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QueryParams<T> {

	@JsonProperty("cond-column")
	private String column;

	@JsonProperty("cond-operator")
	private String operator;

	@JsonProperty("cond-values")
	private List<T> values = new ArrayList<>();

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public List<T> getValues() {
		return values;
	}

	public void setValues(List<T> values) {
		this.values = values;
	}

}
