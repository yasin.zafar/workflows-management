package com.welldyne.workflowapiinvokers.invokers.process;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.welldyne.common.frameworks.restapiinvoker.RestApi;
import com.welldyne.workflowapiinvokers.invokers.base.BaseWorkflowApiInvoker;
import com.welldyne.workflowapiinvokers.invokers.requests.StartProcessApiInvokerRequest;

@Repository
public class StartProcessApiInvoker extends BaseWorkflowApiInvoker<StartProcessApiInvokerRequest, String> {

	@Override
	protected RestApi getApi() {
		return RestApi.START_PROCESS;
	}

	@Override
	protected Map<String, Object> getQueryParameters(StartProcessApiInvokerRequest request) {
		Map<String, Object> param = new HashMap<>();
		param.put("correlationId", request.getCorrelationId());
		param.put("containerId", request.getContainerId());
		param.put("processDefId", request.getProcessDefId());

		return param;
	}

	@Override
	protected Object getRequestBody(StartProcessApiInvokerRequest request) {
		return request.getProcessVariables();
	}

}
