#!/usr/bin/env bash

## Verifying Tomcat Installation Directory ##
if [[ ! -d /usr/share/tomcat ]];
 then
  amazon-linux-extras install tomcat8.5
  service tomcat start
 else
  rm -Rf /usr/share/tomcat/webapps/*
fi

## Creating Logs Directory  ##

if [[ ! -d /AppLogs ]];
 then
  mkdir /AppLogs
 else
  echo "Logs Directory Exists"
fi

chown -R tomcat:tomcat /AppLogs

## Identifying Env Name ##
REGION=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone | sed 's/\(.*\)[a-z]/\1/'`
AWS_INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
env=`aws ec2 describe-tags --region $REGION --filters "Name=resource-id,Values=$AWS_INSTANCE_ID" "Name=key,Values=environment" --output text | cut -f5`

## Copying SQL Drivers ##
aws s3 cp s3://$env-member-portal-env-s3-use1/dbdrivers /usr/share/tomcat/lib --recursive

## Copying Application Config Files ##
aws s3 cp s3://$env-member-portal-env-s3-use1/conf /usr/share/tomcat/conf --recursive

## Copying JKS file ##
aws s3 cp s3://$env-member-portal-env-s3-use1/SSO/keystore/wellviewkeystore.jks /usr/share/tomcat/conf/

## Install Yum Packages ##
yum install jq -y


## Adding Cron Job For Log folder date updating ##
rm -f /etc/crontab
cat > /etc/crontab <<- "EOF"
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root
HOME=/

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name command to be executed
1 0 * * * root sh /tmp/monitoring.sh >/dev/null 2>&1
EOF
chown -R root:root /etc/crontab
service crond restart

## Installing Rapid7 Agent ##
aws s3 cp s3://$env-member-portal-env-s3-use1/Rapid7/monitoring.sh /tmp
aws s3 cp s3://$env-member-portal-env-s3-use1/Rapid7/Rapid7.tar /tmp
chmod +x /tmp/monitoring.sh
/tmp/monitoring.sh

## Installing AppDynamics Agent ##
aws s3 cp s3://$env-member-portal-env-s3-use1/AppDynamics/appdynamics.sh /tmp
aws s3 cp s3://$env-member-portal-env-s3-use1/AppDynamics/AppDynamics.tar /tmp
chmod +x /tmp/appdynamics.sh
/tmp/appdynamics.sh
