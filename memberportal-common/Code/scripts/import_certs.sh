#!/usr/bin/env bash

## Installing WellDyne Issuing certificate ##
welldynissuing=$(keytool -list -keystore /usr/lib/jvm/jre/lib/security/cacerts -storepass changeit | grep welldyn-issuing | wc -l)
if [ $welldynissuing -eq 0 ]; then
  keytool -importcert -alias welldyn-issuing -keystore /usr/lib/jvm/jre/lib/security/cacerts -storepass changeit -noprompt -file /usr/share/tomcat/conf/WellDyneRxIssuingCA-01.cer
  echo "certificate successfully imported"
else
  echo "certificate already exist"
fi

## Installing WellDyne Root certificate ##
welldynroot=$(keytool -list -keystore /usr/lib/jvm/jre/lib/security/cacerts -storepass changeit | grep welldyn-root | wc -l)
if [ $welldynroot -eq 0 ]; then
  keytool -importcert -alias welldyn-root -keystore /usr/lib/jvm/jre/lib/security/cacerts -storepass changeit -noprompt -file /usr/share/tomcat/conf/WellDyneRxRootCA-01.cer
  echo "certificate successfully imported"
else
  echo "certificate already exist"
fi