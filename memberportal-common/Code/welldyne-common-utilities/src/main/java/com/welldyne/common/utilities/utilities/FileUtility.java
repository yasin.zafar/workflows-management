package com.welldyne.common.utilities.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author nilofarmew
 *
 */
public class FileUtility {

	private FileUtility() {

	}

	private static final String tempFolderPath = "/tmp/";

	/**
	 * 
	 * @param cvs
	 * @param fileName
	 * @return List of files form List of CSV
	 */
	public static List<File> csvToFile(List<String> cvs, String fileName) {

		List<File> contentFiles = new ArrayList<>(0);

		int i = 0;
		for (String contentString : cvs) {
			String zipFileName = new StringBuilder(fileName).append("_").append(i).append(".csv").toString();
			String zipFilePath = tempFolderPath + zipFileName;
			PrintWriter writer1;
			try {
				writer1 = new PrintWriter(new OutputStreamWriter(new FileOutputStream(zipFilePath), "UTF-8"));
				writer1.print(contentString);
				writer1.close();
			} catch (UnsupportedEncodingException | FileNotFoundException e) {
				e.printStackTrace();
			}
			contentFiles.add(new File(zipFilePath));
			i++;
		}
		return contentFiles;
	}

	public static File byteArrayToFileTempFolder(byte[] bs, String fileIdentifier, String extension) {
		String pathname = tempFolderPath + fileIdentifier + "." + extension;
		return byteArrayToFile(bs, pathname);
	}

	public static File byteArrayToFile(byte[] bs, String pathname) {
		File file = new File(pathname);
		try {
			FileUtils.writeByteArrayToFile(file, bs);
		} catch (Exception e) {
			throw new RuntimeException("Error while creating file", e);
		}
		return file;
	}

	/**
	 * This method is used to generate the response with "application/zip"
	 * attachment for the given content strings and convert these content to
	 * specified format file.
	 * 
	 * @param response
	 * @param contentStrings
	 * @param fileName
	 * @param fileFormat
	 * @return
	 */
	public static HttpServletResponse generateZipResponse(HttpServletResponse response, final List<String> contentStrings, final String fileName, final String fileFormat) {

		response.setContentType("application/zip");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".zip");

		List<File> contentFiles = new ArrayList<>(0);

		int i = 0;
		for (String contentString : contentStrings) {
			String zipFileName = new StringBuilder(fileName).append("_").append(i).append(fileFormat).toString();
			String zipFilePath = tempFolderPath + zipFileName;
			PrintWriter writer1;
			try {
				writer1 = new PrintWriter(new OutputStreamWriter(new FileOutputStream(zipFilePath), "UTF-8"));
				writer1.print(contentString);
				writer1.close();
			} catch (UnsupportedEncodingException | FileNotFoundException e) {

			}
			contentFiles.add(new File(zipFilePath));
			i++;
		}

		HttpServletResponse filesToZipResponse = filesToZip(response, contentFiles);
		for (File file : contentFiles) {
			file.delete();
		}

		return filesToZipResponse;
	}

	/**
	 * This method is used to binds the files in folder and compressed that
	 * folder.
	 * 
	 * @param response
	 * @param files
	 * @return
	 */
	public static HttpServletResponse filesToZip(HttpServletResponse response, List<File> files) {
		// Create a buffer for reading the files
		byte[] buf = new byte[1024];
		// create the ZIP file
		try {
			ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
			// compress the files
			for (File file : files) {
				FileInputStream in = new FileInputStream(file.getAbsolutePath());
				// add ZIP entry to output stream
				out.putNextEntry(new ZipEntry(file.getName()));
				// transfer bytes from the file to the ZIP file
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				// complete the entry
				out.closeEntry();
				in.close();
			}
			// complete the ZIP file
			out.close();
		} catch (java.io.IOException e) {
			throw new RuntimeException("Failed to create zip", e);
		}
		return response;
	}

	/**
	 * This method is used to binds the files in folder and compressed that
	 * folder.
	 * 
	 * @param response
	 * @param files
	 * @return
	 */
	public static File filesToZip(List<File> files, String fileName) {
		try {
			File zip = new File(tempFolderPath + fileName + ".zip");
			FileOutputStream fos = new FileOutputStream(zip);
			ZipOutputStream zos = new ZipOutputStream(fos);
			for (File file : files) {
				FileInputStream fis = new FileInputStream(file.getAbsolutePath());
				ZipEntry zipEntry = new ZipEntry(file.getName());
				zos.putNextEntry(zipEntry);

				byte[] bytes = new byte[1024];
				int length;
				while ((length = fis.read(bytes)) >= 0) {
					zos.write(bytes, 0, length);
				}
				zos.closeEntry();
				fis.close();
			}
			zos.close();
			fos.close();
			return zip;
		} catch (java.io.IOException e) {
			throw new RuntimeException("failed to create zip", e);
		}
	}

	/**
	 * This method is used create zip files from byte array
	 * 
	 * @param files
	 * @param fileName
	 * @return
	 */
	public static File byteArrayToPDFZipFile(List<byte[]> files, String fileName) {
		return byteArraysToZip(files, fileName, "pdf");
	}

	/**
	 * This method is used to binds the files in folder and compressed that
	 * folder.
	 * 
	 * @param response
	 * @param files
	 * @return
	 */
	public static File byteArraysToZip(List<byte[]> files, String fileName, String fileExtension) {
		try {
			File zip = new File(tempFolderPath + fileName + ".zip");
			FileOutputStream fos = new FileOutputStream(zip);
			ZipOutputStream zos = new ZipOutputStream(fos);
			int count = 1;
			for (byte[] file : files) {
				ByteArrayInputStream fis = new ByteArrayInputStream(file);
				ZipEntry zipEntry = new ZipEntry(fileName + "_" + count++ + "." + fileExtension);
				zos.putNextEntry(zipEntry);

				byte[] bytes = new byte[1024];
				int length;
				while ((length = fis.read(bytes)) >= 0) {
					zos.write(bytes, 0, length);
				}
				zos.closeEntry();
				fis.close();
			}
			zos.close();
			fos.close();
			return zip;
		} catch (java.io.IOException e) {
			throw new RuntimeException("Failed to create zip", e);
		}
	}

	/**
	 * This method is used to binds the files in folder and compressed that
	 * folder.
	 * 
	 * @param response
	 * @param files
	 * @return
	 */
	public static HttpServletResponse byteToZip(HttpServletResponse response, List<byte[]> files, String fileName) {
		try {
			try (ServletOutputStream outputStream = response.getOutputStream()) {
				ZipOutputStream out = new ZipOutputStream(outputStream);

				for (byte[] file : files) {
					out.putNextEntry(new ZipEntry(fileName));
					out.write(file);
				}
				out.closeEntry();
			}
		} catch (Exception e) {
			throw new RuntimeException("Error creating zip", e);
		}

		return response;
	}

	public static String saveFileOnServer(MultipartFile multipartFile, String folderName, String userIdentifier) {
		try {
			String directoryName = System.getProperty("catalina.base") + tempFolderPath + folderName;
			String userContextPath = userIdentifier + "/" + System.currentTimeMillis();
			String finalFileDirectory = directoryName + "/" + userContextPath;
			File directory = new File(finalFileDirectory);
			if (!directory.exists()) {
				directory.mkdirs();
			}
			File file = new File(finalFileDirectory + "/" + multipartFile.getOriginalFilename());
			multipartFile.transferTo(file);
			return userContextPath + "/" + file.getName();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public static byte[] convertInputStreamToByteArray(InputStream inputStream) {

		try {
			byte[] bytes = IOUtils.toByteArray(inputStream);
			return bytes;
		} catch (Exception e) {
			throw new RuntimeException("Error converting inputstream to byte[]", e);
		}

	}

	public static byte[] convertBase64EncodedStringToByteArray(String base64Encoded) {

		try {
			byte[] bytes = Base64.getDecoder().decode(base64Encoded.getBytes());
			return bytes;
		} catch (Exception e) {
			throw new RuntimeException("Error converting base64Encoded String to byte[]", e);
		}

	}

}
