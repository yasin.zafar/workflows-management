package com.welldyne.common.utilities.utilities;

import javax.servlet.http.HttpServletRequest;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class RequestUtility {

	private RequestUtility() {

	}

	public static boolean isWebRequest() {
		var request = getCurrentRequest();
		var device = getCurrentDevice(request);
		return device.isNormal();
	}

	public static boolean isMobileRequest() {
		var request = getCurrentRequest();
		var device = getCurrentDevice(request);
		if (device != null) {
			return !device.isNormal();
		}
		return false;
	}

	private static Device getCurrentDevice(HttpServletRequest request) {
		Device device = DeviceUtils.getCurrentDevice(request);
		return device;
	}

	public static HttpServletRequest getCurrentRequest() {
		var request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return request.getRequest();
	}

	public static String getCurrentRequestUri() {
		var request = getCurrentRequest();
		return request.getRequestURI();
	}

	public static boolean isAjaxCall() {
		var request = getCurrentRequest();
		var requestedWithHeader = request.getHeader("X-Requested-With");
		var isAjaxCall = "XMLHttpRequest".equals(requestedWithHeader);
		return isAjaxCall;
	}
}
