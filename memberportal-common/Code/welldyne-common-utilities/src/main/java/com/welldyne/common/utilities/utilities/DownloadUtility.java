package com.welldyne.common.utilities.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public class DownloadUtility {

	/**
	 * If there is single file, download single file or download zip
	 * 
	 * @param httpServletResponse
	 * @param csv
	 * @param downloadType
	 * @throws Exception
	 */
	public static void downloadCsvFile(HttpServletResponse httpServletResponse, List<String> csv, DownloadType downloadType) {

		String fileName = downloadType.getFileNamePrefix();

		List<File> files = FileUtility.csvToFile(csv, fileName);

		if (files.size() == 1) {
			downloadCsvFile(httpServletResponse, downloadType, fileName, files.get(0));
		} else {
			File zip = FileUtility.filesToZip(files, fileName);
			downloadZipFile(httpServletResponse, fileName, zip);
			zip.delete();
		}

		for (File file : files) {
			file.delete();
		}
	}

	private static void downloadZipFile(HttpServletResponse httpServletResponse, String fileName, File file) {

		httpServletResponse.setContentType("application/zip");
		httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".zip");

		downloadFile(httpServletResponse, file);
	}

	public static void downloadPdf(HttpServletResponse httpServletResponse, byte[] byteStream, DownloadType downloadType) {

		String fileName = downloadType.getFileNamePrefix();
		try {
			// To set "Content-Disposition" header
			// In case of CORS requests, browsers can only access few header
			httpServletResponse.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
			httpServletResponse.setHeader("Content-Type", "application/pdf");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".pdf\"");
			ServletOutputStream outputStream = httpServletResponse.getOutputStream();
			outputStream.write(byteStream);
			outputStream.flush();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private static void downloadCsvFile(HttpServletResponse httpServletResponse, DownloadType downloadType, String fileName, File file) {

		httpServletResponse.setContentType("application/text");
		httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");

		downloadFile(httpServletResponse, file);
	}

	/**
	 * Downloads file using httpServletResponse
	 * 
	 * @param httpServletResponse
	 * @param file
	 */
	private static void downloadFile(HttpServletResponse httpServletResponse, File file) {
		try {
			ServletOutputStream outputStream = httpServletResponse.getOutputStream();

			FileInputStream inputStream = new FileInputStream(file.getAbsolutePath());
			int len;

			byte[] buf = new byte[(int) file.length()];
			while ((len = inputStream.read(buf)) > 0) {
				outputStream.write(buf, 0, len);
			}
			inputStream.close();
			outputStream.close();
			outputStream.flush();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
