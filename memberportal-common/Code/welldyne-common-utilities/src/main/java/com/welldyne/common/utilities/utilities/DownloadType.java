package com.welldyne.common.utilities.utilities;

public enum DownloadType {

	CLAIM_SUMMARY("claimSummary", "Claims", "csv"),
	PRESCRIPTION_SUMMARY("prescriptionSummary", "Prescriptions", "csv"),
	ORDER_SUMMARY("orderSummary", "Orders", "csv"),
	ID_CARD("idCard", "IdCard", "pdf");

	private final String downloadTypeName;
	private final String fileNamePrefix;
	private final String fileType;

	DownloadType(String downloadTypeName, String fileNamePrefix, String fileType) {
		this.downloadTypeName = downloadTypeName;
		this.fileNamePrefix = fileNamePrefix;
		this.fileType = fileType;
	}

	public String getDownloadTypeName() {
		return downloadTypeName;
	}

	public String getFileNamePrefix() {
		return fileNamePrefix;
	}

	public String getFileType() {
		return fileType;
	}

}
