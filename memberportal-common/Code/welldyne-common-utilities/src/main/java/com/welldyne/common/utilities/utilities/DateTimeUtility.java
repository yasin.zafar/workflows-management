package com.welldyne.common.utilities.utilities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * 
 * @author nilofarmew
 *
 */
public class DateTimeUtility {

	private static final String AWS_COGNITO_DATE_FORMAT = "yyyy-MM-dd";

	/**
	 * rivate constructor to make builder class non instantiable and in turn static
	 */
	private DateTimeUtility() {

	}

	/**
	 * Converts LocalDate to string date of the given format
	 * 
	 * @param date
	 * @param format
	 * @return formated string date
	 */
	public static String formatDate(LocalDate date, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return date.format(formatter);
	}

	/**
	 * Converts string date to LocalDate using the given format
	 * 
	 * @param date
	 * @param format
	 * @return date of LocalDate type
	 */
	public static LocalDate getDate(String date, String format) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		LocalDate localDate = LocalDate.parse(date, formatter);

		return localDate;
	}

	/**
	 * Converts date from AWS Cognito string date
	 * 
	 * @param date
	 * @return date of LocalDate type
	 */
	public static LocalDate fromAWSCognitoDate(String date) {
		return getDate(date, AWS_COGNITO_DATE_FORMAT);
	}

	/**
	 * Converts date to AWS Cognito string date
	 * 
	 * @param localDate
	 * @return AWS Cognito formatted date
	 */
	public static String toAWSCognitoDate(LocalDate localDate) {
		return formatDate(localDate, AWS_COGNITO_DATE_FORMAT);
	}

	public static LocalDate convertXMLGregorianCalendarToLocalDate(XMLGregorianCalendar xmlGregorianCalendar) {
		LocalDate localDate = LocalDate.of(xmlGregorianCalendar.getYear(), xmlGregorianCalendar.getMonth(),
				xmlGregorianCalendar.getDay());
		return localDate;
	}

	public static XMLGregorianCalendar convertLocalDateToXMLGregorianCalendar(LocalDate localDate) {
		XMLGregorianCalendar xmlGregorianCalendar = null;
		try {
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(localDate.toString());
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return xmlGregorianCalendar;
	}

}
