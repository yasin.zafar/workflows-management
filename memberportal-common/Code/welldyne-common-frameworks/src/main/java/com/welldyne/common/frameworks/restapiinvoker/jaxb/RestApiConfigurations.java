
package com.welldyne.common.frameworks.restapiinvoker.jaxb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Apis" type="{http://www.welldyne.com/common/frameworks/restapiinvoker/jaxb}RestApis" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apis"
})
@XmlRootElement(name = "RestApiConfigurations")
public class RestApiConfigurations
    implements Serializable
{

    private final static long serialVersionUID = -1L;
    @XmlElement(name = "Apis", required = true)
    protected List<RestApis> apis;

    /**
     * Gets the value of the apis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the apis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RestApis }
     * 
     * 
     */
    public List<RestApis> getApis() {
        if (apis == null) {
            apis = new ArrayList<RestApis>();
        }
        return this.apis;
    }

}
