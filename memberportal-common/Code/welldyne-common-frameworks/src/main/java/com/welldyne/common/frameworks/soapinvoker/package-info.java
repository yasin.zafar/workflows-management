/**
 */
/**
 * This contains soap api invoker framework related classes.
 * 
 * @author nilofarmew
 *
 */
package com.welldyne.common.frameworks.soapinvoker;