package com.welldyne.common.frameworks.cognito;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminConfirmSignUpRequest;
import com.amazonaws.services.cognitoidp.model.AdminDeleteUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminDeleteUserResult;
import com.amazonaws.services.cognitoidp.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminSetUserPasswordRequest;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.cognitoidp.model.SignUpRequest;
import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import com.welldyne.common.frameworks.logging.Loggable;

/**
 * 
 * @author nilofarmew
 *
 */
@Service
@Loggable
public class AWSCognitoServiceImpl implements AWSCognitoService {

	@Autowired
	private AWSCognitoIdentityProvider awsCognitoIdentityProvider;

	@Value("${aws.cognito.client-id}")
	private String clientId;

	@Value("${aws.cognito.userpool-id}")
	private String userPoolId;

	@Override
	public AWSCognitoAuthenticationResult authenticateUser(String username, String password) {
		var authParameters = new HashMap<String, String>();
		authParameters.put("USERNAME", username);
		authParameters.put("PASSWORD", password);

		var adminInitiateAuthRequest = new AdminInitiateAuthRequest().withClientId(clientId)
				.withUserPoolId(userPoolId)
				.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
				.withAuthParameters(authParameters);
		var adminInitiateAuthResult = awsCognitoIdentityProvider.adminInitiateAuth(adminInitiateAuthRequest);

		var authenticationResult = AWSCognitoAuthenticationResultMapper.map(adminInitiateAuthResult);
		return authenticationResult;

	}

	@Override
	public boolean userExists(String username) {
		try {
			var adminGetUserRequest = new AdminGetUserRequest().withUsername(username)
					.withUserPoolId(userPoolId);
			awsCognitoIdentityProvider.adminGetUser(adminGetUserRequest);
			return true;
		} catch (UserNotFoundException e) {
			return false;
		}
	}

	@Override
	public String createUser(String username, String password) {
		var signUpRequest = new SignUpRequest().withClientId(clientId)
				.withUsername(username)
				.withPassword(password);
		var signUpResult = awsCognitoIdentityProvider.signUp(signUpRequest);
		return signUpResult.getUserSub();
	}

	@Override
	public void confirmUserSignUp(String username) {
		var adminGetUserRequest = new AdminGetUserRequest().withUsername(username)
				.withUserPoolId(userPoolId);
		var adminGetUserResult = awsCognitoIdentityProvider.adminGetUser(adminGetUserRequest);
		if ("CONFIRMED".equalsIgnoreCase(adminGetUserResult.getUserStatus())) {
			return;
		}
		var adminConfirmSignUpRequest = new AdminConfirmSignUpRequest().withUsername(username)
				.withUserPoolId(userPoolId);
		awsCognitoIdentityProvider.adminConfirmSignUp(adminConfirmSignUpRequest);
	}

	@Override
	public boolean resetPassword(String username, String password) {
		var adminSetUserPasswordRequest = new AdminSetUserPasswordRequest().withUsername(username)
				.withPassword(password)
				.withPermanent(true)
				.withUserPoolId(userPoolId);
		awsCognitoIdentityProvider.adminSetUserPassword(adminSetUserPasswordRequest);
		return true;
	}

	@Override
	public AdminDeleteUserResult deleteUser(String username) {
		var adminDeleteUserRequest = new AdminDeleteUserRequest().withUsername(username)
				.withUserPoolId(userPoolId);
		return awsCognitoIdentityProvider.adminDeleteUser(adminDeleteUserRequest);
	}

}
