package com.welldyne.common.frameworks.cognito;

import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;

/**
 * 
 * @author nilofarmew
 *
 */
public class AWSCognitoAuthenticationResultMapper {

	/**
	 * 
	 */
	private AWSCognitoAuthenticationResultMapper() {
	}

	/**
	 * 
	 * @param result
	 * @return
	 */
	public static AWSCognitoAuthenticationResult map(AdminInitiateAuthResult result) {

		AWSCognitoAuthenticationResult authenticationResult = new AWSCognitoAuthenticationResult();
		authenticationResult.setAuthenticated(true);
		authenticationResult.setAccessToken(result.getAuthenticationResult().getAccessToken());
		authenticationResult.setRefreshToken(result.getAuthenticationResult().getRefreshToken());
		authenticationResult.setExpiresIn(result.getAuthenticationResult().getExpiresIn());
		authenticationResult.setSession(result.getSession());

		return authenticationResult;
	}
	
	
	/**
	 * 
	 * @param authenticates
	 * @return
	 */
	public static AWSCognitoAuthenticationResult map(boolean authenticates) {
		AWSCognitoAuthenticationResult authenticationResult = new AWSCognitoAuthenticationResult();
		authenticationResult.setAuthenticated(authenticates);
		return authenticationResult;
	}


}
