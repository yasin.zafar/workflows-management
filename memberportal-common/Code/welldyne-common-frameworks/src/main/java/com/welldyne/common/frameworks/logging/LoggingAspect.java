package com.welldyne.common.frameworks.logging;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.stereotype.Component;

/**
 * This is an AspectJ Aspect, being enabled using Spring AOP used to define
 * logging advices.
 * 
 * @author nilofarmew
 *
 */
@Component
@Aspect
public class LoggingAspect {

	private static final Logger logger = LogManager.getLogger(LoggingAspect.class);

	/**
	 * This is an around advice which logs entry and exit of all the public
	 * method of a class on which @Loggable annotation defined.
	 *
	 * @param joinPoint
	 *            the join point
	 * @return Object
	 * @throws Throwable
	 *             the throwable
	 */
	@Around("@within(com.welldyne.common.frameworks.logging.Loggable)")
	public Object logAnnotateClass(ProceedingJoinPoint joinPoint) throws Throwable {
		LoggableMethod loggableMethod = getLoggableMethod(joinPoint);
		logger.debug("Entering {} with arguments {} as {} ", loggableMethod.getMethodSignature(), loggableMethod.getMethodParameterNames(), loggableMethod.getMethodParameterValues());
		Instant start = Instant.now();
		Object result = joinPoint.proceed();
		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish).toMillis();
		logger.debug("Exiting {} with result {}. Total time took {} millisecods.", loggableMethod.getMethodSignature(), result, timeElapsed);
		return result;

	}

	/**
	 * This extracts the method related information from JoinPoint.
	 *
	 * @param joinPoint
	 *            the join point
	 * @return LoggableMethod the information related to method and parameters.
	 * 
	 */
	private LoggableMethod getLoggableMethod(JoinPoint joinPoint) {

		CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();

		LoggableMethod loggableMethod = new LoggableMethod();
		loggableMethod.setMethodSignature(codeSignature.toLongString());
		loggableMethod.setMethodParameterNames(Arrays.toString(codeSignature.getParameterNames()));
		loggableMethod.setMethodParameterValues(Arrays.toString(joinPoint.getArgs()));

		return loggableMethod;
	}

}
