package com.welldyne.common.frameworks.validateaddress;

/**
 * 
 * @author nilofarmew
 *
 */
public interface ValidateAddressService {

	/**
	 * Validates address and suggests correct address
	 * 
	 * @param address
	 * @return AddressValidationResult
	 */
	public AddressValidationResult validate(AddressValidationRequest address);

}