package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

/**
 * This is thrown by business layers.
 * 
 * @author nilofarmew
 *
 */
public class BusinessException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8189433174339714682L;

	public BusinessException() {
		super();
	}

	public BusinessException(Throwable e) {
		super(e);
	}

	public BusinessException(Throwable e, boolean isLogged) {
		super(e, isLogged);
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(String message, Throwable e) {
		super(message, e);
	}

	public BusinessException(List<Error> errors) {
		super(errors);
	}

	public BusinessException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public BusinessException(List<Error> errors, String message) {
		super(errors, message);
	}

	public BusinessException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public BusinessException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public BusinessException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

}
