package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class DataAccessDeniedException extends ComponentControllerException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataAccessDeniedException() {
		super();
	}

	public DataAccessDeniedException(Throwable e) {
		super(e);
	}

	public DataAccessDeniedException(Throwable e, boolean isLogged) {
		super(e, isLogged);
	}

	public DataAccessDeniedException(String message) {
		super(message);
	}

	public DataAccessDeniedException(String message, Throwable e) {
		super(message, e);
	}

	public DataAccessDeniedException(List<Error> errors) {
		super(errors);
	}

	public DataAccessDeniedException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public DataAccessDeniedException(List<Error> errors, String message) {
		super(errors, message);
	}

	public DataAccessDeniedException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public DataAccessDeniedException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public DataAccessDeniedException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

}
