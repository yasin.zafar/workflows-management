package com.welldyne.common.frameworks.notifications.beans;

/**
 * This is the bean class which represents the response structure of
 * notification senders
 * 
 * @author Puneet.Kumar
 *
 * @param <T>
 */
public class SenderResponse<T> {

	private T response;

	/**
	 * @return the response
	 */
	public T getResponse() {
		return response;
	}

	/**
	 * @param response
	 *            the response to set
	 */
	public void setResponse(T response) {
		this.response = response;
	}

}
