package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

/**
 * This exception is thrown by the controller of rest APIs.
 * 
 * @author nilofarmew
 *
 */
public class RestApiControllerException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3284252265764417686L;

	public RestApiControllerException() {
		super();
	}

	public RestApiControllerException(Throwable e) {
		super(e);
	}

	public RestApiControllerException(Throwable e, boolean isLogged) {
		super(e, isLogged);
	}

	public RestApiControllerException(String message) {
		super(message);
	}

	public RestApiControllerException(String message, Throwable e) {
		super(message, e);
	}

	public RestApiControllerException(List<Error> errors) {
		super(errors);
	}

	public RestApiControllerException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public RestApiControllerException(List<Error> errors, String message) {
		super(errors, message);
	}

	public RestApiControllerException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public RestApiControllerException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public RestApiControllerException with(Consumer<RestApiControllerException> builder) {
		builder.accept(this);
		return this;
	}
}
