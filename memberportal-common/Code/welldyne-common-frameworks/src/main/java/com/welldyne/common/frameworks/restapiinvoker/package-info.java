/**
 */
/**
 * This contains rest api invoker framework related classes.
 * 
 * @author nilofarmew
 *
 */
package com.welldyne.common.frameworks.restapiinvoker;