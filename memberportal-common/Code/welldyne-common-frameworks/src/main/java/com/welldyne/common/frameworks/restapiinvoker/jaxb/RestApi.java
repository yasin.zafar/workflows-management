
package com.welldyne.common.frameworks.restapiinvoker.jaxb;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RestApi complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestApi"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.welldyne.com/common/frameworks/restapiinvoker/jaxb}RestApiName" /&gt;
 *       &lt;attribute name="method" use="required" type="{http://www.welldyne.com/common/frameworks/restapiinvoker/jaxb}RestApiMethodType" /&gt;
 *       &lt;attribute name="url" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="timeout" type="{http://www.w3.org/2001/XMLSchema}int" default="3000" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestApi")
public class RestApi
    implements Serializable
{

    private final static long serialVersionUID = -1L;
    @XmlAttribute(name = "name", required = true)
    protected RestApiName name;
    @XmlAttribute(name = "method", required = true)
    protected RestApiMethodType method;
    @XmlAttribute(name = "url", required = true)
    protected String url;
    @XmlAttribute(name = "timeout")
    protected Integer timeout;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link RestApiName }
     *     
     */
    public RestApiName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestApiName }
     *     
     */
    public void setName(RestApiName value) {
        this.name = value;
    }

    /**
     * Gets the value of the method property.
     * 
     * @return
     *     possible object is
     *     {@link RestApiMethodType }
     *     
     */
    public RestApiMethodType getMethod() {
        return method;
    }

    /**
     * Sets the value of the method property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestApiMethodType }
     *     
     */
    public void setMethod(RestApiMethodType value) {
        this.method = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the timeout property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getTimeout() {
        if (timeout == null) {
            return  3000;
        } else {
            return timeout;
        }
    }

    /**
     * Sets the value of the timeout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeout(Integer value) {
        this.timeout = value;
    }

}
