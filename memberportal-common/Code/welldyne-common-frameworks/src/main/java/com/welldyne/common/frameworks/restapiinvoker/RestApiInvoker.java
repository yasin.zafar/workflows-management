package com.welldyne.common.frameworks.restapiinvoker;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.welldyne.common.frameworks.exceptionhandling.RestApiInvokerException;
import com.welldyne.common.frameworks.layers.ApiInvoker;
import com.welldyne.common.frameworks.logging.Loggable;

/**
 * This is base class for API invoker providing implementation for invoke
 * method. This is designed using template method design pattern with invoke()
 * method as the template method.
 * 
 * @author nilofarmew
 *
 * @param <TInvokerRequest>
 *            This is the request that is will be used to create inputs like
 *            header, query parameters and request body while calling API over
 *            HTTP.
 * @param <TApiResponse>
 *            This is the response expected from the rest call.
 * 
 */
@Loggable
@ApiInvoker
public abstract class RestApiInvoker<TInvokerRequest, TApiResponse> {

	private final Logger logger;

	@Autowired
	@Qualifier("invokerObjectMapper")
	protected ObjectMapper objectMapper;

	protected RestApiInvoker() {
		logger = LogManager.getLogger(this.getClass());
	}

	/**
	 * This invokes the API associated with the subclass implementation of this
	 * invoker class
	 * 
	 * @param invokerRequest
	 *            this is the request object that is will be used to create
	 *            inputs like header, query parameters and request body while
	 *            calling API over HTTP.
	 * @return the API response
	 */
	public TApiResponse invoke(TInvokerRequest invokerRequest) {
		RestApi api = getApi();

		HttpEntity<Object> requestEntity = getHttpRequest(invokerRequest, api.getMethod());
		Map<String, Object> queryParameters = getQueryParameters(invokerRequest);

		ResponseEntity<TApiResponse> responseEntity = invokeRestApi(api, requestEntity, queryParameters);

		if (!(HttpStatus.OK.equals(responseEntity.getStatusCode()) || HttpStatus.CREATED.equals(responseEntity.getStatusCode()))) {
			logger.error("Exception occured while calling rest api " + api.getName() + " " + api.getUrl());
			throw new RestApiInvokerException(responseEntity, "Exception occured while calling rest api " + api.getName() + " " + api.getUrl(), new Exception());
		}

		TApiResponse response = objectMapper.convertValue(responseEntity.getBody(), getResponseType());
		return response;

	}

	/**
	 * Calls the rest API with give request and query parameters.
	 * 
	 * @param api
	 *            the API that is needed to be invoked.
	 * @param requestEntity
	 *            the request that is supposed to be sent over HTTP
	 * @param queryParameters
	 *            query parameters to append to service request
	 * @return API response
	 */
	@SuppressWarnings("unchecked")
	private ResponseEntity<TApiResponse> invokeRestApi(RestApi api, HttpEntity<Object> requestEntity, Map<String, Object> queryParameters) {

		String apiName = api.getName();
		String apiUrl = api.getUrl();
		HttpMethod apiMethod = api.getMethod();

		logger.debug("BaseRestApiInvoker invoking " + apiName + " " + apiUrl);
		ResponseEntity<TApiResponse> responseEntity;
		RestTemplate restTemplate = new RestTemplate();

		if (api.getTimeout() != 0) {
			logger.debug("api time out " + api.getTimeout());
			setConnectionTimeOut(restTemplate, api.getTimeout());
		}
		if (!CollectionUtils.isEmpty(queryParameters)) {

			responseEntity = (ResponseEntity<TApiResponse>) restTemplate.exchange(apiUrl, apiMethod, requestEntity, getResponseType().getRawClass(), queryParameters);

		} else {

			responseEntity = (ResponseEntity<TApiResponse>) restTemplate.exchange(apiUrl, apiMethod, requestEntity, getResponseType().getRawClass());

		}

		return responseEntity;
	}

	/**
	 * Subclass of this class must provide implementation of this to provide the
	 * api that needs to be invoked.
	 * 
	 * @return the api that is needed to be invoked.
	 */
	protected abstract RestApi getApi();

	/**
	 * Subclass of this class must provide implementation for header creation,
	 * since expected header for can change from api to api.
	 * 
	 * @param request
	 * @return header that will used while invoking the api.
	 */
	protected abstract HttpHeaders getHeaders(TInvokerRequest request);

	/**
	 * Override this method if query parameters and path variables are needed to
	 * pass to the api.
	 * 
	 * @param request
	 * @return query parameters need to be added to the request.
	 */
	protected Map<String, Object> getQueryParameters(TInvokerRequest request) {
		return new HashMap<String, Object>();
	}

	/**
	 * Override this if a request body is needed to be posted.
	 * 
	 * This method is only being used by POST, DELETE and PUT type of APIs.
	 * 
	 * There is not point implementing this for GET APIs since this will not be
	 * used while invoking GET type of APIs.
	 * 
	 * @param request
	 *            the invoker request that can be used to create request body is
	 *            needed.
	 * @return the request body to send to the API
	 */
	protected Object getRequestBody(TInvokerRequest request) {
		return null;
	}

	/**
	 * This creates HTTP request for the provided HTTP method.
	 * 
	 * @param invokerRequest
	 *            used to get header and request body.
	 * @param apiMethod
	 *            method type of the API for which request is being created.
	 * @return the HTTP request with header and request body appended.
	 */
	private HttpEntity<Object> getHttpRequest(TInvokerRequest invokerRequest, HttpMethod apiMethod) {
		HttpHeaders headers = getHeaders(invokerRequest);

		HttpEntity<Object> requestEntity;
		if (apiMethod == HttpMethod.POST || apiMethod == HttpMethod.DELETE || apiMethod == HttpMethod.PUT) {
			Object requestBody = getRequestBody(invokerRequest);
			if (requestBody != null) {
				requestEntity = new HttpEntity<>(requestBody, headers);
			} else {
				requestEntity = new HttpEntity<>(headers);
			}
		} else {
			requestEntity = new HttpEntity<>(headers);
		}
		return requestEntity;
	}

	/**
	 * This provides actual type of the generic response type TResponse.
	 * 
	 * @return the actual type of API response referred as generic TResponse
	 *         here.
	 */
	private JavaType getResponseType() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		Type[] typeArr = genericSuperclass.getActualTypeArguments();
		Type type = typeArr[1];
		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.constructType(type);
	}

	/**
	 * This sets timeout in the api invocation.
	 * 
	 * @param restTemplate
	 *            the restTemplate that is being used to invoke this api.
	 * @param timeInMilliseconds
	 *            timeout in milliseconds for the api.
	 */
	private void setConnectionTimeOut(RestTemplate restTemplate, int timeInMilliseconds) {
		SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		simpleClientHttpRequestFactory.setConnectTimeout(timeInMilliseconds);
		restTemplate.setRequestFactory(simpleClientHttpRequestFactory);
	}

}
