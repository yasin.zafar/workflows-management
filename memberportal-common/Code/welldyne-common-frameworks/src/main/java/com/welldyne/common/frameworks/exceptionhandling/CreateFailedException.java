package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

/**
 * 
 * @author nilofarmew
 *
 */
public class CreateFailedException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4426981521907761443L;

	public CreateFailedException() {
		super();
	}

	public CreateFailedException(Throwable e) {
		super(e);
	}

	public CreateFailedException(String message) {
		super(message);
	}

	public CreateFailedException(String message, Throwable e) {
		super(message, e);
	}

	public CreateFailedException(List<Error> errors) {
		super(errors);
	}

	public CreateFailedException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public CreateFailedException(List<Error> errors, String message) {
		super(errors, message);
	}

	public CreateFailedException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public CreateFailedException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public CreateFailedException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public CreateFailedException with(Consumer<CreateFailedException> builder) {
		builder.accept(this);
		return this;
	}
}
