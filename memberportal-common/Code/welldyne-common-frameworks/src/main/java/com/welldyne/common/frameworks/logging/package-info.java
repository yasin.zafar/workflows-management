/**
 */
/**
 * This contains classes related to logging framework.
 * 
 * @author nilofarmew
 *
 */
package com.welldyne.common.frameworks.logging;