package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class S3Exception extends BusinessException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5374947671912468577L;

	public S3Exception() {
		super();
	}

	public S3Exception(Throwable e) {
		super(e);
	}

	public S3Exception(String message) {
		super(message);
	}

	public S3Exception(String message, Throwable e) {
		super(message, e);
	}

	public S3Exception(List<Error> errors) {
		super(errors);
	}

	public S3Exception(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public S3Exception(List<Error> errors, String message) {
		super(errors, message);
	}

	public S3Exception(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public S3Exception withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public S3Exception withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public S3Exception with(Consumer<S3Exception> builder) {
		builder.accept(this);
		return this;
	}

}
