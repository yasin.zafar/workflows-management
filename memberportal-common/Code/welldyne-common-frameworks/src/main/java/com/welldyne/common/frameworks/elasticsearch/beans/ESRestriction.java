package com.welldyne.common.frameworks.elasticsearch.beans;

import com.welldyne.common.frameworks.elasticsearch.annotations.ESFieldValueMatchType;

public class ESRestriction {

	private Object value;
	private ESFieldValueMatchType matchType;
	
	private ESRestriction() {
		
	}

	public Object getValue() {
		return value;
	}

	public ESRestriction withValue(Object value) {
		this.value = value;
		return this;
	}

	public ESFieldValueMatchType getMatchType() {
		return matchType;
	}

	public ESRestriction withMatchType(ESFieldValueMatchType matchType) {
		this.matchType = matchType;
		return this;
	}
	
	public static ESRestriction create() {
		return new ESRestriction();
	}

}
