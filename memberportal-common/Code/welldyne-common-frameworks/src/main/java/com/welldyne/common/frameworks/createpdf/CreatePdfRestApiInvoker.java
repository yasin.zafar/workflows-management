package com.welldyne.common.frameworks.createpdf;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Repository;

import com.welldyne.common.frameworks.restapiinvoker.RestApi;
import com.welldyne.common.frameworks.restapiinvoker.RestApiInvoker;

@Repository
public class CreatePdfRestApiInvoker extends RestApiInvoker<CreatePdfRestApiInvokerRequest, String> {

	@Override
	protected RestApi getApi() {
		return RestApi.CREATE_PDF;
	}

	@Override
	protected HttpHeaders getHeaders(CreatePdfRestApiInvokerRequest request) {
		HttpHeaders headers = new HttpHeaders();

		return headers;
	}

	@Override
	protected Object getRequestBody(CreatePdfRestApiInvokerRequest request) {
		return request.getPayload();
	}

}
