package com.welldyne.common.frameworks.elasticsearch.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation must be used on ESEntity type sub classes.
 * 
 * @author nilofarmew
 *
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface ESIndex {

	/**
	 * This is name of the index field is bean is being defined for. One or more
	 * indexes can be provided for the same entity as long as the structure of
	 * the indexes are exactly the same.
	 * 
	 * @return
	 */
	String[] name();
}
