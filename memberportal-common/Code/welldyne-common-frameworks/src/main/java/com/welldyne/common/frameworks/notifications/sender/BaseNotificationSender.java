package com.welldyne.common.frameworks.notifications.sender;

import com.welldyne.common.frameworks.notifications.beans.Notification;

public abstract class BaseNotificationSender<T extends Notification> implements NotificationSender<T> {

	public void auditNotification(T notification) {

	}
}
