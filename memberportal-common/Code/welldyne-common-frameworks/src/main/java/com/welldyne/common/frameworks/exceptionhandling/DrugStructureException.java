package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class DrugStructureException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2024654437351238411L;

	public DrugStructureException() {
		super();
	}

	public DrugStructureException(Throwable e) {
		super(e);
	}

	public DrugStructureException(String message) {
		super(message);
	}

	public DrugStructureException(String message, Throwable e) {
		super(message, e);
	}

	public DrugStructureException(List<Error> errors) {
		super(errors);
	}

	public DrugStructureException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public DrugStructureException(List<Error> errors, String message) {
		super(errors, message);
	}

	public DrugStructureException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public DrugStructureException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public DrugStructureException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public DrugStructureException with(Consumer<DrugStructureException> builder) {
		builder.accept(this);
		return this;
	}

}
