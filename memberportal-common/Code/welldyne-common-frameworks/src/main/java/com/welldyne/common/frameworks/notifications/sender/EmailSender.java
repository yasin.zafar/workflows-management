package com.welldyne.common.frameworks.notifications.sender;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.welldyne.common.frameworks.exceptionhandling.NotificationSenderException;
import com.welldyne.common.frameworks.notifications.beans.EmailNotification;
import com.welldyne.common.frameworks.notifications.beans.SenderResponse;
import com.welldyne.common.frameworks.notifications.utilities.SendEmailUtility;
import com.welldyne.common.frameworks.s3.S3Utility;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * 
 * This class performs all the functionalities required to send email
 * notifications.
 * 
 * @author Puneet.Kumar
 *
 */
@Component
public class EmailSender extends BaseNotificationSender<EmailNotification> {

	@Autowired
	private S3Utility s3Utility;

	@Autowired
	private SendEmailUtility mailUtility;

	@Autowired
	private Configuration freeMarkerConfiguration;

	@Override
	public SenderResponse<Void> sendNotification(EmailNotification emailNotification) throws NotificationSenderException {
		try {

			InputStream is = s3Utility.getS3File(emailNotification.getTemplatePath());
			InputStreamReader isr = new InputStreamReader(is);

			Template template = new Template("", isr, freeMarkerConfiguration);

			StringWriter writer = new StringWriter();
			template.process(emailNotification.getNotificationData(), writer);
			String emailBody = writer.toString();
			String subject = emailNotification.getNotificationData()
					.get("subject")
					.toString();
			if (emailNotification.getAttachments() != null) {
				mailUtility.sendSeSMail(emailNotification.getTo()[0], emailBody, subject, emailNotification.getAttachments()
						.toArray(new String[0]));
			} else {
				mailUtility.sendSeSMail(emailNotification.getTo()[0], emailBody, emailNotification.getNotificationData()
						.get("subject")
						.toString(), null);
			}
			return new SenderResponse<Void>();
		} catch (TemplateException | IOException e) {
			throw new NotificationSenderException(e.getMessage());
		}

	}

}
