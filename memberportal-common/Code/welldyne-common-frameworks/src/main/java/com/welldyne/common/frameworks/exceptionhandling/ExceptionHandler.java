package com.welldyne.common.frameworks.exceptionhandling;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * 
 * This is being done by ExceptionHandler in the exception handling framework.
 * ExceptionHandler is an AspectJ Aspect.
 * 
 * This applies different AspectJ Advice to different layers. It decides layers
 * by the Layer specific annotation.
 * 
 * 
 * @author nilofarmew
 *
 */
@Aspect
@Component
public class ExceptionHandler {

	private static final Logger logger = LogManager.getLogger(ExceptionHandler.class);

	/**
	 * This Log the exception and Converts any exception thrown from a Component
	 * Controller to ComponentControllerException
	 * 
	 * @param joinPoint
	 *            the method that threw the exception
	 * @param e
	 *            the exception throw from the joinPoint
	 * @throws ComponentControllerException
	 */
	@AfterThrowing(pointcut = "@within(com.welldyne.common.frameworks.layers.ComponentController)", throwing = "e")
	public void handleComponentControllerException(JoinPoint joinPoint, Throwable e) throws ComponentControllerException {
		if ((e instanceof BaseException)) {
			BaseException baseException = ((BaseException) e);
			if (!baseException.isLogged()) {
				logger.error("Exception occured in {} method of Component Controller layer", getMethodName(joinPoint), e);
				baseException.setLogged(true);
			}
			if (e instanceof ComponentControllerException) {
				throw (ComponentControllerException) e;
			}
			throw new ComponentControllerException(baseException.getErrors(), baseException);
		}
		logger.error("Exception occured in {} method of Component Controller layer", getMethodName(joinPoint), e);
		throw new ComponentControllerException(e, true);
	}

	/**
	 * This Log the exception and Converts any exception thrown from a View
	 * Controller to ViewControllerException
	 * 
	 * @param joinPoint
	 *            the method that threw the exception
	 * @param e
	 *            the exception throw from the joinPoint
	 * @throws ViewControllerException
	 */
	@AfterThrowing(pointcut = "@within(com.welldyne.common.frameworks.layers.ViewController)", throwing = "e")
	public void handleViewControllerException(JoinPoint joinPoint, Throwable e) throws ViewControllerException {
		if ((e instanceof BaseException)) {
			BaseException baseException = ((BaseException) e);
			if (!baseException.isLogged()) {
				logger.error("Exception occurred in {} method of View Controller layer", getMethodName(joinPoint), e);
				baseException.setLogged(true);
			}
			if (e instanceof ViewControllerException) {
				throw (ViewControllerException) e;
			}
			throw new ViewControllerException(baseException.getErrors(), baseException);
		}
		logger.error("Exception occurred in {} method of View Controller layer", getMethodName(joinPoint), e);
		throw new ViewControllerException(e, true);
	}

	/**
	 * 
	 * This Log the exception and Converts any exception thrown from a Rest Api
	 * Controller to RestApiControllerException
	 * 
	 * @param joinPoint
	 *            the method that threw the exception
	 * @param e
	 *            the exception throw from the joinPoint
	 * @throws RestApiControllerException
	 */
	@AfterThrowing(pointcut = "@within(com.welldyne.common.frameworks.layers.RestApiController)", throwing = "e")
	public void handleRestApiControllerException(JoinPoint joinPoint, Throwable e) throws RestApiControllerException {
		if ((e instanceof BaseException)) {
			BaseException baseException = ((BaseException) e);
			if (!baseException.isLogged()) {
				logger.error("Exception occured in {} method of Api Controller layer", getMethodName(joinPoint), e);
				baseException.setLogged(true);
			}
			if (e instanceof RestApiControllerException) {
				throw (RestApiControllerException) e;
			}
			throw new RestApiControllerException(baseException.getErrors(), baseException);
		}
		logger.error("Exception occured in {} method of Api Controller layer", getMethodName(joinPoint), e);
		throw new RestApiControllerException(e, true);
	}

	/**
	 * This Log the exception and Converts any exception thrown from a
	 * Presentation Layer to PresentationException
	 * 
	 * @param joinPoint
	 *            the method that threw the exception
	 * @param e
	 *            the exception throw from the joinPoint
	 * @throws PresentationException
	 */
	@AfterThrowing(pointcut = "@within(com.welldyne.common.frameworks.layers.PresentationService)", throwing = "e")
	public void handlePresentationServiceException(JoinPoint joinPoint, Throwable e) throws PresentationException {
		if ((e instanceof BaseException)) {
			BaseException baseException = ((BaseException) e);
			if (!baseException.isLogged()) {
				logger.error("Exception occurred in {} method of Presenttation layer", getMethodName(joinPoint), e);
				baseException.setLogged(true);
			}
			if (e instanceof PresentationException) {
				throw (PresentationException) e;
			}
			throw new PresentationException(baseException.getErrors(), baseException);
		}
		logger.error("Exception occurred in {} method of Presenttation layer", getMethodName(joinPoint), e);
		throw new PresentationException(e, true);
	}

	/**
	 * This Log the exception and Converts any exception thrown from a Business
	 * Layer to BusinessException
	 * 
	 * @param joinPoint
	 *            the method that threw the exception
	 * @param e
	 *            the exception throw from the joinPoint
	 * @throws BusinessException
	 */
	@AfterThrowing(pointcut = "@within(com.welldyne.common.frameworks.layers.BusinessService)", throwing = "e")
	public void handleBusinessServiceException(JoinPoint joinPoint, Throwable e) throws BusinessException {
		if ((e instanceof BaseException)) {
			BaseException baseException = ((BaseException) e);
			if (!baseException.isLogged()) {
				logger.error("Exception occurred in {} method of Business Service layer ", getMethodName(joinPoint), e);
				baseException.setLogged(true);
			}
			if (e instanceof BusinessException) {
				throw (BusinessException) e;
			}
			throw new BusinessException(baseException.getErrors(), baseException);
		}

		logger.error("Exception occurred in {} method of Business Service layer ", getMethodName(joinPoint), e);
		throw new BusinessException(e, true);
	}

	/**
	 * This Log the exception and Converts any exception thrown from a
	 * Repository Layer to DataAccessException
	 * 
	 * @param joinPoint
	 *            the method that threw the exception
	 * @param e
	 *            the exception throw from the joinPoint
	 * @throws DataAccessException
	 */
	@AfterThrowing(pointcut = "@within(com.welldyne.common.frameworks.layers.RepositoryService)", throwing = "e")
	public void handleRepositoryException(JoinPoint joinPoint, Throwable e) throws DataAccessException {
		if ((e instanceof BaseException)) {
			BaseException baseException = ((BaseException) e);
			if (!baseException.isLogged()) {
				logger.error("Exception occurred in {} method of Repository Service layer", getMethodName(joinPoint), e);
				baseException.setLogged(true);
			}

			if (e instanceof DataAccessException) {
				throw (DataAccessException) e;
			}
			throw new DataAccessException(baseException.getErrors(), baseException);
		}

		logger.error("Exception occurred in {} method of Repository Service layer", getMethodName(joinPoint), e);
		throw new DataAccessException(e, true);
	}

	/**
	 * This Log the exception and Converts any exception thrown from a Api
	 * Invoker to DataAccessException
	 * 
	 * @param joinPoint
	 *            the method that threw the exception
	 * @param e
	 *            the exception throw from the joinPoint
	 * @throws DataAccessException
	 */
	@AfterThrowing(pointcut = "@within(com.welldyne.common.frameworks.layers.ApiInvoker)", throwing = "e")
	public void handleInvokerException(JoinPoint joinPoint, Throwable e) throws DataAccessException {
		if ((e instanceof BaseException)) {
			BaseException baseException = ((BaseException) e);
			if (!baseException.isLogged()) {
				logger.error("Exception occurred in {} method of Service Invoker layer ", getMethodName(joinPoint), e);
				baseException.setLogged(true);
			}
			if (e instanceof DataAccessException) {
				throw (DataAccessException) e;
			}
			throw new DataAccessException(baseException.getErrors(), baseException);
		}
		logger.error("Exception occurred in {} method of Service Invoker layer ", getMethodName(joinPoint), e);
		throw new DataAccessException(e, true);
	}

	/**
	 * This method helps to prepare the class name and method name for a log.
	 *
	 * @param joinPoint
	 *            the join point
	 * @return String name of the method from joinPoint
	 */
	private String getMethodName(JoinPoint joinPoint) {
		return joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName() + "()";
	}
}
