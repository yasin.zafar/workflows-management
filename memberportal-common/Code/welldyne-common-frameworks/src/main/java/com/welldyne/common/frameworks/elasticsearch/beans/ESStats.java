package com.welldyne.common.frameworks.elasticsearch.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

/**
 * This is the object which will be returned by ESRepository as stats for both
 * count stats and sum stats.
 * 
 * @author nilofarmew
 *
 */
public class ESStats implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This is the category (name) of the stats.
	 */
	private String name;

	/**
	 * Count of records under this categroy.
	 */
	private int recordCount;

	/**
	 * Aggregation of summable fields for this stats where Key is the name of
	 * the aggregated field and value is its aggregated value.
	 */
	private Map<String, Double> aggregations;

	/**
	 * This is sub stats under this status. This can happen when there are 'set'
	 * type of fields in index which have parent-child relationship.
	 * 
	 * For example, status and state. There can be multiple statuses under one
	 * state.
	 */
	private List<ESStats> subStats;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the recordCount
	 */
	public int getRecordCount() {
		return recordCount;
	}

	/**
	 * @param recordCount
	 *            the recordCount to set
	 */
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	/**
	 * @return the stats
	 */
	public Map<String, Double> getAggregations() {
		return aggregations;
	}

	/**
	 * @param aggregations
	 *            the stats to set
	 */
	public void addAggregation(String name, Double value) {
		if (CollectionUtils.isEmpty(this.aggregations)) {
			this.aggregations = new HashMap<String, Double>();
		}
		this.aggregations.put(name, value);
	}

	/**
	 * @return the subStats
	 */
	public List<ESStats> getSubStats() {
		return subStats;
	}

	/**
	 * @param subStats
	 *            the subStats to set
	 */
	public void addSubStats(ESStats subStats) {
		if (CollectionUtils.isEmpty(this.subStats)) {
			this.subStats = new ArrayList<ESStats>();
		}
		this.subStats.add(subStats);
	}

}
