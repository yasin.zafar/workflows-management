package com.welldyne.common.frameworks.restapiinvoker;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;

import com.welldyne.common.frameworks.caching.Cache;
import com.welldyne.common.frameworks.exceptionhandling.SystemException;
import com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApiConfigurations;
import com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApiName;
import com.welldyne.common.frameworks.s3.S3Utility;

/**
 * This defines all the rest apis and provided ways to read api configurations.
 * 
 * @author nilofarmew
 *
 */
public enum RestApi {

	SAVE_USER(RestApiName.SAVE_USER),
	GET_MEMBER(RestApiName.GET_MEMBER),
	VALIDATE_ADDRESS(RestApiName.VALIDATE_ADDRESS),
	GET_CONTACTS(RestApiName.GET_CONTACTS),
	UPDATE_PATIENT_CONTACT(RestApiName.UPDATE_CONTACTS),
	GET_HEALTH_INFORMATION(RestApiName.GET_HEALTH_INFORMATION),
	UPDATE_HEALTH_INFORMATION(RestApiName.UPDATE_HEALTH_INFORMATION),
	GET_DRUG_ALLERGIES(RestApiName.GET_DRUG_ALLERGIES),
	GET_HEALTH_CONDITIONS(RestApiName.GET_HEALTH_CONDITIONS),
	CREATE_PATIENT(RestApiName.CREATE_PATIENT),
	SEARCH_MEMBERS(RestApiName.SEARCH_MEMBERS),
	CREATE_TICKET(RestApiName.CREATE_TICKET),
	GET_PRESCRIPTION_DETAIL(RestApiName.GET_PRESCRIPTION_DETAIL),
	GET_ALTERNATE_DRUGS(RestApiName.GET_ALTERNATE_DRUGS),
	SUBMIT_TRIAL_CLAIM(RestApiName.SUBMIT_TRIAL_CLAIM),
	GET_PATIENT_CONTACT(RestApiName.GET_PATIENT_CONTACT),
	GENERATE_ACCESS_TOKEN(RestApiName.GENERATE_ACCESS_TOKEN),
	SEARCH_MEMBER(RestApiName.SEARCH_MEMBER),
	RUN_TRIAL_CLAIM(RestApiName.RUN_TRIAL_CLAIM),
	GET_PATIENT_ADDRESSES(RestApiName.GET_PATIENT_ADDRESSES),
	LIST_SHIPPING(RestApiName.LIST_SHIPPING),
	LIST_PATIENT_CREDIT_CARD(RestApiName.LIST_PATIENT_CREDIT_CARD),
	CREATE_ORDER(RestApiName.CREATE_ORDER),
	ADD_PATIENT_ADDRESS(RestApiName.ADD_PATIENT_ADDRESS),
	UPDATE_PATIENT_ADDRESSES(RestApiName.UPDATE_PATIENT_ADDRESSES),
	DELETE_PATIENT_ADDRESS(RestApiName.DELETE_PATIENT_ADDRESS),
	GET_ORDER_DETAIL(RestApiName.GET_ORDER_DETAIL),
	GET_PATIENT_ADDRESS_BY_ID(RestApiName.GET_PATIENT_ADDRESS_BY_ID),
	GET_SHIPPING_OPTION_BY_ID(RestApiName.GET_SHIPPING_OPTION_BY_ID),
	UPDATE_PATIENT_DEFAULT_EMAIL(RestApiName.UPDATE_PATIENT_DEFAULT_EMAIL),
	GET_MEMBER_PLAN(RestApiName.GET_MEMBER_PLAN),
	GET_MEMBER_ID_CARD(RestApiName.GET_MEMBER_ID_CARD),
	GET_PATIENT_SAVED_CARD_BY_ID(RestApiName.GET_PATIENT_SAVED_CARD_BY_ID),
	GET_GROUP_CONTACT_DETAIL(RestApiName.GET_GROUP_CONTACT_DETAIL),
	GET_CLAIM_DETAIL(RestApiName.GET_CLAIM_DETAIL),
	ADD_PATIENT_PHONE(RestApiName.ADD_PATIENT_PHONE),
	UPDATE_PATIENT_PHONE(RestApiName.UPDATE_PATIENT_PHONE),
	GET_PATIENT_PHONES(RestApiName.GET_PATIENT_PHONES),
	GET_ALL_PHONE_TYPES(RestApiName.GET_ALL_PHONE_TYPES),
	DELETE_PATIENT_PHONE(RestApiName.DELETE_PATIENT_PHONE),
	SEARCH_PHARMACY(RestApiName.SEARCH_PHARMACY),
	GET_PHARMACIES(RestApiName.GET_PHARMACIES),
	SET_USER_DEFAULT_ADDRESS(RestApiName.SET_USER_DEFAULT_ADDRESS),
	GET_USER_DEFAULT_ADDRESS(RestApiName.GET_USER_DEFAULT_ADDRESS),
	SET_USER_DEFAULT_CARD(RestApiName.SET_USER_DEFAULT_CARD),
	DELETE_USER_CARD(RestApiName.DELETE_USER_CARD),
	DRUG_STRUCTURE(RestApiName.DRUG_STRUCTURE),
	GET_DRUG_STRUCTURE(RestApiName.GET_DRUG_STRUCTURE),
	SET_USER_DEFAULT_PHONE(RestApiName.SET_USER_DEFAULT_PHONE),
	GET_USER_PHONE_BY_ID(RestApiName.GET_USER_PHONE_BY_ID),
	SEARCH_ACCUMULATOR(RestApiName.SEARCH_ACCUMULATOR),
	ADD_PATIENT_CARD(RestApiName.ADD_PATIENT_CARD),
	AUTHENTICATE_LEGACY_USER(RestApiName.AUTHENTICATE_LEGACY_USER),
	VALIDATE_LEGACY_USER_NAME(RestApiName.VALIDATE_LEGACY_USER_NAME),
	GET_FORMULARY_NAME(RestApiName.GET_FORMULARY_NAME),
	GET_PATIENT_CAG(RestApiName.GET_PATIENT_CAG),
	CREATE_PDF(RestApiName.CREATE_PDF),
	GET_CONTAINERS(RestApiName.GET_CONTAINERS),
	START_PROCESS(RestApiName.START_PROCESS),
	GET_WORK_ITEMS(RestApiName.GET_WORK_ITEMS),
	GET_WORK_ITEM_DETAIL(RestApiName.GET_WORK_ITEM_DETAIL),
	SUBMIT_TASK(RestApiName.SUBMIT_TASK);

	RestApiName apiName;

	static S3Utility s3Utility;
	static String env;
	static String localEnv;
	static String configuration;

	private String url;
	private com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi restApi;

	RestApi(RestApiName apiName) {
		this.apiName = apiName;

	}

	/**
	 * This return name of the API
	 * 
	 * @return
	 */
	public String getName() {
		com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi api = getApi(apiName);
		return api.getName()
				.name();
	}

	/**
	 * This returns full URL of the API.
	 * 
	 * @return
	 */
	public String getUrl() {
		if (this.url == null) {
			com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi api = getApi(apiName);
			this.url = api.getUrl();
		}
		return url;
	}

	public String getRestApi() {
		com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi api = getApi(apiName);
		return api.getName()
				.name();
	}

	/**
	 * This return type of HTTP method the API supports.
	 * 
	 * @return
	 */
	public HttpMethod getMethod() {
		com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi api = getApi(apiName);
		return HttpMethod.valueOf(api.getMethod()
				.name());
	}

	/**
	 * @return timeout limit of the API.
	 */
	public int getTimeout() {
		com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi api = getApi(apiName);
		return api.getTimeout();
	}

	/**
	 * @param apiName
	 *            name of the api for which configuration is needed.
	 * @return returns API configuration.
	 */
	private com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi getApi(RestApiName apiName) {
		if (restApi == null) {
			this.restApi = getApis().get(apiName);
		}
		return this.restApi;
	}

	/**
	 * This first tries to get APIs from Cache, if not present then creates the
	 * map and put into Cache.
	 * 
	 * @return This returns map of APIs by name so that they are easier to
	 *         search.
	 */
	private Map<RestApiName, com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi> getApis() {

		Map<RestApiName, com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi> cachedConfig = Cache.REST_API_CONFIGURATION.get();

		if (cachedConfig != null && !cachedConfig.isEmpty()) {
			return cachedConfig;
		}
		Map<RestApiName, com.welldyne.common.frameworks.restapiinvoker.jaxb.RestApi> apis = new HashMap<>();

		RestApiConfigurations restApiConfigurations = getApiConfigurations();

		restApiConfigurations.getApis()
				.stream()
				.forEach(apiGroup -> {
					apiGroup.getApi()
							.forEach(api -> {
								api.setUrl(apiGroup.getBasePath()
										.concat(api.getUrl()));
								apis.put(api.getName(), api);
							});
				});

		Cache.REST_API_CONFIGURATION.put(apis);
		return apis;
	}

	/**
	 * This reads proper configuration for the enabled environments.
	 * 
	 * @return returns API configurations.
	 */
	private RestApiConfigurations getApiConfigurations() {
		InputStream inputStream = null;
		Logger log = LogManager.getLogger(this.getClass());
		log.debug("Rest Api path: " + configuration);
		log.debug("localEnv " + localEnv);
		log.debug("env " + env);
		if (localEnv.equalsIgnoreCase(env)) {
			inputStream = getClass().getResourceAsStream(configuration);
		} else {
			inputStream = s3Utility.getS3File(configuration);

		}
		try {
			log.debug("inputstream: " + inputStream);
			JAXBContext jaxbContext = JAXBContext.newInstance(RestApiConfigurations.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			RestApiConfigurations restApiConfigurations = (RestApiConfigurations) unmarshaller.unmarshal(inputStream);
			log.debug("config: " + restApiConfigurations.toString());
			inputStream.close();
			return restApiConfigurations;
		} catch (JAXBException | IOException e) {
			throw new SystemException(e);
		}
	}

}
