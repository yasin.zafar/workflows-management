package com.welldyne.common.frameworks.soapinvoker;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.welldyne.common.frameworks.s3.S3Utility;

@Service
public class SoapApiInitializer {

	@Value("${env}")
	private String env;

	@Value("${env.local}")
	private String localEnv;

	@Value("${configuration.soapApi.name}")
	private String configuration;

	@Autowired
	private S3Utility s3Utility;

	@PostConstruct
	public void postConstruct() {
		SoapApi.env = this.env;
		SoapApi.configuration = this.configuration;
		SoapApi.localEnv = this.localEnv;
		SoapApi.s3Utility = this.s3Utility;
	}
}
