
package com.welldyne.common.frameworks.restapiinvoker.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RestApiName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RestApiName"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="saveUser"/&gt;
 *     &lt;enumeration value="getHealthConditions"/&gt;
 *     &lt;enumeration value="getDrugAllergies"/&gt;
 *     &lt;enumeration value="createTicket"/&gt;
 *     &lt;enumeration value="getContacts"/&gt;
 *     &lt;enumeration value="searchMembers"/&gt;
 *     &lt;enumeration value="getMember"/&gt;
 *     &lt;enumeration value="createPatient"/&gt;
 *     &lt;enumeration value="getHealthInformation"/&gt;
 *     &lt;enumeration value="updateHealthInformation"/&gt;
 *     &lt;enumeration value="updateContacts"/&gt;
 *     &lt;enumeration value="getAlternateDrugs"/&gt;
 *     &lt;enumeration value="validateAddress"/&gt;
 *     &lt;enumeration value="getPrescriptionDetail"/&gt;
 *     &lt;enumeration value="validateAddress"/&gt;
 *     &lt;enumeration value="submitTrialClaim"/&gt;
 *     &lt;enumeration value="getPatientContact"/&gt;
 *     &lt;enumeration value="generateAccessToken"/&gt;
 *     &lt;enumeration value="searchMember"/&gt;
 *     &lt;enumeration value="runTrialClaim"/&gt;
 *     &lt;enumeration value="listShipping"/&gt;
 *     &lt;enumeration value="listPatientCreditCard"/&gt;
 *     &lt;enumeration value="getPatientAddresses"/&gt;
 *     &lt;enumeration value="addPatientAddress"/&gt;
 *     &lt;enumeration value="updatePatientAddresses"/&gt;
 *     &lt;enumeration value="createOrder"/&gt;
 *     &lt;enumeration value="getOrderDetail"/&gt;
 *     &lt;enumeration value="deletePatientAddress"/&gt;
 *     &lt;enumeration value="addPatientPhone"/&gt;
 *     &lt;enumeration value="updatePatientPhone"/&gt;
 *     &lt;enumeration value="getPatientPhones"/&gt;
 *     &lt;enumeration value="getAllPhoneTypes"/&gt;
 *     &lt;enumeration value="deletePatientPhone"/&gt;
 *     &lt;enumeration value="getPatientAddressById"/&gt;
 *     &lt;enumeration value="getShippingOptionById"/&gt;
 *     &lt;enumeration value="getPatientSavedCardById"/&gt;
 *     &lt;enumeration value="getClaimDetail"/&gt;
 *     &lt;enumeration value="updatePatientDefaultEmail"/&gt;
 *     &lt;enumeration value="getMemberPlan"/&gt;
 *     &lt;enumeration value="getMemberIdCard"/&gt;
 *     &lt;enumeration value="getGroupContactDetail"/&gt;
 *     &lt;enumeration value="searchPharmacy"/&gt;
 *     &lt;enumeration value="getPharmacies"/&gt;
 *     &lt;enumeration value="deleteUserCard"/&gt;
 *     &lt;enumeration value="setUserDefaultCard"/&gt;
 *     &lt;enumeration value="setUserDefaultAddress"/&gt;
 *     &lt;enumeration value="setUserDefaultPhone"/&gt;
 *     &lt;enumeration value="getUserDefaultAddress"/&gt;
 *     &lt;enumeration value="drugStructure"/&gt;
 *     &lt;enumeration value="getDrugStructure"/&gt;
 *     &lt;enumeration value="getDrugStructure"/&gt;
 *     &lt;enumeration value="getUserPhoneById"/&gt;
 *     &lt;enumeration value="searchAccumulator"/&gt;
 *     &lt;enumeration value="addPatientCard"/&gt;
 *     &lt;enumeration value="authenticateLegacyUser"/&gt;
 *     &lt;enumeration value="validateLegacyUserName"/&gt;
 *     &lt;enumeration value="getPatientCag"/&gt;
 *     &lt;enumeration value="getFormularyName"/&gt;
 *     &lt;enumeration value="createPdf"/&gt;
 *     &lt;enumeration value="getContainers"/&gt;
 *     &lt;enumeration value="startProcess"/&gt;
 *     &lt;enumeration value="getWorkItems"/&gt;
 *     &lt;enumeration value="getWorkItemDetail"/&gt;
 *     &lt;enumeration value="submitTask"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RestApiName")
@XmlEnum
public enum RestApiName {

    @XmlEnumValue("saveUser")
    SAVE_USER("saveUser"),
    @XmlEnumValue("getHealthConditions")
    GET_HEALTH_CONDITIONS("getHealthConditions"),
    @XmlEnumValue("getDrugAllergies")
    GET_DRUG_ALLERGIES("getDrugAllergies"),
    @XmlEnumValue("createTicket")
    CREATE_TICKET("createTicket"),
    @XmlEnumValue("getContacts")
    GET_CONTACTS("getContacts"),
    @XmlEnumValue("searchMembers")
    SEARCH_MEMBERS("searchMembers"),
    @XmlEnumValue("getMember")
    GET_MEMBER("getMember"),
    @XmlEnumValue("createPatient")
    CREATE_PATIENT("createPatient"),
    @XmlEnumValue("getHealthInformation")
    GET_HEALTH_INFORMATION("getHealthInformation"),
    @XmlEnumValue("updateHealthInformation")
    UPDATE_HEALTH_INFORMATION("updateHealthInformation"),
    @XmlEnumValue("updateContacts")
    UPDATE_CONTACTS("updateContacts"),
    @XmlEnumValue("getAlternateDrugs")
    GET_ALTERNATE_DRUGS("getAlternateDrugs"),
    @XmlEnumValue("validateAddress")
    VALIDATE_ADDRESS("validateAddress"),
    @XmlEnumValue("getPrescriptionDetail")
    GET_PRESCRIPTION_DETAIL("getPrescriptionDetail"),
    @XmlEnumValue("submitTrialClaim")
    SUBMIT_TRIAL_CLAIM("submitTrialClaim"),
    @XmlEnumValue("getPatientContact")
    GET_PATIENT_CONTACT("getPatientContact"),
    @XmlEnumValue("generateAccessToken")
    GENERATE_ACCESS_TOKEN("generateAccessToken"),
    @XmlEnumValue("searchMember")
    SEARCH_MEMBER("searchMember"),
    @XmlEnumValue("runTrialClaim")
    RUN_TRIAL_CLAIM("runTrialClaim"),
    @XmlEnumValue("listShipping")
    LIST_SHIPPING("listShipping"),
    @XmlEnumValue("listPatientCreditCard")
    LIST_PATIENT_CREDIT_CARD("listPatientCreditCard"),
    @XmlEnumValue("getPatientAddresses")
    GET_PATIENT_ADDRESSES("getPatientAddresses"),
    @XmlEnumValue("addPatientAddress")
    ADD_PATIENT_ADDRESS("addPatientAddress"),
    @XmlEnumValue("updatePatientAddresses")
    UPDATE_PATIENT_ADDRESSES("updatePatientAddresses"),
    @XmlEnumValue("createOrder")
    CREATE_ORDER("createOrder"),
    @XmlEnumValue("getOrderDetail")
    GET_ORDER_DETAIL("getOrderDetail"),
    @XmlEnumValue("deletePatientAddress")
    DELETE_PATIENT_ADDRESS("deletePatientAddress"),
    @XmlEnumValue("addPatientPhone")
    ADD_PATIENT_PHONE("addPatientPhone"),
    @XmlEnumValue("updatePatientPhone")
    UPDATE_PATIENT_PHONE("updatePatientPhone"),
    @XmlEnumValue("getPatientPhones")
    GET_PATIENT_PHONES("getPatientPhones"),
    @XmlEnumValue("getAllPhoneTypes")
    GET_ALL_PHONE_TYPES("getAllPhoneTypes"),
    @XmlEnumValue("deletePatientPhone")
    DELETE_PATIENT_PHONE("deletePatientPhone"),
    @XmlEnumValue("getPatientAddressById")
    GET_PATIENT_ADDRESS_BY_ID("getPatientAddressById"),
    @XmlEnumValue("getShippingOptionById")
    GET_SHIPPING_OPTION_BY_ID("getShippingOptionById"),
    @XmlEnumValue("getPatientSavedCardById")
    GET_PATIENT_SAVED_CARD_BY_ID("getPatientSavedCardById"),
    @XmlEnumValue("getClaimDetail")
    GET_CLAIM_DETAIL("getClaimDetail"),
    @XmlEnumValue("updatePatientDefaultEmail")
    UPDATE_PATIENT_DEFAULT_EMAIL("updatePatientDefaultEmail"),
    @XmlEnumValue("getMemberPlan")
    GET_MEMBER_PLAN("getMemberPlan"),
    @XmlEnumValue("getMemberIdCard")
    GET_MEMBER_ID_CARD("getMemberIdCard"),
    @XmlEnumValue("getGroupContactDetail")
    GET_GROUP_CONTACT_DETAIL("getGroupContactDetail"),
    @XmlEnumValue("searchPharmacy")
    SEARCH_PHARMACY("searchPharmacy"),
    @XmlEnumValue("getPharmacies")
    GET_PHARMACIES("getPharmacies"),
    @XmlEnumValue("deleteUserCard")
    DELETE_USER_CARD("deleteUserCard"),
    @XmlEnumValue("setUserDefaultCard")
    SET_USER_DEFAULT_CARD("setUserDefaultCard"),
    @XmlEnumValue("setUserDefaultAddress")
    SET_USER_DEFAULT_ADDRESS("setUserDefaultAddress"),
    @XmlEnumValue("setUserDefaultPhone")
    SET_USER_DEFAULT_PHONE("setUserDefaultPhone"),
    @XmlEnumValue("getUserDefaultAddress")
    GET_USER_DEFAULT_ADDRESS("getUserDefaultAddress"),
    @XmlEnumValue("drugStructure")
    DRUG_STRUCTURE("drugStructure"),
    @XmlEnumValue("getDrugStructure")
    GET_DRUG_STRUCTURE("getDrugStructure"),
    @XmlEnumValue("getUserPhoneById")
    GET_USER_PHONE_BY_ID("getUserPhoneById"),
    @XmlEnumValue("searchAccumulator")
    SEARCH_ACCUMULATOR("searchAccumulator"),
    @XmlEnumValue("addPatientCard")
    ADD_PATIENT_CARD("addPatientCard"),
    @XmlEnumValue("authenticateLegacyUser")
    AUTHENTICATE_LEGACY_USER("authenticateLegacyUser"),
    @XmlEnumValue("validateLegacyUserName")
    VALIDATE_LEGACY_USER_NAME("validateLegacyUserName"),
    @XmlEnumValue("getPatientCag")
    GET_PATIENT_CAG("getPatientCag"),
    @XmlEnumValue("getFormularyName")
    GET_FORMULARY_NAME("getFormularyName"),
    @XmlEnumValue("createPdf")
    CREATE_PDF("createPdf"),
    @XmlEnumValue("getContainers")
    GET_CONTAINERS("getContainers"),
    @XmlEnumValue("startProcess")
    START_PROCESS("startProcess"),
    @XmlEnumValue("getWorkItems")
    GET_WORK_ITEMS("getWorkItems"),
    @XmlEnumValue("getWorkItemDetail")
    GET_WORK_ITEM_DETAIL("getWorkItemDetail"),
    @XmlEnumValue("submitTask")
    SUBMIT_TASK("submitTask");
    private final String value;

    RestApiName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RestApiName fromValue(String v) {
        for (RestApiName c: RestApiName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
