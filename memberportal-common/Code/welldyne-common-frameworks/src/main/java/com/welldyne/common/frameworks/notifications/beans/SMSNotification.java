package com.welldyne.common.frameworks.notifications.beans;

/**
 * This class holds the information required for SMS notifications.
 * 
 * @author Puneet.Kumar
 *
 */
public class SMSNotification extends Notification {

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
