/**
 * This contains classes related to exceptionhandling framework.
 * 
 * @author nilofarmew
 *
 */
package com.welldyne.common.frameworks.exceptionhandling;