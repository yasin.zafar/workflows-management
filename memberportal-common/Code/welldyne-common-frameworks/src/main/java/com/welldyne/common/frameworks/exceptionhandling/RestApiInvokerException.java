package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.function.Consumer;

import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;

/**
 * 
 * @author nilofarmew
 *
 */
public class RestApiInvokerException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6699044596140792189L;

	private ResponseEntity<?> responseEntity;

	public RestApiInvokerException(ResponseEntity<?> responseEntity) {
		super();
		this.responseEntity = responseEntity;
	}

	public RestApiInvokerException(ResponseEntity<?> responseEntity, Throwable e) {
		super(e);
		this.responseEntity = responseEntity;
	}

	public RestApiInvokerException(ResponseEntity<?> responseEntity, String message) {
		super(message);
		this.responseEntity = responseEntity;
	}

	public RestApiInvokerException(ResponseEntity<?> responseEntity, String message, Throwable e) {
		super(message, e);
		this.responseEntity = responseEntity;
	}

	public ResponseEntity<?> getResponseEntity() {
		return responseEntity;
	}

	public RestApiInvokerException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public RestApiInvokerException with(Consumer<RestApiInvokerException> builder) {
		builder.accept(this);
		return this;
	}

}
