package com.welldyne.common.frameworks.elasticsearch.annotations;

/**
 * This is enum to defined different type of values that a field can have in a
 * index.
 * 
 * @author nilofarmew
 *
 */
public enum ESFieldValueType {

	/**
	 * This should be the used when field values are not a set of values or
	 * values which can be aggregated.
	 */
	VARIABLE,

	/**
	 * This should be used when the field values are a pre-defined set of
	 * values. For example, fields like status.
	 */
	SET,

	/**
	 * This should be used when field value can be aggregated. For fields which
	 * have values of type int or double or any type on which mathematical sum
	 * can be performed.
	 */
	SUMMABLE;
}
