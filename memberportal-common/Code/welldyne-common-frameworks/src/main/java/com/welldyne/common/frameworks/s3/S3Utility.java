package com.welldyne.common.frameworks.s3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.welldyne.common.frameworks.exceptionhandling.S3Exception;
import com.welldyne.common.frameworks.exceptionhandling.SystemException;
import com.welldyne.common.utilities.utilities.FileUtility;

@Component
public class S3Utility {

	@Autowired
	private AmazonS3 s3Client;

	@Value("${s3.bucket}")
	private String bucket;

	public static final String tempDir = System.getProperty("user.dir") + "/tmp/";

	public InputStream getS3File(String folder, String fileName) {
		InputStream iStream = null;
		S3Object object = s3Client.getObject(new GetObjectRequest(bucket, folder + "/" + fileName));
		if (null != object) {
			iStream = object.getObjectContent();
		}
		return iStream;
	}

	public InputStream getS3File(String filePath) {
		InputStream iStream = null;
		S3Object object = s3Client.getObject(new GetObjectRequest(bucket, filePath));
		if (null != object) {
			iStream = object.getObjectContent();
		}
		return iStream;
	}

	public void uploadFileToS3(MultipartFile attachment, String filePath) {
		try {
			File file = new File(tempDir + attachment.getOriginalFilename());
			attachment.transferTo(file);
			s3Client.putObject(new PutObjectRequest(bucket, filePath, file));
			file.delete();
		} catch (Exception e) {
			throw new S3Exception("Error while uploading file", e);
		}
	}

	public void uploadFileToS3(File file, String filePath) {
		try {
			s3Client.putObject(new PutObjectRequest(bucket, filePath, file));
			file.delete();
		} catch (Exception e) {
			throw new S3Exception("Error while uploading file", e);
		}
	}

	public void uploadAsPdfToS3(byte[] bytes, String filePath, String filename) {
		try {
			File file = FileUtility.byteArrayToFileTempFolder(bytes, filename, "pdf");
			uploadFileToS3(file, filePath);
			file.delete();
		} catch (Exception e) {
			throw new S3Exception("Error while uploading file", e);
		}
	}

	public List<String> getFileNamesInFolder(String folderName) {
		List<String> files = new ArrayList<String>();
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucket).withPrefix(folderName);

		ObjectListing objectListing = s3Client.listObjects(listObjectsRequest);
		for (S3ObjectSummary os : objectListing.getObjectSummaries()) {
			files.add(os.getKey());
		}

		return files;
	}

	public List<File> getFilesInFolder(String folderName) {
		try {
			var files = new ArrayList<File>();
			var filePaths = getFileNamesInFolder(folderName);
			for (var filePath : filePaths) {
				var fileInputStream = getS3File(filePath);

				byte[] buffer = new byte[fileInputStream.available()];
				fileInputStream.read(buffer);

				var split = filePath.split("/");
				var filename = split[split.length - 1];

				File folder = new File(tempDir);
				folder.mkdirs();

				File targetFile = new File(tempDir + filename);
				targetFile.createNewFile();

				OutputStream outStream = new FileOutputStream(targetFile);
				outStream.write(buffer);

				outStream.close();
				fileInputStream.close();

				File file = new File(tempDir + filename);
				files.add(file);
			}

			return files;
		} catch (IOException e) {
			throw new SystemException(e);
		}
	}
}
