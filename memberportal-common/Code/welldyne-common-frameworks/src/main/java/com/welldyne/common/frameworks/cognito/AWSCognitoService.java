package com.welldyne.common.frameworks.cognito;

import com.amazonaws.services.cognitoidp.model.AdminDeleteUserResult;

/**
 * 
 * @author nilofarmew
 *
 */
public interface AWSCognitoService {

	/**
	 * 
	 * @param username
	 * @return
	 */
	public AWSCognitoAuthenticationResult authenticateUser(String username, String password);

	/**
	 * 
	 * @param username
	 */
	public boolean userExists(String username);

	/**
	 * 
	 * @param accessToken
	 */
	public String createUser(String username, String password);

	/**
	 * 
	 * @param username
	 */
	public void confirmUserSignUp(String username);

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean resetPassword(String username, String password);

	/**
	 * 
	 * @param username
	 */
	public AdminDeleteUserResult deleteUser(String username);

}
