package com.welldyne.common.frameworks.notifications.sender;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.welldyne.common.frameworks.exceptionhandling.NotificationSenderException;
import com.welldyne.common.frameworks.notifications.beans.SMSNotification;
import com.welldyne.common.frameworks.notifications.beans.SenderResponse;
import com.welldyne.common.frameworks.s3.S3Utility;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * 
 * This class performs all the functionalities required to send SMS
 * notifications.
 * 
 * @author Puneet.Kumar
 *
 */
@Component
public class SMSSender extends BaseNotificationSender<SMSNotification> {

	@Autowired
	private S3Utility s3Utility;

	@Value("${aws.emailTemplate.folderName}")
	private String folderName;

	@Value("${aws.smsTemplate.fileName}")
	private String fileName;

	@Autowired
	private AmazonSNSClient snsClient;

	@Autowired
	private Configuration freeMarkerConfiguration;

	private String phoneISDPrefix = "+1";

	@Override
	public SenderResponse<List<String>> sendNotification(SMSNotification notification) throws NotificationSenderException {
		try {

			InputStream is = s3Utility.getS3File(notification.getTemplatePath());
			InputStreamReader isr = new InputStreamReader(is);

			Template template = new Template("", isr, freeMarkerConfiguration);

			StringWriter writer = new StringWriter();
			template.process(notification.getNotificationData(), writer);
			String smsBody = writer.toString();

			String tos[] = notification.getTo();

			Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();

			List<String> messageIds = new ArrayList<String>();
			for (String to : tos) {
				PublishResult result = snsClient.publish(new PublishRequest().withMessage(smsBody)
						.withPhoneNumber(phoneISDPrefix + to)
						.withMessageAttributes(smsAttributes));
				messageIds.add(result.getMessageId());
			}

			SenderResponse<List<String>> senderResponse = new SenderResponse<>();
			senderResponse.setResponse(messageIds);
			return senderResponse;

		} catch (Exception e) {
			throw new NotificationSenderException(e);
		}
	}

}
