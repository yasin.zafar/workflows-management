package com.welldyne.common.frameworks.setup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import com.welldyne.common.frameworks.caching.Cache;

@Configuration
public class RedisCacheConfig {

	@Bean
	public RedisTemplate<Cache, Object> redisTemplate(RedisConnectionFactory rcf) {
		RedisTemplate<Cache, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(rcf);
		return template;
	}
}
