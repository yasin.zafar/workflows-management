package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class TrialClaimException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2024654437351238411L;

	public TrialClaimException() {
		super();
	}

	public TrialClaimException(Throwable e) {
		super(e);
	}

	public TrialClaimException(String message) {
		super(message);
	}

	public TrialClaimException(String message, Throwable e) {
		super(message, e);
	}

	public TrialClaimException(List<Error> errors) {
		super(errors);
	}

	public TrialClaimException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public TrialClaimException(List<Error> errors, String message) {
		super(errors, message);
	}

	public TrialClaimException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public TrialClaimException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public TrialClaimException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public TrialClaimException with(Consumer<TrialClaimException> builder) {
		builder.accept(this);
		return this;
	}

}
