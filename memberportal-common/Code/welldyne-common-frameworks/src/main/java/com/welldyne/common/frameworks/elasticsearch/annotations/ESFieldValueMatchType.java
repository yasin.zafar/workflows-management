package com.welldyne.common.frameworks.elasticsearch.annotations;

public enum ESFieldValueMatchType {
	
	NOT_EQUALS,
	NOT_NULL,
	GREATER_THAN,
	LESS_THAN,
	GREATER_THAN_EQUALS,
	LESS_THAN_EQUALS
	
}
