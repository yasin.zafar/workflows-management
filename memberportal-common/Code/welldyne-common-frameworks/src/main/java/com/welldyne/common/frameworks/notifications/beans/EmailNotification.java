package com.welldyne.common.frameworks.notifications.beans;

import java.util.List;

/**
 * This class holds the information required for email notifications.
 * 
 * @author Puneet.Kumar
 *
 */
public class EmailNotification extends Notification {

	private List<String> attachments;

	public List<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}

}
