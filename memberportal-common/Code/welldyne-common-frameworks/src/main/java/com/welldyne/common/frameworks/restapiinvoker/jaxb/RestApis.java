
package com.welldyne.common.frameworks.restapiinvoker.jaxb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RestApis complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestApis"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Api" type="{http://www.welldyne.com/common/frameworks/restapiinvoker/jaxb}RestApi" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="basePath" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestApis", propOrder = {
    "api"
})
public class RestApis
    implements Serializable
{

    private final static long serialVersionUID = -1L;
    @XmlElement(name = "Api", required = true)
    protected List<RestApi> api;
    @XmlAttribute(name = "basePath", required = true)
    protected String basePath;

    /**
     * Gets the value of the api property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the api property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApi().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RestApi }
     * 
     * 
     */
    public List<RestApi> getApi() {
        if (api == null) {
            api = new ArrayList<RestApi>();
        }
        return this.api;
    }

    /**
     * Gets the value of the basePath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasePath() {
        return basePath;
    }

    /**
     * Sets the value of the basePath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasePath(String value) {
        this.basePath = value;
    }

}
