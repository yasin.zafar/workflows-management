package com.welldyne.common.frameworks.createpdf;

public class CreatePdfRestApiInvokerRequest {

	private Object payload;

	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}

}
