package com.welldyne.common.frameworks.exceptionhandling;

import java.util.List;
import java.util.function.Consumer;

/**
 * 
 * This the exception class which is to be thrown if a label is not found.
 * 
 * @author Puneet.Kumar
 *
 */
public class LabelNotFoundException extends PresentationException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7545169208954269909L;

	public LabelNotFoundException() {
		super();
	}

	public LabelNotFoundException(Throwable e) {
		super(e);
	}

	public LabelNotFoundException(String message) {
		super(message);
	}

	public LabelNotFoundException(String message, Throwable e) {
		super(message, e);
	}

	public LabelNotFoundException(List<Error> errors) {
		super(errors);
	}

	public LabelNotFoundException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public LabelNotFoundException(List<Error> errors, String message) {
		super(errors, message);
	}

	public LabelNotFoundException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public LabelNotFoundException with(Consumer<LabelNotFoundException> builder) {
		builder.accept(this);
		return this;
	}

}
