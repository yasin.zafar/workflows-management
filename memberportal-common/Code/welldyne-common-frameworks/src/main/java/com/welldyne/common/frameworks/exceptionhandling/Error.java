package com.welldyne.common.frameworks.exceptionhandling;

import java.util.function.Consumer;

/**
 * This is returned when exception occurred in the rest api.
 * 
 * @author nilofarmew
 *
 */
public class Error {

	/**
	 * This is custom error code for the type of exception.
	 */
	private ErrorCode errorCode;

	/**
	 * This is the description associates to the error code.
	 */
	private String errorDescription;

	/**
	 * This is the message from the exception instance.
	 */
	private String exceptionMessage;

	/**
	 * This is the detailed stack trace for the exception.
	 */
	private String exceptionDetail;

	/**
	 * builder using consumer
	 * 
	 * @param builder
	 * @return this
	 */
	public Error with(Consumer<Error> builder) {
		builder.accept(this);
		return this;
	}

	/**
	 * 
	 * @param errorCode
	 * @return
	 */
	public Error withErrorCode(ErrorCode errorCode) {
		this.setErrorCode(errorCode);
		return this;
	}

	/**
	 * @return the errorCode
	 */
	public ErrorCode getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return the exceptionMessage
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}

	/**
	 * @param exceptionMessage
	 *            the exceptionMessage to set
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	/**
	 * @return the exceptionDetail
	 */
	public String getExceptionDetail() {
		return exceptionDetail;
	}

	/**
	 * @param exceptionDetail
	 *            the exceptionDetail to set
	 */
	public void setExceptionDetail(String exceptionDetail) {
		this.exceptionDetail = exceptionDetail;
	}

}
