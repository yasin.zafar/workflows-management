package com.welldyne.common.frameworks.logging;

/**
 * This is used to capture information related to a method that is being logged.
 * 
 * @author nilofarmew
 *
 */
public class LoggableMethod {

	/**
	 * This is method's signature.
	 */
	private String methodSignature;
	
	/**
	 * This is name of parameters as String.
	 */
	private String methodParameterNames;
	
	/** 
	 * This values of parameters as String.
	 */
	private String methodParameterValues;

	/**
	 * @return the methodSignature
	 */
	public String getMethodSignature() {
		return methodSignature;
	}

	/**
	 * @param methodSignature
	 *            the methodSignature to set
	 */
	public void setMethodSignature(String methodSignature) {
		this.methodSignature = methodSignature;
	}

	/**
	 * @return the methodParameterNames
	 */
	public String getMethodParameterNames() {
		return methodParameterNames;
	}

	/**
	 * @param methodParameterNames
	 *            the methodParameterNames to set
	 */
	public void setMethodParameterNames(String methodParameterNames) {
		this.methodParameterNames = methodParameterNames;
	}

	/**
	 * @return the methodParameterValues
	 */
	public String getMethodParameterValues() {
		return methodParameterValues;
	}

	/**
	 * @param methodParameterValues
	 *            the methodParameterValues to set
	 */
	public void setMethodParameterValues(String methodParameterValues) {
		this.methodParameterValues = methodParameterValues;
	}

}
