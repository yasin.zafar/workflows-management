package com.welldyne.common.frameworks.caching;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
class CacheIntializer {

	@Autowired
	private RedisTemplate<Cache, Object> redisTemplate;

	@Value("${cache.redis.enabled}")
	private boolean redisCacheEnabled;

	@PostConstruct
	void init() {
		Cache.redisTemplate = this.redisTemplate;
		Cache.redisCacheEnabled = this.redisCacheEnabled;
	}
}
