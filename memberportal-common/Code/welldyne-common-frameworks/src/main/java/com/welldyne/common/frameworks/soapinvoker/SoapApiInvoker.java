package com.welldyne.common.frameworks.soapinvoker;

import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.springframework.util.StringUtils;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;

import com.welldyne.common.frameworks.layers.ApiInvoker;
import com.welldyne.common.frameworks.logging.Loggable;

/***
 * 
 * This is base class for Soap API invoker providing implementation for invoke
 * method. This is designed using template method design pattern with invoke()
 * method as the template method.
 * 
 * @author Abhinandan.Gupta
 *
 * @param <TSoapApiInvokerRequest>
 * @param <TSoapApiInvokerResponse>
 */
@Loggable
@ApiInvoker
public abstract class SoapApiInvoker<TSoapApiInvokerRequest, TSoapApiInvokerResponse> {

	/***
	 * This method calls the soap web service using SoapConnector
	 * 
	 * @param tRequest
	 * @return
	 */
	public final TSoapApiInvokerResponse invoke(TSoapApiInvokerRequest tRequest) {

		SoapApi api = getApi();

		String soapActionUrl = api.getAction();
		String soapUrl = api.getUrl();

		Object soapRequest = getSoapRequest(tRequest);
		Object soapResponse;

		if (!StringUtils.isEmpty(soapActionUrl)) {
			soapResponse = callWebService(soapUrl, soapRequest, soapActionUrl);
		} else {
			soapResponse = callWebService(soapUrl, soapRequest);
		}

		TSoapApiInvokerResponse soapApiInvokerResponse = this.getSoapResponse(soapResponse);
		return soapApiInvokerResponse;

	}

	/**
	 * This method is used to call a web service with an Action URL
	 *
	 * @param url
	 * @param request
	 * @return the response object
	 */
	private Object callWebService(String url, Object request, String soapActionUrl) {
		return getWebServiceTemplate().marshalSendAndReceive(url, request, new WebServiceMessageCallback() {

			@Override
			public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
				((SoapMessage) message).setSoapAction(soapActionUrl);
			}
		});

	}

	/**
	 * This method is used to call a web service without an Action URL
	 * 
	 * @param url
	 * @param request
	 * @return
	 */
	private Object callWebService(String url, Object request) {
		return getWebServiceTemplate().marshalSendAndReceive(url, request);
	}

	/**
	 * 
	 * @return WebServiceTemplate
	 */
	private WebServiceTemplate getWebServiceTemplate() {
		return getSoapConnector().getWebServiceTemplate();
	}

	/**
	 * Implement this to provide soap api connector
	 * 
	 * @return
	 */
	protected abstract SoapConnector getSoapConnector();

	/**
	 * Subclass of this class must provide implementation of this to provide the
	 * api that needs to be invoked.
	 * 
	 * @return the api that is needed to be invoked.
	 */
	protected abstract SoapApi getApi();

	/***
	 * Subclass of this class must provide implementation of this to provide the
	 * soap request structure
	 * 
	 * @param tRequest
	 * @return
	 */
	protected abstract Object getSoapRequest(TSoapApiInvokerRequest tRequest);

	/***
	 * Subclass of this class must provide implementation of this to provide the
	 * soap response structure
	 * 
	 * @param soapResponse
	 * @return
	 */
	protected abstract TSoapApiInvokerResponse getSoapResponse(Object soapResponse);

}