package com.welldyne.common.frameworks.restapiinvoker;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.welldyne.common.frameworks.s3.S3Utility;

@Service
class RestApiInitializer {
	@Value("${env}")
	private String env;

	@Value("${env.local}")
	private String localEnv;

	@Value("${configuration.restApi.name}")
	private String configuration;

	@Autowired
	private S3Utility s3Utility;
	
	@PostConstruct
	public void postConstruct() {
		RestApi.env = this.env;
		RestApi.configuration = this.configuration;
		RestApi.localEnv = this.localEnv;
		RestApi.s3Utility = this.s3Utility;
	}
}
