package com.welldyne.common.frameworks.elasticsearch.beans;

import java.util.List;

/**
 * This is the object which will be returned by ESRepository as record summary.
 * 
 * @author nilofarmew
 *
 * @param <ESEntity>
 *            Type of the index bean.
 */
public class ESSearchResult<TESEntity> {

	/**
	 * The records returned by the query.
	 */
	private List<TESEntity> records;

	/**
	 * Count of maximum number of records that the query can return is
	 * pagination was not applied.
	 */
	private int totalCount;

	/**
	 * This is used in case of open scroll query is being executed. The case
	 * when all the records are requested but total number of records are more
	 * that what elastic search can return at once, open scroll queries become
	 * useful then to apply same query again without re-creating it.
	 */
	private String scrollId;

	/**
	 * @return the records
	 */
	public List<TESEntity> getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(List<TESEntity> records) {
		this.records = records;
	}

	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount
	 *            the totalCount to set
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * @return the scrollId
	 */
	public String getScrollId() {
		return scrollId;
	}

	/**
	 * @param scrollId
	 *            the scrollId to set
	 */
	public void setScrollId(String scrollId) {
		this.scrollId = scrollId;
	}

}
