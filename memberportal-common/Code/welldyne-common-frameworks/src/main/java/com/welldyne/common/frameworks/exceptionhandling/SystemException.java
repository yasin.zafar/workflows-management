package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

/**
 * This can be thrown from classes like utilities and other static classes that
 * are not the layers.
 * 
 * @author nilofarmew
 *
 */
public class SystemException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1797571847003024279L;

	public SystemException() {
		super();
	}

	public SystemException(Throwable e) {
		super(e);
	}

	public SystemException(String message) {
		super(message);
	}

	public SystemException(String message, Throwable e) {
		super(message, e);
	}

	public SystemException(List<Error> errors) {
		super(errors);
	}

	public SystemException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public SystemException(List<Error> errors, String message) {
		super(errors, message);
	}

	public SystemException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public SystemException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public SystemException with(Consumer<SystemException> builder) {
		builder.accept(this);
		return this;
	}
}
