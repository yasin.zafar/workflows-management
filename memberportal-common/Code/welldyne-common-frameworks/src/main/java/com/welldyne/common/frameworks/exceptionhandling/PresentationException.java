package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

/**
 * This exception is thrown from the presentation layer.
 * 
 * @author nilofarmew
 *
 */
public class PresentationException extends BaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5526199796474515394L;

	public PresentationException() {
		super();
	}

	public PresentationException(Throwable e) {
		super(e);
	}

	public PresentationException(Throwable e, boolean isLogged) {
		super(e, isLogged);
	}

	public PresentationException(String message) {
		super(message);
	}

	public PresentationException(String message, Throwable e) {
		super(message, e);
	}

	public PresentationException(List<Error> errors) {
		super(errors);
	}

	public PresentationException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public PresentationException(List<Error> errors, String message) {
		super(errors, message);
	}

	public PresentationException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public PresentationException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

}
