package com.welldyne.common.frameworks.notifications.publishers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.welldyne.common.frameworks.notifications.beans.EmailNotification;
import com.welldyne.common.frameworks.notifications.events.NotificationEvent;

/**
 * 
 * This class holds the functionalities to publish Email Notifications.
 * 
 * @author Puneet.Kumar
 *
 */
@Component
public class EmailNotificationEventPublisher implements INotificationEventPublisher<EmailNotification> {

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	/**
	 * This method publishes email notification event.
	 */
	@Override
	public void publishNotificationEvent(EmailNotification message) {
		NotificationEvent<EmailNotification> event = new NotificationEvent<EmailNotification>(message);
		applicationEventPublisher.publishEvent(event);

	}

}
