package com.welldyne.common.frameworks.elasticsearch.services;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.welldyne.common.frameworks.elasticsearch.annotations.ESField;
import com.welldyne.common.frameworks.elasticsearch.annotations.ESSortOrder;
import com.welldyne.common.frameworks.elasticsearch.beans.ESEntity;
import com.welldyne.common.frameworks.exceptionhandling.SystemException;

/**
 * This is the request that ESRepository will accept to perform all the queries.
 * 
 * Creation of this object is the most critical part. The way values are set in
 * different fields of this object will drive the actual elastic search query
 * and in turn the result of it.
 * 
 * All the getters in this class have only package level access. The user
 * (creator of this request) can only set certain fields. Rest are being used by
 * {@link ESRepository} only for creating elastic search query.
 * 
 * @author nilofarmew
 *
 * @param <TESEntity>
 *            This is the ESEntity this request is being created for. The
 *            request will get all the information about index and its fields
 *            from ESEntity provided for this generic type.
 */
public class ESRequest<TESEntity extends ESEntity<TESEntity>> {

	/**
	 * This is the ESEntity this request is being created for. This will provide
	 * easier way to set values for the field to provide to the elastic search
	 * queries.
	 * 
	 * The field values that are provided will be used to create Filter Query
	 * for elastic search.
	 */
	private TESEntity filterCriteria;

	/**
	 * This will be used to provide start record is pagination needs to be
	 * applies. The default values is 0 for this.
	 */
	private int startRecord;

	/**
	 * This is the count of total records requested. The default value for this
	 * will be read from configuration.
	 */
	private int size;

	/**
	 * This is the date after which records should be returned.
	 */
	private LocalDate startDate;

	/**
	 * This is the date before which records should be returned.
	 */
	private LocalDate endDate;

	/**
	 * This date range limit will be applied on this.dateRangeColumn.
	 * 
	 * If value is not provided, field from TESEntity will be used where
	 * 'dateRange' value in {@link ESField} annotation is true.
	 * 
	 * If no such field is present, then no date range limit will be applied.
	 */
	private String dateRangeColumn;

	/**
	 * If the records returned needs to sorted by a particular field, the field
	 * name should be provided here.
	 * 
	 * If this value is not present, by default the records will be sorted based
	 * on field in TESEntity for which on annotation {@link ESField} 'sort' is
	 * true.
	 * 
	 * This is used to order records by 'sortColumn' in the Ascending or
	 * Descending order. If not present, records will be sorted in Ascending
	 * order.
	 */
	private Map<String, ESSortOrder> sortColumns;

	/**
	 * This is to provide search value to the elastic search query. The default
	 * value is just empty.
	 * 
	 * The search query will only be created is this value is present.
	 */
	private String searchString;

	/**
	 * These are the fields in index that the search needs to be applied.
	 * 
	 * If this value is not present, by default the records will be searched
	 * based on fields in TESEntity for which on annotation {@link ESField}
	 * 'search' is true.
	 * 
	 * If there are no fields marked as true for search, then search query will
	 * not be created.
	 */
	private List<String> searchColumns;

	/**
	 * In case nay painless scripts needs to be added to search, provide it
	 * here. If not present, no painless script will be applied while creating
	 * search query for searchString.
	 */
	private String searchPainlessScript;

	// ---------- Only for ESRepository purpose ------

	/**
	 * This is private attribute being used by this class only to figure out the
	 * actual type of TIndex generic.
	 */
	private Class<TESEntity> classOfFilterCriteria;

	/**
	 * This is only being by ESRepository. The access level on getters and
	 * setters on this field is package level.
	 * 
	 * This is used in case of open scroll query is being executed. The case
	 * when all the records are requested but total number of records are more
	 * that what elastic search can return at once, open scroll queries become
	 * useful then to apply same query again without re-creating it.
	 */
	private String scrollId;

	/**
	 * This is only being used by ESRepository. The access level on getters and
	 * setters on this field is package level.
	 * 
	 * This will be set true when a scroll query is being created.
	 */
	private boolean openScroll;

	/**
	 * There is no public constructor for this class. Only {@link ESRepository }
	 * provides instance to its sub classes.
	 * 
	 * @param classOfFilterCriteria
	 */
	ESRequest(Class<TESEntity> classOfFilterCriteria) {
		this.classOfFilterCriteria = classOfFilterCriteria;
	}

	// ------ Builders for the request --------
	/**
	 * 
	 * This is builder method for object of this class. Since it is a
	 * complicated object to create, the builder will help to make it easier and
	 * neater to create and fill instance of this (ESRequest<TESEntity>) class.
	 * 
	 * @param requestBuilder
	 * @return this
	 */
	public ESRequest<TESEntity> with(Consumer<ESRequest<TESEntity>> requestBuilder) {
		requestBuilder.accept(this);
		return this;
	}

	/**
	 * It can be used if just an empty instance of request is needed.
	 * 
	 * @return this
	 */
	public ESRequest<TESEntity> build() {
		return this;
	}

	/**
	 * This is builder for the search criteria in the request.
	 * 
	 * @param requestBuilder
	 * @return
	 */
	public ESRequest<TESEntity> withFilterCriteria(Consumer<TESEntity> filterCriteriaBuilder) {
		getFilterCriteria().with(filterCriteriaBuilder);
		return this;
	}

	// ---------------------------- Getters ----------------

	/**
	 *
	 * @return filterCriteria if not null. If null then set filterCriteria to
	 *         new instance of TESEntity and returns filterCriteria.
	 */
	TESEntity getFilterCriteria() {
		if (filterCriteria == null) {
			try {
				filterCriteria = (this.classOfFilterCriteria.getDeclaredConstructor()
						.newInstance());
				return this.filterCriteria;
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				throw new SystemException();
			}
		} else {
			return this.filterCriteria;
		}
	}

	/**
	 * @return the startRecord
	 */
	int getStartRecord() {
		return startRecord;
	}

	/**
	 * @return the size
	 */
	int getSize() {
		return size;
	}

	/**
	 * @return the startDate
	 */
	LocalDate getStartDate() {
		return startDate;
	}

	/**
	 * @return the endDate
	 */
	LocalDate getEndDate() {
		return endDate;
	}

	/**
	 * @return the dateRangeColumn
	 */
	String getDateRangeColumn() {
		return dateRangeColumn;
	}

	/**
	 * @return the scrollId
	 */
	String getScrollId() {
		return scrollId;
	}

	/**
	 * @return the openScroll
	 */
	boolean isOpenScroll() {
		return openScroll;
	}

	/**
	 * @return the sortColumn
	 */
	Map<String, ESSortOrder> getSortColumns() {
		return sortColumns;
	}

	/**
	 * @return the searchString
	 */
	String getSearchString() {
		return searchString;
	}

	/**
	 * @return the searchColumns
	 */
	List<String> getSearchColumns() {
		return searchColumns;
	}

	/**
	 * @return the searchPainlessScript
	 */
	String getSearchPainlessScript() {
		return searchPainlessScript;
	}

	// ---------------------------- Setters ----------------

	/**
	 * @param startRecord
	 *            the startRecord to set
	 */
	public ESRequest<TESEntity> startRecord(int startRecord) {
		this.startRecord = startRecord;
		return this;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public ESRequest<TESEntity> endDate(LocalDate endDate) {
		this.endDate = endDate;
		return this;
	}

	/**
	 * 
	 * @param dateRangeColumn
	 */
	public ESRequest<TESEntity> dateRangeColumn(String dateRangeColumn) {
		this.dateRangeColumn = dateRangeColumn;
		return this;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public ESRequest<TESEntity> size(int size) {
		this.size = size;
		return this;
	}

	/**
	 * @param searchString
	 *            the searchString to set
	 */
	public ESRequest<TESEntity> searchString(String searchString) {
		this.searchString = searchString;
		return this;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 * @return
	 */
	public ESRequest<TESEntity> startDate(LocalDate startDate) {
		this.startDate = startDate;
		return this;
	}

	/**
	 * @param scrollId
	 *            the scrollId to set
	 */
	void scrollId(String scrollId) {
		this.scrollId = scrollId;
	}

	/**
	 * @param openScroll
	 *            the openScroll to set
	 */
	void openScroll(boolean openScroll) {
		this.openScroll = openScroll;
	}

	/**
	 * Add column is needed to be sorted in ascending order
	 * 
	 * @param sortColumns
	 *            the sortColumns to set
	 * @return this
	 */
	public ESRequest<TESEntity> sortAscending(String sortColumn) {
		if (this.sortColumns == null) {
			this.sortColumns = new HashMap<>();
		}
		this.sortColumns.put(sortColumn, ESSortOrder.ASC);
		return this;
	}

	/**
	 * Add column is needed to be sorted in descending order
	 * 
	 * @param sortColumns
	 *            the sortColumns to set
	 * @return
	 */
	public ESRequest<TESEntity> sortDescending(String sortColumn) {
		if (this.sortColumns == null) {
			this.sortColumns = new HashMap<>();
		}
		this.sortColumns.put(sortColumn, ESSortOrder.DESC);
		return this;
	}

	/**
	 * @param searchColumns
	 *            the searchColumns to set
	 * @return
	 */
	public ESRequest<TESEntity> searchColumns(String searchColumn) {
		if (this.searchColumns == null) {
			this.searchColumns = new ArrayList<>();
		}
		this.searchColumns.add(searchColumn);
		return this;
	}

	/**
	 * @param searchPainlessScript
	 *            the searchPainlessScript to set
	 */
	public ESRequest<TESEntity> searchPainlessScript(String searchPainlessScript) {
		this.searchPainlessScript = searchPainlessScript;
		return this;
	}

}
