package com.welldyne.common.frameworks.setup;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.google.common.base.Supplier;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.client.http.JestHttpClient;
import vc.inreach.aws.request.AWSSigner;
import vc.inreach.aws.request.AWSSigningRequestInterceptor;

@Configuration
public class ElasticsearchConfig extends WebMvcConfigurationSupport {

	@Value("${aws.es.endpoint}")
	private String awsESEndpoint;

	@Value("${aws.region}")
	private String awsRegion;

	@Value("${aws.es.service}")
	private String awsEsService;

	@Bean
	public JestClient mxJestClient() {
		final JestClientFactory factory = getJestClientFactory();
		factory.setHttpClientConfig(new HttpClientConfig.Builder(awsESEndpoint).multiThreaded(true)
				.readTimeout(60000)
				.build());
		JestClient jestClient = factory.getObject();

		GsonBuilder gsonBuilder = new GsonBuilder();

		gsonBuilder.registerTypeAdapter(LocalDate.class, new JsonDeserializer<LocalDate>() {
			@Override
			public LocalDate deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
				try {
					return ZonedDateTime.parse(json.getAsJsonPrimitive()
							.getAsString())
							.toLocalDateTime()
							.toLocalDate();
				} catch (Exception e) {
					return LocalDate.parse(json.getAsJsonPrimitive()
							.getAsString());
				}
			}
		});

		gsonBuilder.registerTypeAdapter(LocalDate.class, new JsonSerializer<LocalDate>() {
			@Override
			public JsonElement serialize(LocalDate date, Type typeOfSrc, JsonSerializationContext context) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				return new JsonPrimitive(date.format(formatter));
			}
		});

		Gson gson = gsonBuilder.create();

		((JestHttpClient) jestClient).setGson(gson);
		return jestClient;
	}

	private JestClientFactory getJestClientFactory() {
		return new JestClientFactory() {

			@Override
			protected HttpClientBuilder configureHttpClient(HttpClientBuilder builder) {
				builder.addInterceptorLast(prepareInterceptor());
				return builder;
			}

			@Override
			protected HttpAsyncClientBuilder configureHttpClient(HttpAsyncClientBuilder builder) {
				builder.addInterceptorLast(prepareInterceptor());
				return builder;
			}
		};
	}

	private AWSSigningRequestInterceptor prepareInterceptor() {
		Supplier<LocalDateTime> dateTimeSupplier = () -> LocalDateTime.now(ZoneOffset.UTC);
		AWSSigner signer = new AWSSigner(new DefaultAWSCredentialsProviderChain(), awsRegion, awsEsService, dateTimeSupplier);
		return new AWSSigningRequestInterceptor(signer);
	}

}
