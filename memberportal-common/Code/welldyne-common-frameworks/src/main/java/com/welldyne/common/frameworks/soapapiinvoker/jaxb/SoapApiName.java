
package com.welldyne.common.frameworks.soapapiinvoker.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SoapApiName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SoapApiName"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="allergyLookup"/&gt;
 *     &lt;enumeration value="listPatientAllergies"/&gt;
 *     &lt;enumeration value="listPatientHealthConditions"/&gt;
 *     &lt;enumeration value="searchEligibleMember"/&gt;
 *     &lt;enumeration value="updatePatientAddress"/&gt;
 *     &lt;enumeration value="listPatientAddress"/&gt;
 *     &lt;enumeration value="getPatientAddress"/&gt;
 *     &lt;enumeration value="listPatientPhone"/&gt;
 *     &lt;enumeration value="getPatientPhone"/&gt;
 *     &lt;enumeration value="deletePatientAddress"/&gt;
 *     &lt;enumeration value="editPatientPhoneNumber"/&gt;
 *     &lt;enumeration value="listEligibleMemberDetail"/&gt;
 *     &lt;enumeration value="addPatientAllergies"/&gt;
 *     &lt;enumeration value="addPatientDrugAllergies"/&gt;
 *     &lt;enumeration value="addPatientHealthConditions"/&gt;
 *     &lt;enumeration value="registerFamilyInfo"/&gt;
 *     &lt;enumeration value="getPatient"/&gt;
 *     &lt;enumeration value="updatePatientEmail"/&gt;
 *     &lt;enumeration value="patientPlans"/&gt;
 *     &lt;enumeration value="familyMembers"/&gt;
 *     &lt;enumeration value="patient"/&gt;
 *     &lt;enumeration value="getPatientAllergies"/&gt;
 *     &lt;enumeration value="getPatientConditions"/&gt;
 *     &lt;enumeration value="patient"/&gt;
 *     &lt;enumeration value="patientPhone"/&gt;
 *     &lt;enumeration value="patientAddress"/&gt;
 *     &lt;enumeration value="addAllergy"/&gt;
 *     &lt;enumeration value="addCondition"/&gt;
 *     &lt;enumeration value="lookupByCondition"/&gt;
 *     &lt;enumeration value="listPatientPlans"/&gt;
 *     &lt;enumeration value="listCreditCards"/&gt;
 *     &lt;enumeration value="getCreditCard"/&gt;
 *     &lt;enumeration value="shippingMethods"/&gt;
 *     &lt;enumeration value="createOrder"/&gt;
 *     &lt;enumeration value="addPatientAddress"/&gt;
 *     &lt;enumeration value="getOrder"/&gt;
 *     &lt;enumeration value="listPrescriberAddress"/&gt;
 *     &lt;enumeration value="listPrescriberPhone"/&gt;
 *     &lt;enumeration value="addPatientPhone"/&gt;
 *     &lt;enumeration value="getPatientPhones"/&gt;
 *     &lt;enumeration value="createOrderEPost"/&gt;
 *     &lt;enumeration value="deleteCreditCard"/&gt;
 *     &lt;enumeration value="updateCreditCard"/&gt;
 *     &lt;enumeration value="addCreditCard"/&gt;
 *     &lt;enumeration value="authenticateIdentity"/&gt;
 *     &lt;enumeration value="searchIdentity"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SoapApiName")
@XmlEnum
public enum SoapApiName {

    @XmlEnumValue("allergyLookup")
    ALLERGY_LOOKUP("allergyLookup"),
    @XmlEnumValue("listPatientAllergies")
    LIST_PATIENT_ALLERGIES("listPatientAllergies"),
    @XmlEnumValue("listPatientHealthConditions")
    LIST_PATIENT_HEALTH_CONDITIONS("listPatientHealthConditions"),
    @XmlEnumValue("searchEligibleMember")
    SEARCH_ELIGIBLE_MEMBER("searchEligibleMember"),
    @XmlEnumValue("updatePatientAddress")
    UPDATE_PATIENT_ADDRESS("updatePatientAddress"),
    @XmlEnumValue("listPatientAddress")
    LIST_PATIENT_ADDRESS("listPatientAddress"),
    @XmlEnumValue("getPatientAddress")
    GET_PATIENT_ADDRESS("getPatientAddress"),
    @XmlEnumValue("listPatientPhone")
    LIST_PATIENT_PHONE("listPatientPhone"),
    @XmlEnumValue("getPatientPhone")
    GET_PATIENT_PHONE("getPatientPhone"),
    @XmlEnumValue("deletePatientAddress")
    DELETE_PATIENT_ADDRESS("deletePatientAddress"),
    @XmlEnumValue("editPatientPhoneNumber")
    EDIT_PATIENT_PHONE_NUMBER("editPatientPhoneNumber"),
    @XmlEnumValue("listEligibleMemberDetail")
    LIST_ELIGIBLE_MEMBER_DETAIL("listEligibleMemberDetail"),
    @XmlEnumValue("addPatientAllergies")
    ADD_PATIENT_ALLERGIES("addPatientAllergies"),
    @XmlEnumValue("addPatientDrugAllergies")
    ADD_PATIENT_DRUG_ALLERGIES("addPatientDrugAllergies"),
    @XmlEnumValue("addPatientHealthConditions")
    ADD_PATIENT_HEALTH_CONDITIONS("addPatientHealthConditions"),
    @XmlEnumValue("registerFamilyInfo")
    REGISTER_FAMILY_INFO("registerFamilyInfo"),
    @XmlEnumValue("getPatient")
    GET_PATIENT("getPatient"),
    @XmlEnumValue("updatePatientEmail")
    UPDATE_PATIENT_EMAIL("updatePatientEmail"),
    @XmlEnumValue("patientPlans")
    PATIENT_PLANS("patientPlans"),
    @XmlEnumValue("familyMembers")
    FAMILY_MEMBERS("familyMembers"),
    @XmlEnumValue("patient")
    PATIENT("patient"),
    @XmlEnumValue("getPatientAllergies")
    GET_PATIENT_ALLERGIES("getPatientAllergies"),
    @XmlEnumValue("getPatientConditions")
    GET_PATIENT_CONDITIONS("getPatientConditions"),
    @XmlEnumValue("patientPhone")
    PATIENT_PHONE("patientPhone"),
    @XmlEnumValue("patientAddress")
    PATIENT_ADDRESS("patientAddress"),
    @XmlEnumValue("addAllergy")
    ADD_ALLERGY("addAllergy"),
    @XmlEnumValue("addCondition")
    ADD_CONDITION("addCondition"),
    @XmlEnumValue("lookupByCondition")
    LOOKUP_BY_CONDITION("lookupByCondition"),
    @XmlEnumValue("listPatientPlans")
    LIST_PATIENT_PLANS("listPatientPlans"),
    @XmlEnumValue("listCreditCards")
    LIST_CREDIT_CARDS("listCreditCards"),
    @XmlEnumValue("getCreditCard")
    GET_CREDIT_CARD("getCreditCard"),
    @XmlEnumValue("shippingMethods")
    SHIPPING_METHODS("shippingMethods"),
    @XmlEnumValue("createOrder")
    CREATE_ORDER("createOrder"),
    @XmlEnumValue("addPatientAddress")
    ADD_PATIENT_ADDRESS("addPatientAddress"),
    @XmlEnumValue("getOrder")
    GET_ORDER("getOrder"),
    @XmlEnumValue("listPrescriberAddress")
    LIST_PRESCRIBER_ADDRESS("listPrescriberAddress"),
    @XmlEnumValue("listPrescriberPhone")
    LIST_PRESCRIBER_PHONE("listPrescriberPhone"),
    @XmlEnumValue("addPatientPhone")
    ADD_PATIENT_PHONE("addPatientPhone"),
    @XmlEnumValue("getPatientPhones")
    GET_PATIENT_PHONES("getPatientPhones"),
    @XmlEnumValue("createOrderEPost")
    CREATE_ORDER_E_POST("createOrderEPost"),
    @XmlEnumValue("deleteCreditCard")
    DELETE_CREDIT_CARD("deleteCreditCard"),
    @XmlEnumValue("updateCreditCard")
    UPDATE_CREDIT_CARD("updateCreditCard"),
    @XmlEnumValue("addCreditCard")
    ADD_CREDIT_CARD("addCreditCard"),
    @XmlEnumValue("authenticateIdentity")
    AUTHENTICATE_IDENTITY("authenticateIdentity"),
    @XmlEnumValue("searchIdentity")
    SEARCH_IDENTITY("searchIdentity");
    private final String value;

    SoapApiName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SoapApiName fromValue(String v) {
        for (SoapApiName c: SoapApiName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
