package com.welldyne.common.frameworks.validateaddress;

import javax.xml.bind.annotation.XmlElement;

public class Address {
	private String id;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip5;
	private Integer zip4;
	private Error error;
	private String retunText;

	@XmlElement(name = "ID")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement(name = "Address1")
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	@XmlElement(name = "Address2")
	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	@XmlElement(name = "City")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@XmlElement(name = "State")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@XmlElement(name = "Error")
	public Error getError() {
		return error;
	}

	@XmlElement(name = "Zip5")
	public String getZip5() {
		return zip5;
	}

	public void setZip5(String zip5) {
		this.zip5 = zip5;
	}

	@XmlElement(name = "Zip4")
	public Integer getZip4() {
		return zip4;
	}

	public void setZip4(Integer zip4) {
		this.zip4 = zip4;
	}

	public void setError(Error error) {
		this.error = error;
	}

	@XmlElement(name = "ReturnText")
	public String getRetunText() {
		return retunText;
	}

	public void setRetunText(String retunText) {
		this.retunText = retunText;
	}

}
