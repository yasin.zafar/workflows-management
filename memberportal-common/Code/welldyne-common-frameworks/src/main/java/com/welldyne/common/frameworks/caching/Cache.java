package com.welldyne.common.frameworks.caching;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.redis.core.RedisTemplate;

/**
 * 
 * @author nilofarmew
 *
 */
public enum Cache {

	DOMAIN_ALL_DRUG_ALLERGIES,
	DOMAIN_ALL_HEALTH_CONDITIONS,
	BUSINESS_ALL_DRUG_ALLERGIES,
	BUSINESS_ALL_HEALTH_CONDITIONS,
	REST_API_CONFIGURATION(true),
	SOAP_API_CONFIGURATION(true),
	WELLDYNE_PORTAL(true),
	WELLDYNE_PORTAL_LINKS(true),
	WELLDYNE_PORTAL_COMPONENTS(true),
	WELLDYNE_PORTAL_TABLE_COMPONENTS(true),
	WELLDYNE_PORTAL_DROPDOWN_COMPONENTS(true),
	WELLDYNE_PORTAL_GRAPHIC_TAB_COMPONENTS(true),
	OPTUM_TOKEN(true),
	MAIL_ORDER_GROUPS(true),
	ALL_GROUPS(true);

	static RedisTemplate<Cache, Object> redisTemplate;
	static Map<Cache, Object> inMemmoryCache = new HashMap<Cache, Object>();
	static int timeout;

	static boolean redisCacheEnabled;
	private boolean cacheInMemoryOnly;

	Cache() {
		this.cacheInMemoryOnly = false;
	}

	Cache(boolean cacheInMemoryOnly) {
		this.cacheInMemoryOnly = cacheInMemoryOnly;
	}

	public <T> T get() {
		Object obj = null;
		if (isCacheInMemory()) {
			obj = inMemmoryCache.get(this);
		} else {
			obj = redisTemplate.opsForHash()
					.get(this, this.toString());
			if (obj == null) {
				return null;
			}
		}

		@SuppressWarnings("unchecked")
		T value = (T) obj;
		return value;
	}

	public <T> void put(T value) {
		if (isCacheInMemory()) {
			inMemmoryCache.put(this, value);
		} else {
			redisTemplate.opsForHash()
					.put(this, this.toString(), value);
		}
	}

	private boolean isCacheInMemory() {
		return this.cacheInMemoryOnly || !redisCacheEnabled;
	}

	public void delete() {
		if (isCacheInMemory()) {
			if (inMemmoryCache.containsKey(this)) {
				inMemmoryCache.remove(this);
			}
		} else {
			redisTemplate.opsForHash()
					.delete(this, this.toString());
		}
	}

	public static void clearInMemoryCache() {
		inMemmoryCache.clear();
	}
}
