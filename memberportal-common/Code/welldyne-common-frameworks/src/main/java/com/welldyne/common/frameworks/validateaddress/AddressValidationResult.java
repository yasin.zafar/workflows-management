package com.welldyne.common.frameworks.validateaddress;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author nilofarmew
 *
 */
@XmlRootElement(name = "AddressValidateResponse")
public class AddressValidationResult {

	Address address;

	@XmlElement(name = "Address")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
