package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

/**
 * This exception is thrown from the repository and invoker layers.
 * 
 * @author nilofarmew
 *
 */
public class DataAccessException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1050596170240554519L;

	public DataAccessException() {
		super();
	}

	public DataAccessException(Throwable e) {
		super(e);
	}

	public DataAccessException(Throwable e, boolean isLogged) {
		super(e, isLogged);
	}

	public DataAccessException(String message) {
		super(message);
	}

	public DataAccessException(String message, Throwable e) {
		super(message, e);
	}

	public DataAccessException(List<Error> errors, Throwable e) {
		super(errors, e);
	}

	public DataAccessException(List<Error> errors) {
		super(errors);
	}

	public DataAccessException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public DataAccessException(List<Error> errors, String message) {
		super(errors, message);
	}

	public DataAccessException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public DataAccessException withError(Error error) {
		if (CollectionUtils.isEmpty(this.errors)) {
			this.errors = new ArrayList<Error>();
		}
		this.errors.add(error);
		return this;
	}

	public DataAccessException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(this.errors)) {
			this.errors = new ArrayList<Error>();
		}

		this.errors.addAll(errors);
		return this;
	}

	public DataAccessException with(Consumer<DataAccessException> builder) {
		builder.accept(this);
		return this;
	}
}
