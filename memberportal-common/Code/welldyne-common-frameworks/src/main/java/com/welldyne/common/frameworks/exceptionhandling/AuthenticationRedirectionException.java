package com.welldyne.common.frameworks.exceptionhandling;

/**
 * 
 * @author nilofarmew
 *
 */
public class AuthenticationRedirectionException extends BusinessException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3804770459735441126L;

	/**
	 * errors related to teh exceptions
	 */
	protected ErrorCode errorCode;

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	private String username;
	private String password;

	public AuthenticationRedirectionException(String msg, ErrorCode errorCode, String username, String password) {
		super(msg);
		this.errorCode = errorCode;
		this.username = username;
		this.password = password;
	}

	public AuthenticationRedirectionException() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AuthenticationRedirectionException(String message) {
		super(message);
	}

	public AuthenticationRedirectionException(String message, Throwable e, ErrorCode errorCode) {
		super(message, e);
		this.errorCode = errorCode;

	}

	public AuthenticationRedirectionException(String message, Throwable e, ErrorCode errorCode, String username, String password) {
		super(message, e);
		this.errorCode = errorCode;
		this.username = username;
		this.password = password;

	}

	public AuthenticationRedirectionException(String message, Throwable e) {
		super(message, e);
	}

	/**
	 * @return the errorCode
	 */
	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public AuthenticationRedirectionException with(String username, String password) {
		this.username = username;
		this.password = password;
		return this;
	}

}
