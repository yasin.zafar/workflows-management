package com.welldyne.common.frameworks.notifications.sender;

import com.welldyne.common.frameworks.exceptionhandling.NotificationSenderException;
import com.welldyne.common.frameworks.notifications.beans.Notification;
import com.welldyne.common.frameworks.notifications.beans.SenderResponse;

/**
 * 
 * 
 * 
 * @author Puneet.Kumar
 *
 * @param <T>
 */
public interface NotificationSender<T extends Notification> {

	/**
	 * This method performs the functionality to send the notification as per
	 * the type of notification. This should be implemented in the corresponding
	 * notification sub class
	 * 
	 * @param notification
	 * @throws NotificationSenderException
	 */

	public SenderResponse<?> sendNotification(T notification) throws NotificationSenderException;

}
