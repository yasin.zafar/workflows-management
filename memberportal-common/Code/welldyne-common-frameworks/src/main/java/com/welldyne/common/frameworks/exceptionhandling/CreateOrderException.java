package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class CreateOrderException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2024654437351238411L;

	public CreateOrderException() {
		super();
	}

	public CreateOrderException(Throwable e) {
		super(e);
	}

	public CreateOrderException(String message) {
		super(message);
	}

	public CreateOrderException(String message, Throwable e) {
		super(message, e);
	}

	public CreateOrderException(List<Error> errors) {
		super(errors);
	}

	public CreateOrderException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public CreateOrderException(List<Error> errors, String message) {
		super(errors, message);
	}

	public CreateOrderException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public CreateOrderException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public CreateOrderException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public CreateOrderException with(Consumer<CreateOrderException> builder) {
		builder.accept(this);
		return this;
	}

}
