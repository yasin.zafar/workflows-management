package com.welldyne.common.frameworks.validateaddress;

/**
 * 
 * @author nilofarmew
 *
 */
class AddressValidationResultMapper {

	private AddressValidationResultMapper() {

	}

	/**
	 * 
	 * @return
	 */
	public static AddressValidationResult with(AddressValidationResult uspsApiResponse) {
		var addressValidationResult = new AddressValidationResult();

		addressValidationResult.setAddress(uspsApiResponse.getAddress());
		return addressValidationResult;
	}
}
