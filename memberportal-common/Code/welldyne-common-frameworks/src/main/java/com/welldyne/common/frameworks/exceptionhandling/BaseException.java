package com.welldyne.common.frameworks.exceptionhandling;

import java.util.List;

/**
 * 
 * This is base for all the custom exceptions created for WellDyne projects.
 * 
 * @author nilofarmew
 *
 */
public abstract class BaseException extends RuntimeException {
	/**
	 * 
	 */
	protected static final long serialVersionUID = 1L;

	/**
	 * errors related to teh exceptions
	 */
	protected List<Error> errors;

	/**
	 * if exception is already logged
	 */
	private boolean isLogged;

	/**
	 * If exception has been logged or not. To avoid logging exception multiple
	 * times
	 * 
	 * @return the isLogged
	 */
	boolean isLogged() {
		return isLogged;
	}

	void setLogged(boolean isLogged) {
		this.isLogged = isLogged;
	}

	public List<Error> getErrors() {
		return errors;
	}

	public BaseException() {
		super();
	}

	public BaseException(Throwable e, boolean isLogged) {
		super(e);
		this.isLogged = isLogged;
	}

	public BaseException(Throwable e) {
		super(e);
	}

	public BaseException(String message) {
		super(message);
	}

	public BaseException(String message, Throwable e) {
		super(message, e);
	}

	public BaseException(List<Error> errors) {
		super();
		this.errors = errors;
	}

	public BaseException(List<Error> errors, Throwable e) {
		super(e);
		this.errors = errors;
	}

	public BaseException(List<Error> errors, String message) {
		super(message);
		this.errors = errors;
	}

	public BaseException(List<Error> errors, String message, Throwable e) {
		super(message, e);
		this.errors = errors;
	}

}
