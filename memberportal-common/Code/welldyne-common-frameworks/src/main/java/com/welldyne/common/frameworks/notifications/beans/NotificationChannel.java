package com.welldyne.common.frameworks.notifications.beans;

/**
 * 
 * @author nilofarmew
 *
 */
public enum NotificationChannel {

	EMAIL,
	SMS,
	WEB
}
