package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class UpdateFailedException extends DataAccessException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2024654437351238411L;

	public UpdateFailedException() {
		super();
	}

	public UpdateFailedException(Throwable e) {
		super(e);
	}

	public UpdateFailedException(String message) {
		super(message);
	}

	public UpdateFailedException(String message, Throwable e) {
		super(message, e);
	}

	public UpdateFailedException(List<Error> errors) {
		super(errors);
	}

	public UpdateFailedException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public UpdateFailedException(List<Error> errors, String message) {
		super(errors, message);
	}

	public UpdateFailedException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public UpdateFailedException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public UpdateFailedException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	@Override
	public UpdateFailedException with(Consumer<DataAccessException> builder) {
		builder.accept(this);
		return this;
	}

}
