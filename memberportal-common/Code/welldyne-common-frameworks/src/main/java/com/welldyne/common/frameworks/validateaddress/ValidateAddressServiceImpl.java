package com.welldyne.common.frameworks.validateaddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.welldyne.common.frameworks.logging.Loggable;

@Loggable
@Component
public class ValidateAddressServiceImpl implements ValidateAddressService {

	@Autowired
	private ValidateAddressUspsApiInvoker validateAddressUspsApiInvoker;

	@Override
	public AddressValidationResult validate(AddressValidationRequest address) {
		var invokerRequest = ValidateAddressUspsApiInvokerRequestMapper.map(address);
		var uspsApiResponse = validateAddressUspsApiInvoker.getResponse(invokerRequest);

		var addressValidationResult = AddressValidationResultMapper.with(uspsApiResponse);
		return addressValidationResult;
	}

}
