package com.welldyne.common.frameworks.elasticsearch.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class ESRestrictions {

	private ESRestrictions() {

	}

	private List<ESRestriction> restrictions;

	public List<ESRestriction> getRestrictions() {
		return restrictions;
	}

	public static ESRestrictions create() {
		return new ESRestrictions();
	}

	public static ESRestrictions create(List<ESRestriction> restrictions) {
		var esRestrictions = new ESRestrictions();
		esRestrictions.restrictions = restrictions;
		return esRestrictions;
	}

	public ESRestrictions addRestriction(ESRestriction restriction) {
		if (CollectionUtils.isEmpty(restrictions)) {
			restrictions = new ArrayList<ESRestriction>();
		}
		restrictions.add(restriction);
		return this;
	}

	public ESRestrictions with(Consumer<ESRestrictions> entityBuilder) {
		entityBuilder.accept(this);
		return this;
	}

}
