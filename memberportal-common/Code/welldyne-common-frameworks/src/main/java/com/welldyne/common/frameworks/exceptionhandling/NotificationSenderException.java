package com.welldyne.common.frameworks.exceptionhandling;

import java.util.function.Consumer;

public class NotificationSenderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1835096115555087885L;

	public NotificationSenderException() {
		super();
	}

	public NotificationSenderException(Throwable e) {
		super(e);
	}

	public NotificationSenderException(String message) {
		super(message);
	}

	public NotificationSenderException(String message, Throwable e) {
		super(message, e);
	}

	public NotificationSenderException with(Consumer<NotificationSenderException> builder) {
		builder.accept(this);
		return this;
	}

}
