package com.welldyne.common.frameworks.notifications.publishers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.welldyne.common.frameworks.notifications.beans.SMSNotification;
import com.welldyne.common.frameworks.notifications.events.NotificationEvent;

/**
 * 
 * This class holds the functionalities to publish SMS Notifications.
 * 
 * @author Puneet.Kumar
 *
 */
@Component
public class SMSNotificationEventPublisher implements INotificationEventPublisher<SMSNotification> {
	@Autowired
	ApplicationEventPublisher applicationEventPublisher;

	/**
	 * This method publishes SMS notification event.
	 */
	@Override
	public void publishNotificationEvent(SMSNotification message) {

		NotificationEvent<SMSNotification> event = new NotificationEvent<SMSNotification>(message);
		applicationEventPublisher.publishEvent(event);
	}

}
