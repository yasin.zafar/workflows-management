package com.welldyne.common.frameworks.session;

import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 
 * @author nilofarmew
 *
 */
public enum Session {

	/**
	 * For logged in user
	 */
	LOGGED_IN_USER,

	/**
	 * Username of the loggedin user
	 */
	LOGGED_IN_USERNAME,

	/**
	 * Cart for the logged in user
	 */
	LOGGED_IN_USER_CART,

	/**
	 * 
	 */
	LOGGED_IN_USER_GLOBAL_FILTERS,

	/**
	 * Used to temporarily store
	 */
	TEMPORARY_ORDER_DURING_CHECKOUT,

	/**
	 * Temporarily being created for savings module. This should be deleted
	 * before production deployment.
	 */
	TEMPORARY_PRICING,

	/**
	 * Pharmacy locator in session to avoid calling apis to get pharmacies for
	 * same zip and miles again and again
	 */
	LOGGED_IN_USER_PHARMACY_LOCATOR,

	/**
	 * Used to check if error occurs while adding a payment card
	 * 
	 */
	ERROR_IN_PAYMENT_CARD;

	/**
	 * Gets value from session
	 * 
	 * @param <T>
	 * @return value from session for the key
	 */
	public <T> T get() {
		@SuppressWarnings("unchecked")
		T value = (T) getSession().getAttribute(this.name());
		return value;
	}

	/**
	 * Puts value in session
	 * 
	 * @param <T>
	 * @param value
	 */
	public <T> void put(T value) {
		getSession().setAttribute(this.name(), value);
	}

	/**
	 * Removes value from session
	 */
	public void delete() {
		getSession().removeAttribute(this.name());
	}

	public static String getId() {
		return getSession().getId();
	}

	public static boolean isUserLoggedIn() {
		if (Session.LOGGED_IN_USER.get() != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	private static HttpSession getSession() {
		var attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest()
				.getSession();
	}

}
