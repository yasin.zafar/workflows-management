package com.welldyne.common.frameworks.notifications.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.welldyne.common.frameworks.exceptionhandling.NotificationSenderException;
import com.welldyne.common.frameworks.notifications.beans.EmailNotification;
import com.welldyne.common.frameworks.notifications.beans.SMSNotification;
import com.welldyne.common.frameworks.notifications.beans.WebNotification;
import com.welldyne.common.frameworks.notifications.events.NotificationEvent;
import com.welldyne.common.frameworks.notifications.sender.EmailSender;
import com.welldyne.common.frameworks.notifications.sender.SMSSender;
import com.welldyne.common.frameworks.notifications.sender.WebNotificationSender;

/**
 * 
 * This is the notification event listener class to call corresponding
 * notification channel.
 * 
 * @author Puneet.Kumar
 *
 */
@Component
public class NotificationEventListener {
	@Autowired
	EmailSender emailSender;
	@Autowired
	SMSSender smsSender;
	@Autowired
	WebNotificationSender webSender;

	@EventListener
	@Async
	public void handleEmailNotificationEvent(NotificationEvent<EmailNotification> event) {
		// audit Email Notifications as Draft
		// send Email

		try {
			emailSender.sendNotification(event.getNotification());
		} catch (NotificationSenderException e) {
			// persist exception to Db
		}
		// Update audit Email Notifications as Error or Success

	}

	@EventListener
	@Async
	public void handleSMSNotificationEvent(NotificationEvent<SMSNotification> event) {
		// audit SMS Notifications
		// send SMS
		try {
			smsSender.sendNotification(event.getNotification());
		} catch (NotificationSenderException e) {
			System.out.println(e.getLocalizedMessage());
			// persist exception to Db
		}
		// Update SMS Notifications as Error or Success
	}

	@EventListener
	@Async
	public void handleWebSNotificationEvent(NotificationEvent<WebNotification> event) {
		// audit Web Notifications
		// send Web Notification
		try {
			webSender.sendNotification(event.getNotification());
		} catch (NotificationSenderException e) {
			// persist exception to Db
		}
		// Update audit Web Notifications as Error or Success
	}

}
