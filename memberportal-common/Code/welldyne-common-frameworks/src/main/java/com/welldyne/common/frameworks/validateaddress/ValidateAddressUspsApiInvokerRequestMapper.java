package com.welldyne.common.frameworks.validateaddress;

/**
 * 
 * @author nilofarmew
 *
 */
class ValidateAddressUspsApiInvokerRequestMapper {

	/**
	 * 
	 */
	private ValidateAddressUspsApiInvokerRequestMapper() {

	}

	/**
	 * 
	 * @param address
	 * @return
	 */
	static ValidateAddressUspsApiInvokerRequest map(AddressValidationRequest address) {
		var validateAddressUspsApiInvokerRequest = new ValidateAddressUspsApiInvokerRequest();
		validateAddressUspsApiInvokerRequest.setCandidates(address.getCandidates());
		validateAddressUspsApiInvokerRequest.setCity(address.getCity());
		validateAddressUspsApiInvokerRequest.setMatch(address.getMatch());
		validateAddressUspsApiInvokerRequest.setState(address.getState());
		validateAddressUspsApiInvokerRequest.setStreet(address.getStreet());
		validateAddressUspsApiInvokerRequest.setStreet2(address.getStreet2());
		validateAddressUspsApiInvokerRequest.setZipcode(address.getZipcode());
		return validateAddressUspsApiInvokerRequest;
	}
}
