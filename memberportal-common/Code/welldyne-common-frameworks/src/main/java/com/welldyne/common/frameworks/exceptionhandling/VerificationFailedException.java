package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

/**
 * 
 * @author nilofarmew
 *
 */
public class VerificationFailedException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6840122650072809130L;

	public VerificationFailedException() {
		super();
	}

	public VerificationFailedException(Throwable e) {
		super(e);
	}

	public VerificationFailedException(String message) {
		super(message);
	}

	public VerificationFailedException(String message, Throwable e) {
		super(message, e);
	}

	public VerificationFailedException(List<Error> errors) {
		super(errors);
	}

	public VerificationFailedException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public VerificationFailedException(List<Error> errors, String message) {
		super(errors, message);
	}

	public VerificationFailedException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public VerificationFailedException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public VerificationFailedException with(Consumer<VerificationFailedException> builder) {
		builder.accept(this);
		return this;
	}
}
