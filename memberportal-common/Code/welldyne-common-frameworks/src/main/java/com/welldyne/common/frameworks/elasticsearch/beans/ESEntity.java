package com.welldyne.common.frameworks.elasticsearch.beans;

import java.util.function.Consumer;

import io.searchbox.annotations.JestId;

/**
 * This is base of all the entities which will be created to defined entity
 * against each index.
 * 
 * @author nilofarmew
 */
public abstract class ESEntity<TESEntity> {

	/**
	 * This is id of the document.
	 */
	@JestId
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * This accepts the builder provided and returns the instance for this
	 * object back.
	 * 
	 * This gets inherited by all ESEntities providing them with builder
	 * functionality.
	 * 
	 * @param entityBuilder
	 *            this is the builder function to create the entity object.
	 * @return updated instance of this object after accepting the builder
	 */
	@SuppressWarnings("unchecked")
	public TESEntity with(Consumer<TESEntity> entityBuilder) {
		entityBuilder.accept((TESEntity) this);
		return (TESEntity) this;
	}

}
