package com.welldyne.common.frameworks.notifications.beans;

import java.util.Map;

/**
 * This is the notification bean class which will holds the common information
 * across different types of notifications.
 * 
 * @author Puneet.Kumar
 *
 */
public class Notification {

	private Map<String, Object> notificationData;
	private String[] to;
	private String templatePath;

	public Map<String, Object> getNotificationData() {
		return notificationData;
	}

	public void setNotificationData(Map<String, Object> notificationData) {
		this.notificationData = notificationData;
	}

	public String[] getTo() {
		return to;
	}

	public void setTo(String[] to) {
		this.to = to;
	}

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getTemplate() {
		return templatePath;
	}

	public void setTemplate(String template) {
		this.templatePath = template;
	}

}
