
package com.welldyne.common.frameworks.soapapiinvoker.jaxb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Apis" type="{http://www.welldyne.com/common/frameworks/soapapiinvoker/jaxb}SoapApis" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apis"
})
@XmlRootElement(name = "SoapApiConfigurations")
public class SoapApiConfigurations
    implements Serializable
{

    private final static long serialVersionUID = -1L;
    @XmlElement(name = "Apis", required = true)
    protected List<SoapApis> apis;

    /**
     * Gets the value of the apis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the apis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SoapApis }
     * 
     * 
     */
    public List<SoapApis> getApis() {
        if (apis == null) {
            apis = new ArrayList<SoapApis>();
        }
        return this.apis;
    }

}
