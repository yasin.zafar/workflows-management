package com.welldyne.common.frameworks.elasticsearch.services;

import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.welldyne.common.frameworks.elasticsearch.beans.ESStats;

/**
 * This is a static class being used to create ESStats object from the elastic
 * search result for stats type of queries.
 * 
 * @author nilofarmew
 *
 */
class ESStatsMapper {

	/**
	 * Since this is a static class, there are no public constructors.
	 */
	private ESStatsMapper() {

	}

	/**
	 * This method is the only package level access method. This is being used
	 * by {@link ESRepository} to convert the query result for count stats and
	 * sum stats to {@link ESStats} object.
	 * 
	 * @param recordCount
	 *            This is total number of records being aggregated by query.
	 * 
	 * @param aggregations
	 *            This the aggregation result.
	 * @param list
	 * 
	 * @return ESStats the POJO for to store the aggregation result.
	 */
	static ESStats map(int recordCount, JsonObject aggregations) {
		ESStats sumStats = new ESStats();
		sumStats.setName("total");
		sumStats.setRecordCount(recordCount);

		aggregations.entrySet().stream().forEach(entry -> {
			map(recordCount, sumStats, entry);
		});

		return sumStats;
	}

	/**
	 * This method and map(int recordCount, ESStats sumStats, Entry<String,
	 * JsonElement> entry) are working in a regressive manner to read the nested
	 * Hierarchical JSON result and create ESStats accordingly until the leaf
	 * aggregation in the JSON.
	 * 
	 * @param bucket
	 * @return ESStats
	 */
	private static ESStats map(JsonElement bucket) {
		ESStats sumStats = new ESStats();
		JsonObject bucketAsJsonObject = bucket.getAsJsonObject();
		String name = "";
		int recordCount = 0;

		Set<Entry<String, JsonElement>> entrySet = bucketAsJsonObject.entrySet();

		for (Entry<String, JsonElement> entry : entrySet) {
			if ("key".equalsIgnoreCase(entry.getKey())) {
				name = bucketAsJsonObject.get("key").getAsString();
				sumStats.setName(name);
			} else if ("doc_count".equalsIgnoreCase(entry.getKey())) {
				recordCount = bucketAsJsonObject.get("doc_count").getAsInt();
				sumStats.setRecordCount(recordCount);
			} else {
				map(recordCount, sumStats, entry);
			}
		}

		return sumStats;

	}

	/**
	 * This method and map(JsonElement bucket) are working in a regressive
	 * manner to read the nested Hierarchical JSON result and create ESStats
	 * accordingly until the leaf aggregation in the JSON.
	 * 
	 * @param recordCount
	 * @param sumStats
	 * @param entry
	 * 
	 */
	private static void map(int recordCount, ESStats sumStats, Entry<String, JsonElement> entry) {
		if (entry.getValue().getAsJsonObject().get("value") != null) {
			sumStats.addAggregation(entry.getKey(), entry.getValue().getAsJsonObject().get("value").getAsDouble());
		} else {
			JsonObject complexAggregation = entry.getValue().getAsJsonObject();

			JsonArray buckets = complexAggregation.get("buckets").getAsJsonArray();

			for (JsonElement bucket : buckets) {
				ESStats subSumStats = map(bucket);
				if (subSumStats != null)
					sumStats.addSubStats(subSumStats);
			}
		}
	}

}
