package com.welldyne.common.frameworks.soapinvoker;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.welldyne.common.frameworks.caching.Cache;
import com.welldyne.common.frameworks.exceptionhandling.SystemException;
import com.welldyne.common.frameworks.s3.S3Utility;
import com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApiConfigurations;
import com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApiName;

public enum SoapApi {

	LIST_PATIENT_ALLERGIES(SoapApiName.LIST_PATIENT_ALLERGIES),
	ALLERGY_LOOKUP(SoapApiName.ALLERGY_LOOKUP),
	LIST_ELIGIBLE_MEMBER_DETAIL(SoapApiName.LIST_ELIGIBLE_MEMBER_DETAIL),
	LIST_PATIENT_HEALTH_CONDITIONS(SoapApiName.LIST_PATIENT_HEALTH_CONDITIONS),
	SEARCH_ELIGIBLE_MEMBER(SoapApiName.SEARCH_ELIGIBLE_MEMBER),
	UPDATE_PATIENT_ADDRESS(SoapApiName.UPDATE_PATIENT_ADDRESS),
	LIST_PATIENT_ADDRESS(SoapApiName.LIST_PATIENT_ADDRESS),
	GET_PATIENT_ADDRESS(SoapApiName.GET_PATIENT_ADDRESS),
	DELETE_PATIENT_ADDRESS(SoapApiName.DELETE_PATIENT_ADDRESS),
	LIST_PATIENT_PHONE(SoapApiName.LIST_PATIENT_PHONE),
	GET_PATIENT_PHONE(SoapApiName.GET_PATIENT_PHONE),
	EDIT_PATIENT_PHONE_NUMBER(SoapApiName.EDIT_PATIENT_PHONE_NUMBER),
	ADD_PATIENT_PHONE_NUMBER(SoapApiName.ADD_PATIENT_PHONE),
	ADD_PATIENT_HEALTH_CONDITIONS(SoapApiName.ADD_PATIENT_HEALTH_CONDITIONS),
	ADD_PATIENT_DRUG_ALLERGIES(SoapApiName.ADD_PATIENT_DRUG_ALLERGIES),
	GET_PATIENT(SoapApiName.GET_PATIENT),
	UPDATE_PATIENT_EMAIL(SoapApiName.UPDATE_PATIENT_EMAIL),
	REGISTER_FAMILY_INFO(SoapApiName.REGISTER_FAMILY_INFO),
	PATIENT_PLANS(SoapApiName.PATIENT_PLANS),
	LIST_PATIENT_PLANS(SoapApiName.LIST_PATIENT_PLANS),
	PATIENT(SoapApiName.PATIENT),
	FAMILY_MEMBERS(SoapApiName.FAMILY_MEMBERS),
	GET_PATIENT_ALLERGIES(SoapApiName.GET_PATIENT_ALLERGIES),
	GET_PATIENT_CONDITIONS(SoapApiName.GET_PATIENT_CONDITIONS),
	PATIENT_PHONE(SoapApiName.PATIENT_PHONE),
	PATIENT_ADDRESS(SoapApiName.PATIENT_ADDRESS),
	ADD_ALLERGY(SoapApiName.ADD_ALLERGY),
	ADD_CONDITION(SoapApiName.ADD_CONDITION),
	LOOKUP_BY_CONDITION(SoapApiName.LOOKUP_BY_CONDITION),
	LIST_CREDIT_CARDS(SoapApiName.LIST_CREDIT_CARDS),
	GET_CREDIT_CARD(SoapApiName.GET_CREDIT_CARD),
	SHIPPING_METHODS(SoapApiName.SHIPPING_METHODS),
	CREATE_ORDER(SoapApiName.CREATE_ORDER),
	GET_ORDER(SoapApiName.GET_ORDER),
	ADD_PATIENT_ADDRESS(SoapApiName.ADD_PATIENT_ADDRESS),
	LIST_PRESCRIBER_ADDRESS(SoapApiName.LIST_PRESCRIBER_ADDRESS),
	LIST_PRESCRIBER_PHONE(SoapApiName.LIST_PRESCRIBER_PHONE),
	GET_PATIENT_PHONES(SoapApiName.GET_PATIENT_PHONES),
	CREATE_ORDER_E_POST(SoapApiName.CREATE_ORDER_E_POST),
	DELETE_CREDIT_CARD(SoapApiName.DELETE_CREDIT_CARD),
	UPDATE_CREDIT_CARD(SoapApiName.UPDATE_CREDIT_CARD),
	ADD_CREDIT_CARD(SoapApiName.ADD_CREDIT_CARD),
	AUTHENTICATE_IDENTITY(SoapApiName.AUTHENTICATE_IDENTITY),
	SEARCH_IDENTITY(SoapApiName.SEARCH_IDENTITY);
	SoapApiName apiName;

	static S3Utility s3Utility;
	static String env;
	static String localEnv;
	static String configuration;

	SoapApi(SoapApiName apiName) {
		this.apiName = apiName;

	}

	/**
	 * This return name of the API
	 * 
	 * @return
	 */
	public String getName() {
		com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApi api = getApi(apiName);
		return api.getName()
				.name();
	}

	/**
	 * This returns full URL of the API.
	 * 
	 * @return
	 */
	public String getUrl() {
		com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApi api = getApi(apiName);
		return api.getUrl();
	}

	/**
	 * This returns full URL of the API.
	 * 
	 * @return
	 */
	public String getAction() {
		com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApi api = getApi(apiName);
		return api.getAction();
	}

	/**
	 * @param apiName
	 *            name of the api for which configuration is needed.
	 * @return returns API configuration.
	 */
	private com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApi getApi(SoapApiName apiName) {
		com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApi SoapApi = getApis().get(apiName);
		return SoapApi;
	}

	/**
	 * This first tries to get APIs from Cache, if not present then creates the
	 * map and put into Cache.
	 * 
	 * @return This returns map of APIs by name so that they are easier to
	 *         search.
	 */
	private Map<SoapApiName, com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApi> getApis() {

		Map<SoapApiName, com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApi> cacheConfig = Cache.SOAP_API_CONFIGURATION.get();

		if (cacheConfig != null && cacheConfig.isEmpty()) {
			return cacheConfig;
		}
		Map<SoapApiName, com.welldyne.common.frameworks.soapapiinvoker.jaxb.SoapApi> soapApis = new HashMap<>();

		SoapApiConfigurations SoapApiConfigurations = getApiConfigurations();
		SoapApiConfigurations.getApis()
				.stream()
				.forEach(apiGroup -> {
					apiGroup.getApi()
							.forEach(api -> {
								api.setUrl(apiGroup.getBasePath()
										.concat(api.getUrl()));
								soapApis.put(api.getName(), api);
							});
				});

		Cache.SOAP_API_CONFIGURATION.put(soapApis);
		return soapApis;
	}

	/**
	 * This reads proper configuration for the enabled environments.
	 * 
	 * @return returns API configurations.
	 */
	private SoapApiConfigurations getApiConfigurations() {
		InputStream inputStream = null;
		if (localEnv.equalsIgnoreCase(env)) {
			inputStream = getClass().getResourceAsStream(configuration);
		} else {
			inputStream = s3Utility.getS3File(configuration);
		}
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(SoapApiConfigurations.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			SoapApiConfigurations SoapApiConfigurations = (SoapApiConfigurations) unmarshaller.unmarshal(inputStream);
			inputStream.close();
			return SoapApiConfigurations;
		} catch (JAXBException | IOException e) {
			throw new SystemException(e);
		}
	}
}
