package com.welldyne.common.frameworks.elasticsearch.services;

import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.SimpleQueryStringFlag;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.GenericTypeResolver;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.welldyne.common.frameworks.elasticsearch.annotations.ESField;
import com.welldyne.common.frameworks.elasticsearch.annotations.ESFieldValueMatchType;
import com.welldyne.common.frameworks.elasticsearch.annotations.ESFieldValueRestriction;
import com.welldyne.common.frameworks.elasticsearch.annotations.ESFieldValueType;
import com.welldyne.common.frameworks.elasticsearch.annotations.ESIndex;
import com.welldyne.common.frameworks.elasticsearch.annotations.ESSortOrder;
import com.welldyne.common.frameworks.elasticsearch.beans.ESEntity;
import com.welldyne.common.frameworks.elasticsearch.beans.ESRestrictions;
import com.welldyne.common.frameworks.elasticsearch.beans.ESSearchResult;
import com.welldyne.common.frameworks.elasticsearch.beans.ESSortColumnMapping;
import com.welldyne.common.frameworks.elasticsearch.beans.ESStats;
import com.welldyne.common.frameworks.exceptionhandling.DataAccessException;
import com.welldyne.common.frameworks.exceptionhandling.SystemException;
import com.welldyne.common.frameworks.layers.RepositoryService;
import com.welldyne.common.frameworks.logging.Loggable;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Count;
import io.searchbox.core.CountResult;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.Search.Builder;
import io.searchbox.core.SearchScroll;
import io.searchbox.params.Parameters;

/**
 * This is the repository which implements four different ways to get data from
 * elastic search. These are the only four identified ways for member portal.
 * 
 * To provide ways to perform CRUD operations very close to the entity is it
 * performing operations on, Repository pattern is very tied to the entity. Here
 * the elastic search entity is tied using generic <TESEntity>, where ESEntity
 * extends ESEntity which is base for all the entities.
 * 
 * @author nilofarmew
 *
 * @param <TESEntity>
 *            The ESEntity associated with the repository
 */
@Loggable
@RepositoryService
public abstract class ESRepository<TESEntity extends ESEntity<TESEntity>> {

	/**
	 * Reads default scroll time from the configuration of the deployable
	 * application which has this library imported
	 */
	@Value("${es.default.scrolltime}")
	private String scrollTime;

	/**
	 * Reads default pagination size from the configuration of the deployable
	 * application which has this library imported
	 */
	@Value("${es.default.paginationSize}")
	private int paginationSize;

	/**
	 * Reads default max limit for number records that can be fetched at once
	 * from the configuration of the deployable application which has this
	 * library imported
	 */
	@Value("${es.default.maxLimit}")
	private int maxLimit;

	/**
	 * Hold index names as mentioned in ESIndex annotation on ESEntity
	 */
	private List<String> indexes;

	/**
	 * Holds class type of ESEntity associate with the subclass of this
	 * repository.
	 */
	private Class<TESEntity> esBeanClass;

	/**
	 * Holds as all the fields that as annotated with {@link @ESField} in
	 * ESEntity
	 */
	private List<Field> esFields;

	/**
	 * Keeps list values for in clause on a field
	 */
	private Map<String, Field> esListFields;

	/**
	 * Holds all name of the fields that as annotated with {@link @ESField} in
	 * ESEntity
	 */
	private List<String> esFieldNames;

	/**
	 * Holds as all the fields that as annotated with {@link @ESField} in
	 * ESEntity where 'sort' is set to true
	 */
	private List<Field> defaultSortColumns;

	/**
	 * Holds as all the fields that as annotated with {@link @ESField} in
	 * ESEntity where 'search' is set to true
	 */
	private List<Field> defaultSearchColumns;

	/**
	 * Holds as all the fields that as annotated with {@link @ESField} in
	 * ESEntity where 'type' is set to SUMMABLE
	 */
	private List<Field> defaultSummableColumns;

	/**
	 * Holds as all the fields that as annotated with {@link @ESField} in
	 * ESEntity where 'type' is set to SET
	 */
	private List<Field> defaultSetColumns;

	/**
	 * Holds parent child relationship for the 'SET' type of fields in the
	 * ESEntity
	 */
	private MultiValueMap<String, Field> subSetByParent;

	/**
	 * Value restrictions against a field can be provided here. The key is name
	 * of the field in index and value is the type of restrictions that should
	 * be applied while fetching the records
	 */
	private Map<String, Field> esFieldValueRestrictions;

	@Autowired
	@Qualifier("mxJestClient")
	private JestClient jestClient;

	protected final Logger logger;

	/**
	 * Protected constructor so that this class can not be instantiated without
	 * creating sub class
	 */
	protected ESRepository() {
		logger = LogManager.getLogger(getClass());
	}

	/**
	 * Initializes private variables will be not change for this instance of the
	 * class
	 */
	@SuppressWarnings("unchecked")
	@PostConstruct
	private void init() {
		// Initializes the actual type of generic TESEntity
		Class<?>[] genericTypes = GenericTypeResolver.resolveTypeArguments(getClass(), ESRepository.class);
		esBeanClass = (Class<TESEntity>) genericTypes[0];

		// Initializes name of the indexes as mentioned in ESIndex annotation
		// on ESEntity
		if (esBeanClass.isAnnotationPresent(ESIndex.class)) {
			ESIndex esIndex = esBeanClass.getAnnotation(ESIndex.class);
			indexes = Arrays.asList(esIndex.name());
			logger.info("Index being invoked is {} ", indexes);
		} else {
			// Throws exception if no ESIndex annotation was found as none of
			// the
			// operations in the repository class will work if index name is not
			// known.
			throw new SystemException("ESIndex annotation must be present on TESEntity");
		}

		// Initializes esFields as all the fields that as annotated with
		// @ESField in ESEntity
		esFields = Arrays.asList(esBeanClass.getDeclaredFields())
				.stream()
				.filter(field -> field.isAnnotationPresent(ESField.class) && !field.getType()
						.isAssignableFrom(List.class))
				.collect(Collectors.toList());

		esListFields = Arrays.asList(esBeanClass.getDeclaredFields())
				.stream()
				.filter(field -> field.isAnnotationPresent(ESField.class) && field.getType()
						.isAssignableFrom(List.class))
				.collect(Collectors.toMap(field -> getFieldName(field), field -> field));

		// Initializes esFieldNames as all name of the fields that as annotated
		// with @ESField in ESEntity
		esFieldNames = esFields.stream()
				.map(esField -> getFieldName(esField))
				.collect(Collectors.toList());

		// Initializes restriction mapping for fields if any available.
		// Restrictions are annotated with @ESFieldValueRestriction
		esFieldValueRestrictions = Arrays.asList(esBeanClass.getDeclaredFields())
				.stream()
				.filter(field -> field.isAnnotationPresent(ESFieldValueRestriction.class))
				.collect(Collectors.toMap(field -> {
					ESFieldValueRestriction eSFieldValueRestriction = field.getAnnotation(ESFieldValueRestriction.class);
					var associatedField = eSFieldValueRestriction.name();
					if (!esFieldNames.contains(associatedField)) {
						throw new SystemException("ESFieldValueRestriction is used for field " + associatedField + " which doesnot esists with ESField annotation in ESBean " + esBeanClass.getName());
					}
					return associatedField;
				}, field -> field));

		// Initializes parent child relationship for the 'SET' type of fields in
		// the ESEntity
		subSetByParent = new LinkedMultiValueMap<String, Field>();

		for (Field field : esFields) {
			ESField esField = field.getAnnotation(ESField.class);
			if (esField.type()
					.equals(ESFieldValueType.SET) && !"self".equalsIgnoreCase(esField.parentSet())) {
				subSetByParent.add(esField.parentSet(), field);
			}
		}
	}

	/**
	 * This returns paginated list the records using search criteria and count
	 * of the total records present in index for same search criteria.
	 * 
	 * @param requestBuilder
	 * @return ESSearchResult<TESEntity> This is a summary DTO created for
	 *         elastic search repository.
	 */
	protected ESSearchResult<TESEntity> getRecords(Consumer<ESRequest<TESEntity>> requestBuilder) {
		ESRequest<TESEntity> esRequest = getRequest(requestBuilder);
		ESSearchResult<TESEntity> result = search(esRequest);

		int totalRecordCount = count(esRequest);
		result.setTotalCount(totalRecordCount);

		return result;
	}

	/**
	 * This returns all the records using search criteria present in index
	 * without considering size limit in request.
	 * 
	 * @param requestBuilder
	 * @return ESSearchResult<TESEntity>
	 */
	protected ESSearchResult<TESEntity> getAllRecords(Consumer<ESRequest<TESEntity>> requestBuilder) {

		int threshold = 10000;

		List<TESEntity> allRecords = new ArrayList<>();
		int returnedRecordCount = 0;

		ESRequest<TESEntity> esRequest = getRequest(requestBuilder);
		do {

			if (esRequest.getSize() == 0)
				esRequest.size(threshold);
			esRequest.openScroll(true);

			ESSearchResult<TESEntity> records = search(esRequest);

			String scrollId = records.getScrollId();
			returnedRecordCount = records.getRecords()
					.size();
			int startRecord = esRequest.getStartRecord() + returnedRecordCount;

			esRequest.startRecord(startRecord);
			esRequest.scrollId(scrollId);

			allRecords.addAll(records.getRecords());

		} while (returnedRecordCount == threshold);

		ESSearchResult<TESEntity> results = new ESSearchResult<>();
		results.setRecords(allRecords);

		return results;

	}

	/**
	 * This returns record count stats by the aggregation criteria provided in
	 * 
	 * @param requestBuilder
	 * @return ESStats
	 */
	protected ESStats getCountStats(Consumer<ESRequest<TESEntity>> requestBuilder) {
		try {
			ESRequest<TESEntity> esRequest = getRequest(requestBuilder);

			BoolQueryBuilder query = getFilterQuery(esRequest);
			addSearchQuery(esRequest, query);

			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			addCountByQuery(esRequest, query, searchSourceBuilder);

			Builder builder = new Search.Builder(searchSourceBuilder.toString());
			indexes.forEach(index -> { builder.addIndex(index); });

			Search search = builder.build();
			JestResult jestResult = jestClient.execute(search);

			JsonElement aggregationsResult = jestResult.getJsonObject()
					.get("aggregations");

			int recordCount = jestResult.getJsonObject()
					.get("hits")
					.getAsJsonObject()
					.get("total")
					.getAsInt();
			JsonObject aggregations = aggregationsResult.getAsJsonObject();

			ESStats stats = ESStatsMapper.map(recordCount, aggregations);

			return stats;
		} catch (IOException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * This returns record sum stats by the aggregation criteria provided in
	 * request.
	 * 
	 * @param requestBuilder
	 * @return ESStats
	 */
	protected ESStats getSumStats(Consumer<ESRequest<TESEntity>> requestBuilder) {
		try {

			ESRequest<TESEntity> esRequest = getRequest(requestBuilder);

			BoolQueryBuilder query = getFilterQuery(esRequest);
			addSearchQuery(esRequest, query);

			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			searchSourceBuilder.query(query);
			addSumByQuery(esRequest, searchSourceBuilder);

			Builder builder = new Search.Builder(searchSourceBuilder.toString());
			indexes.forEach(index -> { builder.addIndex(index); });

			Search search = builder.build();

			JestResult jestResult = jestClient.execute(search);
			JsonElement aggregationsResult = jestResult.getJsonObject()
					.get("aggregations");

			int recordCount = jestResult.getJsonObject()
					.get("hits")
					.getAsJsonObject()
					.get("total")
					.getAsInt();
			JsonObject aggregations = aggregationsResult.getAsJsonObject();

			ESStats stats = ESStatsMapper.map(recordCount, aggregations);

			return stats;
		} catch (IOException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	protected void upsertRecord(TESEntity record) {
		try {
			Index index = new Index.Builder(record).index(indexes.get(0))
					.type("_doc")
					.id(record.getId())
					.build();
			jestClient.execute(index);
		} catch (IOException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Counts total records in index for the request.
	 * 
	 * @param esRequest
	 * @return count
	 */
	private int count(ESRequest<TESEntity> esRequest) {
		try {
			BoolQueryBuilder query = getFilterQuery(esRequest);
			addSearchQuery(esRequest, query);

			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			searchSourceBuilder.query(query);

			Count.Builder builder = new Count.Builder().query(searchSourceBuilder.toString());
			indexes.forEach(index -> { builder.addIndex(index); });

			Count count = builder.build();
			CountResult countResult = jestClient.execute(count);

			int result = 0;
			if (null != countResult && null != countResult.getCount()) {
				result = countResult.getCount()
						.intValue();
			}
			return result;

		} catch (IOException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Checks whether to search using scroll id or using the request object and
	 * invoked appropriate search implementations.
	 * 
	 * @param esRequest
	 * @return ESSearchResult<TESEntity>
	 */
	private ESSearchResult<TESEntity> search(ESRequest<TESEntity> esRequest) {
		try {
			JestResult jestResult = null;
			if (!StringUtils.isEmpty(esRequest.getScrollId())) {
				SearchScroll searchScroll = getSearchScroll(esRequest);
				jestResult = jestClient.execute(searchScroll);
			} else {
				Search search = getSearch(esRequest);
				jestResult = jestClient.execute(search);
			}
			ESSearchResult<TESEntity> esSearchSummary = getESSearchResult(jestResult);
			return esSearchSummary;
		} catch (IOException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Checks if request is to get all the records, if yes then it opens scrolls
	 * on search too.
	 * 
	 * @param esRequest
	 * @return Search
	 */
	private Search getSearch(ESRequest<TESEntity> esRequest) {

		BoolQueryBuilder query = getFilterQuery(esRequest);
		addSearchQuery(esRequest, query);

		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.query(query);

		addSortQuery(esRequest, searchSourceBuilder);
		addPaginationQuery(esRequest, searchSourceBuilder);

		Builder builder = new Search.Builder(searchSourceBuilder.toString());
		indexes.forEach(index -> { builder.addIndex(index); });

		if (esRequest.isOpenScroll()) {
			builder.setParameter(Parameters.SCROLL, scrollTime);
		}
		Search search = builder.build();
		return search;
	}

	/**
	 * Maps JestResult, and CountResult to ESSearchResult
	 * 
	 * @param jestResult
	 * @param countResult
	 * @return ESSearchResult<TESEntity> with records and total count
	 */
	private ESSearchResult<TESEntity> getESSearchResult(JestResult jestResult) {
		String scrollId = getScrollId(jestResult);

		List<TESEntity> records = jestResult.getSourceAsObjectList(esBeanClass);

		ESSearchResult<TESEntity> esSearchSummary = new ESSearchResult<>();
		esSearchSummary.setRecords(records);
		esSearchSummary.setScrollId(scrollId);

		return esSearchSummary;
	}

	/**
	 * 
	 * @param jestResult
	 * @return Scroll id if present in result.
	 */
	private String getScrollId(JestResult jestResult) {
		String scrollId = null;
		final JsonElement scrollIdJson = jestResult.getJsonObject()
				.get("_scroll_id");
		if (scrollIdJson != null) {
			scrollId = scrollIdJson.getAsString();
		}
		return scrollId;
	}

	/**
	 * @param esRequest
	 * @return SearchScroll using scroll id in request.
	 */
	private SearchScroll getSearchScroll(ESRequest<TESEntity> esRequest) {
		SearchScroll searchScroll = new SearchScroll.Builder(esRequest.getScrollId(), scrollTime).build();
		return searchScroll;
	}

	/**
	 * This creates filter term queries for all the fields in the ESEntity with
	 * values provided in request for those fields.
	 * 
	 * If not value is provided for a certain field in ESEntity, that field will
	 * be ignored from the query.
	 * 
	 * @param esRequest
	 * @return BoolQueryBuilder
	 */
	private BoolQueryBuilder getFilterQuery(ESRequest<TESEntity> esRequest) {

		TESEntity searchCriteria = esRequest.getFilterCriteria();

		BoolQueryBuilder query = QueryBuilders.boolQuery();

		esFields.stream()
				.forEach(field -> {
					try {
						ESField esField = field.getAnnotation(ESField.class);
						String esFieldName = getFieldName(field);

						if (esFieldName != null && !esFieldName.isEmpty()) {

							if (esField.dateRange()) {
								if (esRequest.getStartDate() != null) {
									query.must(QueryBuilders.rangeQuery(esFieldName)
											.from(getESStartDate(esRequest.getStartDate()), true));
								}
								if (esRequest.getEndDate() != null) {
									query.must(QueryBuilders.rangeQuery(esFieldName)
											.to(getESEndDate(esRequest.getEndDate()), true));
								}
							} else {

								field.setAccessible(true);
								Object fieldValue = field.get(searchCriteria);

								String queryFieldName;
								if (field.getType()
										.isAssignableFrom(String.class)) {
									queryFieldName = esFieldName + ".keyword";
								} else {
									queryFieldName = esFieldName;
								}

								if (fieldValue != null) {
									if (!StringUtils.isEmpty(esField.painlessScript())) {
										Map<String, Object> params = new HashMap<>();
										params.put(esFieldName, fieldValue);

										Script script = new Script(ScriptType.INLINE, "painless", esField.painlessScript(), params);
										query.must(QueryBuilders.scriptQuery(script));

									} else {

										query.must(QueryBuilders.termsQuery(queryFieldName, fieldValue));
									}
								} else if (!CollectionUtils.isEmpty(esListFields) && esListFields.containsKey(esFieldName)) {
									var esListField = esListFields.get(esFieldName);
									esListField.setAccessible(true);
									Object fieldListValue = esListField.get(searchCriteria);

									if (fieldListValue != null) {
										Set<?> setOfValues = new HashSet<>((List<?>) fieldListValue);
										if (((List<?>) fieldListValue).get(0) instanceof String) {
											setOfValues = ((List<?>) fieldListValue).stream()
													.map(value -> ((String) value).toLowerCase())
													.collect(Collectors.toSet());
										}
										query.must(QueryBuilders.termsQuery(esFieldName, setOfValues));
									}
								} else if (esFieldValueRestrictions.containsKey(esFieldName)) {

									Field restrictionField = esFieldValueRestrictions.get(esFieldName);
									restrictionField.setAccessible(true);
									ESRestrictions esRestrictions = null;

									if (restrictionField.get(searchCriteria) != null) {
										esRestrictions = (ESRestrictions) restrictionField.get(searchCriteria);
									}

									var isAdditionalRestrictionsPresent = esRestrictions != null && !CollectionUtils.isEmpty(esRestrictions.getRestrictions());

									if (isAdditionalRestrictionsPresent) {

										var restrictions = (ESRestrictions) restrictionField.get(searchCriteria);

										restrictions.getRestrictions()
												.forEach(restriction -> {
													ESFieldValueMatchType matchType = restriction.getMatchType();
													Object restrictionValue = restriction.getValue();

													if (restrictionValue != null) {
														var restrictionValueTypeValid = field.getType()
																.isAssignableFrom(restrictionValue.getClass());
														if (!restrictionValueTypeValid) {
															throw new SystemException("type " + field.getType() + " of the field " + esFieldName + " and the type " + restrictionValue.getClass() + " of the value " + restrictionValue + " provided in restriction " + matchType + " does not match ");
														}
													}
													switch (matchType) {
														case NOT_EQUALS: {
															query.mustNot(QueryBuilders.termsQuery(queryFieldName, restrictionValue));
															break;
														}
														case NOT_NULL: {
															query.must(QueryBuilders.existsQuery(queryFieldName));
															break;
														}
														case GREATER_THAN: {
															query.must(QueryBuilders.rangeQuery(queryFieldName)
																	.gt(restrictionValue));
															break;
														}
														case LESS_THAN: {
															query.must(QueryBuilders.rangeQuery(queryFieldName)
																	.lt(restrictionValue));
															break;
														}
														case GREATER_THAN_EQUALS: {
															query.must(QueryBuilders.rangeQuery(queryFieldName)
																	.gte(restrictionValue));
															break;
														}
														case LESS_THAN_EQUALS: {
															query.must(QueryBuilders.rangeQuery(queryFieldName)
																	.lte(restrictionValue));
															break;
														}
														default: {
															throw new SystemException("invalid match type");
														}
													}
												});

									}

								}
							}
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						throw new DataAccessException(e);
					}
				});

		return query;
	}

	/**
	 * Adds search parameters (if present in given request) to the provided
	 * query
	 * 
	 * @param esRequest
	 * @param query
	 */
	private void addSearchQuery(ESRequest<TESEntity> esRequest, BoolQueryBuilder query) {
		String searchString = esRequest.getSearchString();
		if (StringUtils.isEmpty(searchString)) {
			return;
		}

		List<String> searchColumns = esRequest.getSearchColumns();
		if (CollectionUtils.isEmpty(searchColumns)) {
			if (CollectionUtils.isEmpty(defaultSearchColumns)) {
				defaultSearchColumns = esFields.stream()
						.filter(field -> field.getAnnotation(ESField.class)
								.search())
						.collect(Collectors.toList());
			}

			if (!CollectionUtils.isEmpty(defaultSearchColumns)) {
				searchColumns = defaultSearchColumns.stream()
						.map(field -> getFieldName(field))
						.collect(Collectors.toList());
			}
		}

		if (CollectionUtils.isEmpty(searchColumns)) {
			return;
		}

		searchColumns.forEach(searchColumn -> {
			if (!isValidField(searchColumn)) {
				throw new DataAccessException("Invalid search field name " + searchColumn);
			}
		});

		var queryStringQuery = QueryBuilders.simpleQueryStringQuery(searchString.concat("*"));
		queryStringQuery.flags(SimpleQueryStringFlag.PREFIX, SimpleQueryStringFlag.WHITESPACE);
		queryStringQuery.defaultOperator(Operator.AND);
		searchColumns.stream()
				.forEach(searchColumn -> queryStringQuery.field(searchColumn));
		query.must(queryStringQuery);

		if (esRequest.getSearchPainlessScript() != null && !esRequest.getSearchPainlessScript()
				.isEmpty()) {

			Map<String, Object> scriptParams = new HashMap<>();
			scriptParams.put("searchString", esRequest.getSearchString());

			Script script = new Script(ScriptType.INLINE, "painless", esRequest.getSearchPainlessScript(), scriptParams);
			query.should(QueryBuilders.scriptQuery(script));
		}

	}

	/**
	 * Adds size limit to the provided query. If no size limit is provided then,
	 * default pagination size will be used. Default pagination size is read
	 * from configuration of the deployable application invoking this library.
	 * 
	 * @param esRequest
	 * @param searchSourceBuilder
	 */
	private void addPaginationQuery(ESRequest<TESEntity> esRequest, SearchSourceBuilder searchSourceBuilder) {
		if (esRequest.getSize() != 0) {
			searchSourceBuilder.size(esRequest.getSize())
					.from(esRequest.getStartRecord());
		} else {
			searchSourceBuilder.size(paginationSize)
					.from(esRequest.getStartRecord());
		}
	}

	/**
	 * Adds sort order (if present in given request) to the provided query from
	 * request. If sort fields are not present in the request then the default
	 * sort columns will be used from ESEntity.
	 * 
	 * If ESEntity also doesn't have any field marked 'sort' as true, then
	 * sorting will be applied.
	 * 
	 * @param esRequest
	 * @param searchSourceBuilder
	 */
	private void addSortQuery(ESRequest<TESEntity> esRequest, SearchSourceBuilder searchSourceBuilder) {

		if (!CollectionUtils.isEmpty(esRequest.getSortColumns())) {
			esRequest.getSortColumns()
					.forEach((column, esSortOrder) -> {
						String sortFieldName;
						if (ESSortColumnMapping.getByEsName(column)
								.getType()
								.equals("string")) {
							sortFieldName = column + ".keyword";
						} else {
							sortFieldName = column;
						}
						SortOrder sortOrder = SortOrder.ASC;
						if (ESSortOrder.DESC.equals(esSortOrder)) {
							sortOrder = SortOrder.DESC;
						}
						searchSourceBuilder.sort(sortFieldName, sortOrder);
					});
			return;
		}

		if (CollectionUtils.isEmpty(defaultSortColumns)) {
			defaultSortColumns = esFields.stream()
					.filter(field -> field.getAnnotation(ESField.class)
							.sort())
					.collect(Collectors.toList());
			defaultSortColumns.stream()
					.sorted(Comparator.comparing(defaultSortColumn -> defaultSortColumn.getAnnotation(ESField.class)
							.sortSequence()));
		}

		if (CollectionUtils.isEmpty(defaultSortColumns)) {
			return;
		}

		defaultSortColumns.forEach(defaultSortColumn -> {
			String column = getFieldName(defaultSortColumn);
			String sortFieldName = column;
			if (defaultSortColumn.getType()
					.isAssignableFrom(String.class)) {
				sortFieldName = column + ".keyword";
			}

			ESSortOrder esSortOrder = defaultSortColumn.getAnnotation(ESField.class)
					.sortOrder();
			SortOrder sortOrder = SortOrder.ASC;
			if (ESSortOrder.DESC.equals(esSortOrder)) {
				sortOrder = SortOrder.DESC;
			}

			searchSourceBuilder.sort(sortFieldName, sortOrder);
		});

	}

	/**
	 * Adds count by query for all the 'SET' type fields in the ESEntity. If no
	 * 'SET' type fields are present no count by query will be added.
	 * 
	 * If a 'SET' field has a parentSet value, the count stats for the child set
	 * will be part of parent set as sub count stats.
	 * 
	 * @param esRequest
	 * @param query
	 * @return SearchSourceBuilder
	 */
	private void addCountByQuery(ESRequest<TESEntity> esRequest, BoolQueryBuilder query, SearchSourceBuilder searchSourceBuilder) {

		if (CollectionUtils.isEmpty(defaultSetColumns)) {
			defaultSetColumns = esFields.stream()
					.filter(field -> field.getAnnotation(ESField.class)
							.type()
							.equals(ESFieldValueType.SET))
					.collect(Collectors.toList());
		}

		if (CollectionUtils.isEmpty(defaultSetColumns)) {
			return;
		}

		defaultSetColumns.forEach(setColumn -> {
			ESField esField = setColumn.getAnnotation(ESField.class);
			if ("self".equalsIgnoreCase(esField.parentSet())) {
				String fieldName = getFieldName(setColumn);
				TermsAggregationBuilder aggregator = AggregationBuilders.terms(fieldName)
						.field(fieldName + ".keyword");
				if (esRequest.getSize() > 0)
					aggregator.size(esRequest.getSize());
				if (subSetByParent.containsKey(fieldName)) {
					List<Field> subSets = subSetByParent.get(fieldName);

					subSets.stream()
							.forEach(subSet -> {
								String subSetFieldName = getFieldName(subSet);
								TermsAggregationBuilder subAggregator = AggregationBuilders.terms(subSetFieldName)
										.field(subSetFieldName + ".keyword");
								aggregator.subAggregation(subAggregator);
							});
				}

				searchSourceBuilder.aggregation(aggregator)
						.query(query);

			}
		});

	}

	/**
	 * This adds aggregation query for all the 'SUMMABLE' fields. The
	 * aggregation of all the 'SUMMABLE' filed will be returned for each 'SET'
	 * type field.
	 * 
	 * If a 'SET' field has a parentSet value, the aggregation for the child set
	 * will be part of parent set as sub aggregation.
	 * 
	 * @param esRequest
	 * @param searchSourceBuilder
	 */
	private void addSumByQuery(ESRequest<TESEntity> esRequest, SearchSourceBuilder searchSourceBuilder) {
		if (CollectionUtils.isEmpty(defaultSummableColumns)) {
			defaultSummableColumns = esFields.stream()
					.filter(field -> field.getAnnotation(ESField.class)
							.type()
							.equals(ESFieldValueType.SUMMABLE))
					.collect(Collectors.toList());
		}

		if (CollectionUtils.isEmpty(defaultSummableColumns)) {
			return;
		}

		defaultSummableColumns.forEach(field -> {
			String esFieldName = getFieldName(field);
			searchSourceBuilder.aggregation(AggregationBuilders.sum(esFieldName)
					.field(esFieldName));
		});

		if (CollectionUtils.isEmpty(defaultSetColumns)) {
			defaultSetColumns = esFields.stream()
					.filter(field -> field.getAnnotation(ESField.class)
							.type()
							.equals(ESFieldValueType.SET))
					.collect(Collectors.toList());
		}

		defaultSetColumns.forEach(setColumn -> {
			ESField esField = setColumn.getAnnotation(ESField.class);
			if ("self".equalsIgnoreCase(esField.parentSet())) {
				String fieldName = getFieldName(setColumn);
				TermsAggregationBuilder aggregator = AggregationBuilders.terms(fieldName)
						.field(fieldName + ".keyword");

				if (subSetByParent.containsKey(fieldName)) {
					List<Field> subSets = subSetByParent.get(fieldName);

					subSets.stream()
							.forEach(subSet -> {
								String subSetFieldName = getFieldName(subSet);
								TermsAggregationBuilder subAggregator = AggregationBuilders.terms(subSetFieldName)
										.field(subSetFieldName + ".keyword");

								defaultSummableColumns.forEach(summableColumn -> {
									String esFieldName = getFieldName(summableColumn);
									subAggregator.subAggregation(AggregationBuilders.sum(esFieldName)
											.field(esFieldName));
								});
								aggregator.subAggregation(subAggregator);
							});
				}

				defaultSummableColumns.forEach(summableColumn -> {
					String esFieldName = getFieldName(summableColumn);
					aggregator.subAggregation(AggregationBuilders.sum(esFieldName)
							.field(esFieldName));
				});

				searchSourceBuilder.aggregation(aggregator);
			}
		});
	}

	/**
	 * Returns the name of given filed. This first checks value of name()
	 * provided the annotation {@link ESField}. If the value is 'auto' that
	 * means no explicit name have been provided for the Field. In this case the
	 * value() provided in {@link SerializedName} is the name of the field in
	 * the index.
	 * 
	 * 
	 * @param field
	 * @return name of the field in index.
	 */
	private String getFieldName(Field field) {
		ESField esField = field.getAnnotation(ESField.class);
		String esFieldName = esField.name();
		if ("auto".equalsIgnoreCase(esField.name())) {
			SerializedName serializedName = field.getAnnotation(SerializedName.class);
			esFieldName = serializedName.value();
		}
		return esFieldName;
	}

	/**
	 * Check if user provided name for the field is valid field.
	 * 
	 * @param name
	 * @return true is field name is of one of the fields from ESEntity.
	 */
	private boolean isValidField(String name) {
		return esFieldNames.contains(name);
	}

	/**
	 * Builds request object from builder.
	 * 
	 * @param requestBuilder
	 *            builder for the request
	 * @return request object
	 */
	private ESRequest<TESEntity> getRequest(Consumer<ESRequest<TESEntity>> requestBuilder) {
		ESRequest<TESEntity> esRequest = new ESRequest<TESEntity>(esBeanClass).with(requestBuilder);
		return esRequest;
	}

	/**
	 * This creates elastic search acceptable date end date making sure that end
	 * date counted until very last moment of the day that is until
	 * 23:59:59.999Z of the given date.
	 * 
	 * @param date
	 *            the end date from request
	 * @return end date formatted in elastic search acceptable format
	 */
	private static String getESEndDate(LocalDate date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		return date.atTime(LocalTime.MAX)
				.format(formatter);
	}

	/**
	 * This creates elastic search acceptable date for start date making sure
	 * the first moment of the day is considered as start date.
	 * 
	 * @param date
	 *            start date
	 * @return start date formatted in elastic search acceptable format
	 */
	private static String getESStartDate(LocalDate date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		return date.format(formatter);
	}

}
