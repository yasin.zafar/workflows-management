package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

/**
 * This exception is thrown by the controllers in web application which return
 * JSON as the response.
 * 
 * @author nilofarmew
 *
 */
public class ComponentControllerException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4146810208376022306L;
	
	// Added for authentication from legacy and redirection to legacy portal
	private String username;
	private String password;

	public ComponentControllerException() {
		super();
	}

	public ComponentControllerException(Throwable e) {
		super(e);
	}

	public ComponentControllerException(Throwable e, boolean isLogged) {
		super(e, isLogged);
	}

	public ComponentControllerException(String message) {
		super(message);
	}

	public ComponentControllerException(String message, Throwable e) {
		super(message, e);
	}

	public ComponentControllerException(List<Error> errors) {
		super(errors);
	}

	public ComponentControllerException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public ComponentControllerException(List<Error> errors, String message) {
		super(errors, message);
	}

	public ComponentControllerException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public ComponentControllerException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public ComponentControllerException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public ComponentControllerException with(Consumer<ComponentControllerException> builder) {
		builder.accept(this);
		return this;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
