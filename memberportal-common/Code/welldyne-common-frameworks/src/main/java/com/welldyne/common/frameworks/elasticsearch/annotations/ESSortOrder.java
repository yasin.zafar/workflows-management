package com.welldyne.common.frameworks.elasticsearch.annotations;

/**
 * This is to provide sort order on a particular field.
 * 
 * @author nilofarmew
 *
 */
public enum ESSortOrder {

	/**
	 * This is used to sort the field values in Ascending order.
	 */
	ASC,

	/**
	 * This is used to sort the field values in Descending order.
	 */
	DESC
}
