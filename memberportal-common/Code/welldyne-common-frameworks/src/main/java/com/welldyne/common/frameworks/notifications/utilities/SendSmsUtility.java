package com.welldyne.common.frameworks.notifications.utilities;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

public class SendSmsUtility {
	@SuppressWarnings("deprecation")
	public static String sendSMSMessage(String message, String phoneNumber) {
		AWSCredentialsProvider credentials = DefaultAWSCredentialsProviderChain.getInstance();
		AmazonSNSClient snsClient = new AmazonSNSClient(credentials);

		PublishResult result = snsClient.publish(new PublishRequest().withMessage(message).withPhoneNumber(phoneNumber).withMessageAttributes(setSmsAttributes()));

		return String.valueOf(result);
	}

	private static Map<String, MessageAttributeValue> setSmsAttributes() {
		Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
		// smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
		// .withStringValue("mySenderID") //The sender ID shown on the device.
		// .withDataType("String"));
		//
		// smsAttributes.put("AWS.SNS.SMS.MaxPrice", new MessageAttributeValue()
		// .withStringValue("0.50") //Sets the max price to 0.50 USD.
		// .withDataType("Number"));
		//
		// smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
		// .withStringValue("Promotional") //Sets the type to promotional. or
		// Transactional
		// .withDataType("String"));

		return smsAttributes;
	}

}
