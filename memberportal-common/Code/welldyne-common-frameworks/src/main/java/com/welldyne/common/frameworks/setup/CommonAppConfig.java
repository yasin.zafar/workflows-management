package com.welldyne.common.frameworks.setup;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import freemarker.template.TemplateExceptionHandler;

@Configuration
@EnableAspectJAutoProxy
@PropertySources({ @PropertySource("classpath:common-application.properties"), @PropertySource("classpath:common-application-${env}.properties") })
public class CommonAppConfig {

	@Value("${aws.region}")
	private String awsRegion;

	@SuppressWarnings("static-access")
	@Bean
	public AWSCognitoIdentityProvider getAWSCognitoIdentityProvider() {
		AWSCredentialsProvider credentialsProvider = getAWSCredentialsProvider();
		AWSCognitoIdentityProvider awsCognitoIdentityProvider = AWSCognitoIdentityProviderClientBuilder.standard()
				.withRegion(awsRegion)
				.withCredentials(credentialsProvider)
				.defaultClient();
		return awsCognitoIdentityProvider;
	}

	@SuppressWarnings("static-access")
	@Bean
	public AmazonS3 getS3Client() {
		AWSCredentialsProvider credentials = getAWSCredentialsProvider();
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(credentials)
				.withRegion(awsRegion)
				.defaultClient();
		return s3Client;
	}

	@Bean("invokerObjectMapper")
	public ObjectMapper invokerObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.registerModule(new JavaTimeModule());
		return objectMapper;
	}

	/**
	 * Freemarker configuration to read email and sms templates.
	 *
	 * @return the freemarker.template.Configuration
	 */
	@Bean
	public freemarker.template.Configuration freemarkerConfiguration() {
		freemarker.template.Configuration configuration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_23);
		configuration.setDefaultEncoding("UTF-8");
		configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		return configuration;
	}

	/**
	 * Sns client for sending sms
	 * 
	 * @return AmazonSNSClient
	 */
	@Bean
	public AmazonSNSClient getAmazonSNSClient() {
		AWSCredentialsProvider credentialsProvider = getAWSCredentialsProvider();
		return (AmazonSNSClient) AmazonSNSClientBuilder.standard()
				.withCredentials(credentialsProvider)
				.withRegion(awsRegion)
				.build();
	}

	/**
	 * Provides AWS credentials.
	 * 
	 * @return AWSCredentialsProvider
	 */
	@Bean
	public AWSCredentialsProvider getAWSCredentialsProvider() {
		AWSCredentialsProvider credentialsProvider = DefaultAWSCredentialsProviderChain.getInstance();
		return credentialsProvider;
	}

	@Bean
	public AmazonSimpleEmailServiceClient getAmazonSESClient() {
		AWSCredentialsProvider credentialsProvider = getAWSCredentialsProvider();
		return (AmazonSimpleEmailServiceClient) AmazonSimpleEmailServiceClientBuilder.standard()
				.withCredentials(credentialsProvider)
				.withRegion(awsRegion)
				.build();
	}

}
