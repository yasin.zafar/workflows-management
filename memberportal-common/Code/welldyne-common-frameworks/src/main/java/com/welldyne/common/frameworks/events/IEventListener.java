package com.welldyne.common.frameworks.events;

import org.springframework.context.ApplicationEvent;

/**
 * This is the interface defining the contract to be followed by each event
 * listener.
 * 
 * @author Puneet.Kumar
 *
 */
public interface IEventListener<T extends ApplicationEvent> {
	void handleEvent(T event);

}
