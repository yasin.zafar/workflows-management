package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class NoDefaultAddressException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2024654437351238411L;

	public NoDefaultAddressException() {
		super();
	}

	public NoDefaultAddressException(Throwable e) {
		super(e);
	}

	public NoDefaultAddressException(String message) {
		super(message);
	}

	public NoDefaultAddressException(String message, Throwable e) {
		super(message, e);
	}

	public NoDefaultAddressException(List<Error> errors) {
		super(errors);
	}

	public NoDefaultAddressException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public NoDefaultAddressException(List<Error> errors, String message) {
		super(errors, message);
	}

	public NoDefaultAddressException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public NoDefaultAddressException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public NoDefaultAddressException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public NoDefaultAddressException with(Consumer<NoDefaultAddressException> builder) {
		builder.accept(this);
		return this;
	}

}
