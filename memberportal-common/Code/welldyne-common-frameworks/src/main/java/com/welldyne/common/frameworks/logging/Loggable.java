package com.welldyne.common.frameworks.logging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * This is being used to annotate classes for which entry and exits of the all the public methods needs to be logged.
 * 
 * @author nilofarmew
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Loggable {
}
