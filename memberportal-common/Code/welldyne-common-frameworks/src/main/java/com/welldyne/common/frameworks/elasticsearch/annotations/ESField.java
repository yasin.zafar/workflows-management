package com.welldyne.common.frameworks.elasticsearch.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.welldyne.common.frameworks.elasticsearch.services.ESRequest;

/**
 * This annotation is applied on fields which are part of index being defined.
 * 
 * @author nilofarmew
 *
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface ESField {

	/**
	 * This is name of the field as it is in the index. If not provided, value
	 * from @SerializedName will be used as name. Recommended is not to provide
	 * name to avoid duplication of same value on two annotations.
	 * 
	 * @return name
	 */
	String name() default "auto";

	/**
	 * This is type of the field. There are three pre-defined types in the
	 * emun. @see ESFieldValueType for more details.
	 * 
	 * @return type
	 */
	ESFieldValueType type();

	/**
	 * Only set this when type() is ESFieldValueType.SET. If any field is
	 * mentioned as parent of a field, the aggregation query results for parent
	 * will be also have child aggregations as sub aggregations.
	 * 
	 * The default value is "self" which mean this field does not have any
	 * parent.
	 * 
	 * @return parentSet
	 */
	String parentSet() default "self";

	/**
	 * If this is set true for a field, the field will be considered in search
	 * query when search string is present in the request {@link ESRequest}
	 * 
	 * @return search
	 */
	boolean search() default false;

	/**
	 * If this is set true for a field, the field will be used to sort records
	 * if the sort column is not provided in the request {@link ESRequest}
	 * 
	 * Only set this true for one Field in a class. If set true for multiple
	 * field, the first field will be used to sort records by default.
	 * 
	 * If no field is marked with 'sort' as true, then no sorting will be
	 * applied.
	 * 
	 * @return sort
	 */
	boolean sort() default false;

	/**
	 * If 'sort()' is being set true to a field, also provide the order that
	 * sort is required in. If not present then records will be sorted in
	 * Ascending order.
	 * 
	 * Only provide this value when 'sort()' is set as true, otherwise this is
	 * not read.
	 * 
	 * @return sortOrder
	 */
	ESSortOrder sortOrder() default ESSortOrder.ASC;

	/**
	 * If the sorting supposed to be done on multiple fields, the priority
	 * sequence for those fields should be provided. If all the fields have
	 * priority as -1 then the default sequence of fields will be as they appear
	 * in ESEntity class.
	 * 
	 * Otherwise the sort sequence will be the Ascending order of the sequence
	 * of different fields.
	 * 
	 * @return sort sequence
	 */
	int sortSequence() default -1;

	/**
	 * If this is set true for a field, the field will be used to apply date
	 * range limit in the search only when 'dateRange' column value is not
	 * provided in request {@link ESRequest}
	 * 
	 * Only set this true for one Field in a class. If set true for multiple
	 * field, the first field will be used by default.
	 * 
	 * Make sure to apply it on the field which must have date type value in the
	 * index.
	 * 
	 * @return dateRange
	 */
	boolean dateRange() default false;

	/**
	 * 
	 * By default all the values set in ESEntity in {@link ESRequest} will be
	 * included in elastic search query i.e. the query is created for match the
	 * value provided for each field.
	 * 
	 * But if it is needed for a field to exclude values provide (i.e. must not
	 * match), then annotate the field with matchValue() as false.
	 * 
	 * @return matchValue
	 */

	/**
	 * TODO::This is still pending.
	 * 
	 * @return
	 */
	String painlessScript() default "";

}
