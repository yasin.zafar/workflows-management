package com.welldyne.common.frameworks.validateaddress;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "error" })

public class ValidateAddressApiInvokerResponse {

	@XmlAttribute(name = "AddressValidateResponse")
	AddressValidateResponse AddressValidateResponse;

	public AddressValidateResponse getAddressValidateResponse() {
		return AddressValidateResponse;
	}

	public void setAddressValidateResponse(AddressValidateResponse addressValidateResponse) {
		AddressValidateResponse = addressValidateResponse;
	}

}
