
package com.welldyne.common.frameworks.restapiinvoker.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RestApiMethodType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RestApiMethodType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="post"/&gt;
 *     &lt;enumeration value="get"/&gt;
 *     &lt;enumeration value="put"/&gt;
 *     &lt;enumeration value="delete"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RestApiMethodType")
@XmlEnum
public enum RestApiMethodType {

    @XmlEnumValue("post")
    POST("post"),
    @XmlEnumValue("get")
    GET("get"),
    @XmlEnumValue("put")
    PUT("put"),
    @XmlEnumValue("delete")
    DELETE("delete");
    private final String value;

    RestApiMethodType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RestApiMethodType fromValue(String v) {
        for (RestApiMethodType c: RestApiMethodType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
