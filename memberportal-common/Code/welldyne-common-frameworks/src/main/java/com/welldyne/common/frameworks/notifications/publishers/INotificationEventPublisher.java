package com.welldyne.common.frameworks.notifications.publishers;

import com.welldyne.common.frameworks.notifications.beans.Notification;

/**
 * Interface which defines a contract for different event publisher.
 * 
 * @author Puneet.Kumar
 *
 * @param <T>
 */
public interface INotificationEventPublisher<T extends Notification> {
	/**
	 * This method is used to publish different types of notification events.
	 * 
	 * @param message
	 */
	public void publishNotificationEvent(T message);

}
