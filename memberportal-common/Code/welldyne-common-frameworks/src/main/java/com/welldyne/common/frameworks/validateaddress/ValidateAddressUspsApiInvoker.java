package com.welldyne.common.frameworks.validateaddress;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.welldyne.common.frameworks.exceptionhandling.Error;
import com.welldyne.common.frameworks.exceptionhandling.ErrorCode;
import com.welldyne.common.frameworks.exceptionhandling.VerificationFailedException;
import com.welldyne.common.frameworks.restapiinvoker.RestApi;
import com.welldyne.common.frameworks.restapiinvoker.RestApiInvoker;

/**
 * 
 * @author devendra
 *
 */
@Repository
class ValidateAddressUspsApiInvoker extends RestApiInvoker<ValidateAddressUspsApiInvokerRequest, ValidateAddressApiInvokerResponse> {

	@Override
	protected RestApi getApi() {
		return RestApi.VALIDATE_ADDRESS;
	}

	@Override
	protected Map<String, Object> getQueryParameters(ValidateAddressUspsApiInvokerRequest request) {
		Map<String, Object> queryParams = new HashMap<>();
		var xml = "<AddressValidateRequest USERID=\"974AVIZV7426\"><Address ID=\"0\"><Address1>" + request.getStreet() + "</Address1><Address2>" + request.getStreet2() + "</Address2><City>" + request.getCity() + "</City><State>" + request.getState() + "</State><Zip5>" + request.getZipcode() + "</Zip5><Zip4></Zip4></Address></AddressValidateRequest>";
		queryParams.put("xml", xml);
		return queryParams;
	}

	@Override
	protected Object getRequestBody(ValidateAddressUspsApiInvokerRequest request) {
		JsonArray jsonArray = new JsonArray();
		jsonArray.add(request.toString());
		return jsonArray;
	}

	@Override
	protected HttpHeaders getHeaders(ValidateAddressUspsApiInvokerRequest request) {
		HttpHeaders headers = new HttpHeaders();

		return headers;
	}

	public AddressValidationResult getResponse(ValidateAddressUspsApiInvokerRequest req) {
		try {
			HttpEntity<Object> requestEntity = null;

			RestTemplate restTemplate = new RestTemplate();
			var apiUrl = getApi().getUrl();
			var responseEntity = restTemplate.exchange(apiUrl, HttpMethod.GET, requestEntity, String.class, getQueryParameters(req));

			var jaxbContext = JAXBContext.newInstance(AddressValidationResult.class);
			Unmarshaller un = jaxbContext.createUnmarshaller();
			var res = responseEntity.getBody();

			StreamSource streamSource = new StreamSource(new StringReader(res));
			AddressValidationResult addressValidateResponse = (AddressValidationResult) un.unmarshal(streamSource);
			if (ObjectUtils.isEmpty(addressValidateResponse)) {
				throw new VerificationFailedException("Invalid Address").withError(new Error().withErrorCode(ErrorCode.INVALID_ADDRESS));
			} else {
				return addressValidateResponse;
			}
		}

		catch (Exception e) {

			throw new VerificationFailedException("Invalid Address").withError(new Error().withErrorCode(ErrorCode.INVALID_ADDRESS));

		}

	}

}
