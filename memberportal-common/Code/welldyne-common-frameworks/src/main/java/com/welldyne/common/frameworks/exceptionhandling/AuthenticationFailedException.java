package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

/**
 * 
 * @author nilofarmew
 *
 */
public class AuthenticationFailedException extends BaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3804770459735441126L;

	public AuthenticationFailedException() {
		super();
	}

	public AuthenticationFailedException(Throwable e) {
		super(e);
	}

	public AuthenticationFailedException(String message) {
		super(message);
	}

	public AuthenticationFailedException(String message, Throwable e) {
		super(message, e);
	}

	public AuthenticationFailedException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public AuthenticationFailedException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(this.errors)) {
			this.errors = new ArrayList<Error>();
		}

		this.errors.addAll(errors);
		return this;
	}

	public AuthenticationFailedException with(Consumer<AuthenticationFailedException> builder) {
		builder.accept(this);
		return this;
	}
}
