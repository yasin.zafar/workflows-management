package com.welldyne.common.frameworks.elasticsearch.beans;

import java.util.HashMap;
import java.util.Map;

public enum ESSortColumnMapping {

	EXPIRY_DATE("expirationDate", "expirationDate", "date"),
	REFILLS_REMAINING("refillsLeft", "refillsLeft", "number"),
	PREVIOUS_COPAY("lastRefillPatientCopay", "previousCopay", "number"),
	PRESCRIBED_BY("prescriberFirstName", "prescribedBy", "string"),
	POTENTIAL_SAVINGS("mailOrderDrugSaving", "potentialSavings", "number"),
	ESTIMATED_COST("lastRefillPatientCopay", "estimatedCost", "number"),
	/**
	 * we are gettinglastRefillPatientCopay for now as nextRefillPatientCopay is
	 * not available.
	 */
	NEXT_REFILL_DATE("nextAvailableRefillDate", "nextAvailableRefillDate", "date"),
	DRUG_NAME("writtenProduct", "writtenDrugForm", "string"),
	PRESCRIPTION_NUMBER("rxNumber", "rxNumber", "string"),
	ALTERNATE_DISPENSED("dispensedProduct", "alternateDispensed", "string"),
	REFILL_SEQUENCE_NUMBER("refillSequenceNumber", "refillSequenceNumber", "number"),
	CLAIM_UID("claimUid", "claimUid", "number");

	private String esName;
	private String presentationName;
	private String type;

	public String getEsName() {
		return esName;
	}

	public void setEsName(String esName) {
		this.esName = esName;
	}

	public String getPresentationName() {
		return presentationName;
	}

	public void setPresentationName(String presentationName) {
		this.presentationName = presentationName;
	}

	private static Map<String, ESSortColumnMapping> lookUpByPresentationName = new HashMap<>();
	private static Map<String, ESSortColumnMapping> lookUpByEsName = new HashMap<>();

	ESSortColumnMapping(String esName, String presentationName, String type) {
		this.esName = esName;
		this.presentationName = presentationName;
		this.type = type;
	}

	static {
		for (ESSortColumnMapping sortColunms : ESSortColumnMapping.values()) {
			lookUpByEsName.put(sortColunms.esName, sortColunms);
		}
	}

	static {
		for (ESSortColumnMapping sortColunms : ESSortColumnMapping.values()) {
			lookUpByPresentationName.put(sortColunms.presentationName, sortColunms);
		}
	}

	public static ESSortColumnMapping getByPresentationName(String presentationName) {
		return lookUpByPresentationName.get(presentationName);
	}

	public static ESSortColumnMapping getByEsName(String esName) {
		return lookUpByEsName.get(esName);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
