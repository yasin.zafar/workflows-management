package com.welldyne.common.frameworks.notifications.events;

import org.springframework.context.ApplicationEvent;
import org.springframework.core.ResolvableType;
import org.springframework.core.ResolvableTypeProvider;

import com.welldyne.common.frameworks.notifications.beans.Notification;

/**
 * This is the notification event which takes a parameter of generic which
 * extends notification. When this event is published corresponding listener
 * will get invoked.
 * 
 * @author Puneet.Kumar
 *
 * @param <T>
 */
public class NotificationEvent<T extends Notification> extends ApplicationEvent implements ResolvableTypeProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private T notification;

	public NotificationEvent(T notification) {
		super(notification);
		this.notification = notification;
	}

	public T getNotification() {
		return notification;
	}

	public void setNotification(T notfication) {
		this.notification = notfication;
	}

	@Override
	public ResolvableType getResolvableType() {
		return ResolvableType.forClassWithGenerics(getClass(), ResolvableType.forInstance(getSource()));
	}

}
