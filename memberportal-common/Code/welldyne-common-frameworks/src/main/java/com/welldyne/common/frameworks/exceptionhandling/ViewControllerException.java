package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

/**
 * This exception is thrown by the controllers in web application which return
 * ModelAndView as the response.
 * 
 * @author nilofarmew
 *
 */
public class ViewControllerException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2754961930810477841L;

	public ViewControllerException() {
		super();
	}

	public ViewControllerException(Throwable e) {
		super(e);
	}

	public ViewControllerException(Throwable e, boolean isLogged) {
		super(e, isLogged);
	}

	public ViewControllerException(String message) {
		super(message);
	}

	public ViewControllerException(String message, Throwable e) {
		super(message, e);
	}

	public ViewControllerException(List<Error> errors) {
		super(errors);
	}

	public ViewControllerException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public ViewControllerException(List<Error> errors, String message) {
		super(errors, message);
	}

	public ViewControllerException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	public ViewControllerException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	public ViewControllerException with(Consumer<ViewControllerException> builder) {
		builder.accept(this);
		return this;
	}
}
