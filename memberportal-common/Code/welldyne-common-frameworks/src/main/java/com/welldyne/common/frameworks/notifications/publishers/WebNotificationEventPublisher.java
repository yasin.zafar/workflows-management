package com.welldyne.common.frameworks.notifications.publishers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.welldyne.common.frameworks.notifications.beans.WebNotification;
import com.welldyne.common.frameworks.notifications.events.NotificationEvent;

@Component
public class WebNotificationEventPublisher implements INotificationEventPublisher<WebNotification> {

	@Autowired
	ApplicationEventPublisher applicationEventPublisher;

	/**
	 * This method publishes web notification event.
	 */
	@Override
	public void publishNotificationEvent(WebNotification message) {
		NotificationEvent<WebNotification> event = new NotificationEvent<WebNotification>(message);
		applicationEventPublisher.publishEvent(event);
	}

}