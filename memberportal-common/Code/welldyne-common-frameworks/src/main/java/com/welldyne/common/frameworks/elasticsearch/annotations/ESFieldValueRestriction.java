package com.welldyne.common.frameworks.elasticsearch.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface ESFieldValueRestriction {
	/**
	 * This is name of the field as it is in the index.
	 * 
	 * @return name
	 */
	String name();

}
