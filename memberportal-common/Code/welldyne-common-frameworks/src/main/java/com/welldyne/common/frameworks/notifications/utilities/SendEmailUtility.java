package com.welldyne.common.frameworks.notifications.utilities;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;

@Service
public class SendEmailUtility {

	@Value("${mail.user}")
	private String userName;

	@Autowired
	private AmazonSimpleEmailServiceClient sesClient;

	private Logger log = LogManager.getLogger(this.getClass());

	public void sendSeSMail(String to, String mailBody, String subject, String[] attachment) {
		// String userName = environment.getProperty("mail.user");
		Properties properties = new Properties();

		try {

			Session session = Session.getInstance(properties, null);

			// creates a new e-mail message
			Message msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress(userName));

			InternetAddress[] toAddresses = { new InternetAddress(to) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(mailBody, "text/html");

			// creates multi-part
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// adds attachments
			String[] attachFiles = attachment;
			if (attachFiles != null && attachFiles.length > 0) {
				for (String filePath : attachFiles) {

					try {
						MimeBodyPart attachPart = new MimeBodyPart();
						DataSource source = new FileDataSource(filePath);
						attachPart.setDataHandler(new DataHandler(source));
						attachPart.setFileName(filePath);
						multipart.addBodyPart(attachPart);
					} catch (Exception ex) {
						log.error(ex);
					}

				}
			}

			// sets the multi-part as e-mail's content
			msg.setContent(multipart);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			msg.writeTo(outputStream);
			RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

			SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
			rawEmailRequest.setDestinations(Arrays.asList(to));
			rawEmailRequest.setSource(userName);

			sesClient.sendRawEmail(rawEmailRequest);

		} catch (Exception e) {
			log.error(e);
			// return "Mail sending failed";
		}

	}

	// public void sendSmtpMail(String to, String mailBody, String subject,
	// String[] attachment) {
	// // TODO Temp log to test Service now mail. Since there is no other way
	// // to test it. To be removed after testing.
	// log.info("Sending mail to: " + to + ". Subject: " + subject + ". With
	// user name: " + environment.getProperty("mail.user"));
	// String userName = environment.getProperty("mail.user");
	//
	// String password = environment.getProperty("mail.password");
	//
	// // sets SMTP server properties
	// Properties properties = new Properties();
	//
	// // Logging these fields to debug the SMTP settings issue
	// log.info("host: " + environment.getProperty("mail.smtp.host"));
	// log.info("port: " + environment.getProperty("mail.smtp.port"));
	// log.info("auth: " + environment.getProperty("mail.smtp.auth"));
	// log.info("user: " + environment.getProperty("mail.user"));
	// log.info("password: " + environment.getProperty("mail.password"));
	// log.info("starttls: " +
	// environment.getProperty("mail.smtp.starttls.enable"));
	// properties.put("mail.smtp.host",
	// environment.getProperty("mail.smtp.host"));
	// properties.put("mail.smtp.port",
	// environment.getProperty("mail.smtp.port"));
	// properties.put("mail.smtp.auth",
	// environment.getProperty("mail.smtp.auth"));
	// properties.put("mail.smtp.starttls.enable", true);
	// properties.put("mail.user", userName);
	// properties.put("mail.password", password);
	// properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
	//
	// try {
	// // creates a new session with an authenticator
	// Authenticator auth = new Authenticator() {
	// @Override
	// public PasswordAuthentication getPasswordAuthentication() {
	// return new PasswordAuthentication(userName, password);
	// }
	// };
	// Session session = Session.getInstance(properties, auth);
	//
	// // creates a new e-mail message
	// Message msg = new MimeMessage(session);
	//
	// msg.setFrom(new InternetAddress(userName));
	//
	// InternetAddress[] toAddresses = { new InternetAddress(to) };
	// msg.setRecipients(Message.RecipientType.TO, toAddresses);
	// msg.setSubject(subject);
	// // msg.setSentDate(new Date());
	//
	// // creates message part
	// MimeBodyPart messageBodyPart = new MimeBodyPart();
	// messageBodyPart.setContent(mailBody, "text/html");
	//
	// // creates multi-part
	// Multipart multipart = new MimeMultipart();
	// multipart.addBodyPart(messageBodyPart);
	//
	// // adds attachments
	// String[] attachFiles = attachment;
	// if (attachFiles != null && attachFiles.length > 0) {
	// for (String filePath : attachFiles) {
	//
	// try {
	// MimeBodyPart attachPart = new MimeBodyPart();
	// DataSource source = new FileDataSource(filePath);
	// attachPart.setDataHandler(new DataHandler(source));
	// attachPart.setFileName(filePath);
	// multipart.addBodyPart(attachPart);
	// } catch (Exception ex) {
	// log.error(ex);
	// }
	//
	// }
	// }
	//
	// // sets the multi-part as e-mail's content
	// msg.setContent(multipart);
	//
	// // sends the e-mail
	// Transport.send(msg);
	// log.info("Mail Successfully sent");
	//
	// } catch (MessagingException e) {
	// log.error(e);
	// // return "Mail sending failed";
	// }
	// // return "Mail Successfully sent";
	//
	// }

}
