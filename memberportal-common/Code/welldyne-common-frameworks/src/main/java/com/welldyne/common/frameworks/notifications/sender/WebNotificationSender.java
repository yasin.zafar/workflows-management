package com.welldyne.common.frameworks.notifications.sender;

import java.util.List;

import org.springframework.stereotype.Component;

import com.welldyne.common.frameworks.exceptionhandling.NotificationSenderException;
import com.welldyne.common.frameworks.notifications.beans.SenderResponse;
import com.welldyne.common.frameworks.notifications.beans.WebNotification;

/**
 * This class holds all the logic to persist web notification
 * 
 * @author Puneet.Kumar
 *
 */
@Component
public class WebNotificationSender extends BaseNotificationSender<WebNotification> {

	@Override
	public SenderResponse<List<String>> sendNotification(WebNotification notification) throws NotificationSenderException {
		return null;
		// TODO Persist data to database
		// Audit Final Notification
	}

}
