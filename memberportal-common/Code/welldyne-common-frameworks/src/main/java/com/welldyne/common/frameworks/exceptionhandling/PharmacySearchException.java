package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class PharmacySearchException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2024654437351238411L;

	public PharmacySearchException() {
		super();
	}

	public PharmacySearchException(Throwable e) {
		super(e);
	}

	public PharmacySearchException(String message) {
		super(message);
	}

	public PharmacySearchException(String message, Throwable e) {
		super(message, e);
	}

	public PharmacySearchException(List<Error> errors) {
		super(errors);
	}

	public PharmacySearchException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public PharmacySearchException(List<Error> errors, String message) {
		super(errors, message);
	}

	public PharmacySearchException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public PharmacySearchException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public PharmacySearchException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public PharmacySearchException with(Consumer<PharmacySearchException> builder) {
		builder.accept(this);
		return this;
	}

}
