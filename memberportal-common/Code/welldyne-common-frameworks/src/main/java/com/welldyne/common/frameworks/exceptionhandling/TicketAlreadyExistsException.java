package com.welldyne.common.frameworks.exceptionhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.util.CollectionUtils;

public class TicketAlreadyExistsException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5553557842224774740L;

	public TicketAlreadyExistsException() {
		super();
	}

	public TicketAlreadyExistsException(Throwable e) {
		super(e);
	}

	public TicketAlreadyExistsException(String message) {
		super(message);
	}

	public TicketAlreadyExistsException(String message, Throwable e) {
		super(message, e);
	}

	public TicketAlreadyExistsException(List<Error> errors) {
		super(errors);
	}

	public TicketAlreadyExistsException(List<Error> errors, BaseException e) {
		super(errors, e);
		super.setLogged(e.isLogged());
	}

	public TicketAlreadyExistsException(List<Error> errors, String message) {
		super(errors, message);
	}

	public TicketAlreadyExistsException(List<Error> errors, String message, BaseException e) {
		super(errors, message, e);
		super.setLogged(e.isLogged());
	}

	@Override
	public TicketAlreadyExistsException withError(Error error) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.add(error);
		return this;
	}

	@Override
	public TicketAlreadyExistsException withErrors(List<Error> errors) {
		if (CollectionUtils.isEmpty(errors)) {
			errors = new ArrayList<Error>();
		}

		errors.addAll(errors);
		return this;
	}

	public TicketAlreadyExistsException with(Consumer<TicketAlreadyExistsException> builder) {
		builder.accept(this);
		return this;
	}
}